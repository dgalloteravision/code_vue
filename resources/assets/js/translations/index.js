export default {
    es: {
        common: {
          close: 'Cerrrar',
          save: 'Guardar',
          delete: 'Eliminar',
          deleterecord: '¿Esta seguro de eliminar este registro?',
        },
        messages: {
            processing: 'Procesando la solicitud',
            accessDenied: 'Acceso denegado'
        },
        languages: {
            es: 'Español',
            en: 'Inglés'
        },
        navigation: {
            login: 'Iniciar sesión',
            register: 'Registrar cuenta',
            logout: 'Cerrar sesión',
            my_account: 'Mi cuenta',
            cinema: 'Cines',
            bookings: 'Reservas',
            language: 'Selecciona el idioma'
        },
        sidebar: {
          parametrizer: 'Parametrizaciones',
          security: 'Seguridad',
          report: 'Reportes',
          organizations: 'Organizaciones'
        },
        login: {
            title: '<i class="fa fa-fw fa-sign-in-alt mr-1"></i> Iniciar sesión',
            email: 'Correo electrónico',
            password: 'Contraseña',
            error: '<p class="mb-0">Error procesando el formulario</p>'
        },
        register: {
            username: 'Nombre de usuario',
            title: '<i class="fa fa-fw fa-plus mr-1"></i> Registrar',
            email: 'Correo electrónico',
            password: 'Contraseña',
            password_confirmation: 'Confirma la contraseña',
            error: '<p class="mb-0">Error procesando el formulario</p>'
        },
        profile: {
            title: 'Actualizar mi perfil',
            username: 'Nombre de usuario',
            email: 'Correo electrónico',
            updated: 'Perfil y token actualizado correctamente',
            error: '<p class="mb-0">Error actualizando el perfil</p>'
        },
        reset: {
            title: 'Resetear contraseña',
            email: 'Correo electrónico',
            error: '<p class="mb-0">Error actualizando el perfil</p>'
        },
        estado: {
            name: 'Nombre del estado',
            create: 'Crear Estado',
            edit: 'Actualizar Estado',
            delete: 'Elimar Estado',
            title: 'Estados',
            new: 'Nuevo Estado',
            vip: 'Estados VIP',
            admin: 'Acciones Administrador',
            column: 'Estado',
            notfound: 'Estados no encontrados'
        },
        tipodocumento: {
            name: 'Nombre del tipo documento',
            create: 'Crear Tipo Documento',
            edit: 'Actualizar Tipo Documento',
            delete: 'Elimar Tipo Documento',
            title: 'Tipos Documento',
            new: 'Nuevo Tipo Documento',
            vip: 'Tipos Documento VIP',
            admin: 'Acciones Administrador',
            column: 'Tipo Documento',
            notfound: 'Tipos Documento no encontrados'
        },
        departamento: {
          name: 'Nombre del departamento',
          create: 'Crear Departamento',
          edit: 'Actualizar Departamento',
          delete: 'Elimar Departamento',
          title: 'Departamentos',
          new: 'Nuevo Departamento',
          vip: 'Departamentos VIP',
          admin: 'Acciones Administrador',
          column: 'Departamento',
          notfound: 'Departamentos no encontrados'
        },
        municipio: {
          name: 'Nombre del municipio',
          create: 'Crear Municipio',
          edit: 'Actualizar Municipio',
          delete: 'Elimar Municipio',
          title: 'Municipios',
          new: 'Nuevo Municipio',
          vip: 'Municipios VIP',
          admin: 'Acciones Administrador',
          column: 'Municipio',
          notfound: 'Municipios no encontrados'
        },
        filter: {
            cinema: 'Busca cines',
            search: 'Escribe tu búsqueda',
            rooms: 'Número de salas',
            seats: 'Número de asientos',
            clear: 'Limpiar filtros',
            movie: 'Busca pelis',
            rows: 'Número de filas',
            genres: 'Distintos géneros',
            hours: 'Horas disponibles'
        },
        movie: {
            name: 'Película',
            genres: 'Géneros',
            director: 'Director',
            synopsis: 'Sinopsis',
            seats: 'Número de asientos',
            room_number: 'Número de sala',
            room_rows: 'Número de filas en la sala',
            room_seats: 'Número de asientos en la sala',
            empty: 'No se han encontrado películas con este filtro',
            reservation: 'Hacer reserva',
            detail: 'Ver película'
        },
        genre: {
            empty: 'No hay generos asociados',
            name: 'Géneros'
        },
        movie_showing_times: {
            empty: 'Película no disponible para hoy',
            name: 'Horarios para hoy'
        },
        booking: {
            screen_info: 'La pantalla está aquí',
            process_book: 'Procesar reserva!',
            date: 'Reserva',
            seats: 'Asientos reservados',
            row: 'Fila',
            seat: 'Asiento',
            hour: 'Hora',
            list: 'Listado de tus reservas'
        }
    },
    en: {
        messages: {
            processing: 'Processing the request',
            accessDenied: 'Access denied'
        },
        languages: {
            es: 'Spanish',
            en: 'English'
        },
        navigation: {
            login: 'Login',
            register: 'Register',
            logout: 'Logout',
            my_account: 'My account',
            cinema: 'Cinemas',
            bookings: 'Bookings',
            language: 'Select a language'
        },
        login: {
            title: 'Login',
            email: 'E-mail',
            password: 'Password',
            error: 'Error processing the form'
        },
        register: {
            username: 'Username',
            title: 'Register on the platform',
            email: 'E-mail',
            password: 'Password',
            password_confirmation: 'Confirm the password',
            error: 'Error processing the form'
        },
        profile: {
            title: 'Update my profile',
            username: 'Username',
            email: 'E-mail',
            updated: 'Profile and token updated successfully',
            error: 'Error updating profile'
        },
        cinema: {
            name: 'Name of cinema',
            address: 'Cinema address',
            details: 'Cinema details',
            capacity: 'Capacity',
            telephone: 'Telephone',
            seats: 'Number of seats',
            rooms: 'Number of rooms',
            empty: 'No se han encontrado cines con este filtro'
        },
        filter: {
            cinema: 'Search cinemas',
            search: 'Type to search',
            rooms: 'Number of rooms',
            seats: 'Number of seats',
            clear: 'Clear filters',
            movie: 'Search films',
            rows: 'Number of rows',
            genres: 'Genres',
            hours: 'Available hours'
        },
        movie: {
            name: 'Film',
            genres: 'Genres',
            director: 'Director',
            synopsis: 'Synopsis',
            seats: 'Number of seats',
            room_number: 'Number of room',
            room_rows: 'Number of rows in the room',
            room_seats: 'Number of seats in the room',
            empty: 'Films not found with this filter',
            reservation: 'Hacer la reserva',
            detail: 'Show film'
        },
        genre: {
            empty: 'There are no associated genders',
            name: 'Genres'
        },
        movie_showing_times: {
            empty: 'Movie not available for today',
            name: 'Schedules for today'
        },
        booking: {
            screen_info: 'The screen is here',
            process_book: 'Process the reservation!',
            date: 'Booking',
            seats: 'Reserved seats',
            row: 'Row',
            seat: 'Seat',
            hour: 'Hour',
            list: 'List of your reservations'
        }
    }
}
