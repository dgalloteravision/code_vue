import globalTypes from '../types/global';
import types from '../types/municipio';

const state = {
  municipios: [],
  pagination: {
    current_page: 0,
    from: 0,
    last_page: 0,
    per_page: 0,
    to: 0,
    total: 0
  },
  query: {
    search: '',
    page: 1
  }
};

const actions = {
  [types.actions.fetchMunicipios]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.get('api/municipios?page='+object.page+'&token='+object.token).then(municipios => {
      commit(types.mutations.receivedMunicipios, {apiResponse: municipios});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de municipios a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.createMunicipio]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.post(`api/municipios?token=${object.token}`, {
      nombre_municipio: object.nombre_municipio
    }).then(municipio => {
      toastr.success('Municipio registrado', 'REGISTRO');
      commit(types.mutations.setMunicipios, {apiResponse: municipio, operacion: 1});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de creación de municipio a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.updateMunicipio]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.put(`api/municipios/${object.cod_municipio}?token=${object.token}`, {
      nombre_municipio: object.nombre_municipio
    }).then(municipio => {
      toastr.success('Municipio actualizado', 'ACTUALIZAR');
      commit(types.mutations.setMunicipios, {apiResponse: municipio, operacion: 2});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de actualización de municipio a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.deleteMunicipio]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.delete(`api/municipios/${object.cod_municipio}?token=${object.token}`).then(departamento => {
      toastr.success('Municipio eliminado', 'ELIMINAR');
      commit(types.mutations.setMunicipios, {apiResponse: object.cod_municipio, operacion: 3});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de eliminación de municipio a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
};

const getters = {
  [types.getters.search]: (state) => {
    return state.query.search;
  },
  [types.getters.pagination]: (state) => {
    return state.pagination;
  },
  [types.getters.municipios]: (state) => {
    let municipios = state.municipios;
    if(state.query.search) {
      municipios = municipios.filter(municipio => municipio.nombre_municipio.toLowerCase().includes(state.query.search));
    }
    return municipios;
  },
};

const mutations = {
  [types.mutations.receivedMunicipios]: (state, {apiResponse}) => {
    state.municipios = apiResponse.data.data.data;
    // Pagination propierties
    state.pagination.current_page = apiResponse.data.data.current_page;
    state.pagination.from = apiResponse.data.from;
    state.pagination.last_page = apiResponse.data.data.last_page;
    state.pagination.per_page = apiResponse.data.data.per_page;
    state.pagination.to = apiResponse.data.data.to;
    state.pagination.total = apiResponse.data.data.total;
  },
  [types.mutations.setMunicipios]: (state, {apiResponse, operacion}) => {
    switch (operacion) {
      case 1: // Crear
        state.municipios = [apiResponse.data.data, ...state.municipios];
      break;
      case 2: // Actualizar
        var index = state.municipios.findIndex(municipio => municipio.cod_municipio === apiResponse.data.data.cod_municipio);
        state.municipios = [...state.municipios.slice(0, index), apiResponse.data.data, ...state.municipios.slice(index+1)];
      break;
      case 3: // Eliminar
        var index = state.municipios.findIndex(municipio => municipio.cod_municipio === apiResponse);
        state.municipios = [...state.municipios.slice(0, index), ...state.municipios.slice(index+1)];
      break;
    }
  },
  [types.mutations.setSearch]: (state, query) => {
    state.query.search = query;
  },
  [types.mutations.clearFilter]: (state) => {
    state.query = {
      search: '',
      page: 1
    }
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
