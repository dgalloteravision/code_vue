import globalTypes from '../types/global';
import authTypes from '../types/auth';
import types from '../types/permiso';

const state = {
  permisos: [],
  query: {
    search: '',
    page: 1
  }
};

const actions = {
  [types.actions.fetchPermisos]: ({ commit }) => {
    commit(globalTypes.mutations.startProcessing);
    let token = authTypes.getters.token;
    axios.get('api/perfiles?page='+page+'token='+token).then(perfiles => {
      resolve(response);
      commit(types.mutations.receivedPermisos, {apiResponse: perfiles});
    }).catch(error => {
      reject(error);
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  }
};

const getters = {
  [types.getters.search]: (state) => {
    return state.query.search;
  },
  [types.getters.page]: (state) => {
    return state.query.page;
  },
  [types.getters.permisos]: (state) => {
    let permisos = state.permisos;
    if(state.query.search) {
      permisos = permisos.filter(permiso => permiso.nombre_permiso.toLowerCase().includes(state.query.search));
    }
    return permisos;
  },
};

const mutations = {
  [types.mutations.receivedPermisos]: (state, {apiResponse}) => {
    state.permisos = apiResponse.data;
  },
  [types.mutations.setSearch]: (state, query) => {
    state.query.search = query;
  },
  [types.mutations.clearFilter]: (state) => {
    state.query = {
      search: '',
      page: 1
    }
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
