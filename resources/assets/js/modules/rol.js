import globalTypes from '../types/global';
import authTypes from '../types/auth';
import types from '../types/rol';

const state = {
  perfiles: [],
  query: {
    search: '',
    page: 1
  }
};

const actions = {
  [types.actions.fetchPerfiles]: ({ commit }) => {
    commit(globalTypes.mutations.startProcessing);
    let token = authTypes.getters.token;
    axios.get('api/perfiles?page='+page+'token='+token).then(perfiles => {
      resolve(response);
      commit(types.mutations.receivedPerfiles, {apiResponse: perfiles});
    }).catch(error => {
      reject(error);
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  }
};

const getters = {
  [types.getters.search]: (state) => {
    return state.query.search;
  },
  [types.getters.page]: (state) => {
    return state.query.page;
  },
  [types.getters.perfiles]: (state) => {
    let perfiles = state.perfiles;
    if(state.query.search) {
      perfiles = perfiles.filter(perfil => perfil.nombre_rol.toLowerCase().includes(state.query.search));
    }
    return perfiles;
  },
};

const mutations = {
  [types.mutations.receivedPerfiles]: (state, {apiResponse}) => {
    state.perfiles = apiResponse.data;
  },
  [types.mutations.setSearch]: (state, query) => {
    state.query.search = query;
  },
  [types.mutations.clearFilter]: (state) => {
    state.query = {
      search: '',
      page: 1
    }
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
