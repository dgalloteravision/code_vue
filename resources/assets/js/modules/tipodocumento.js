import types from '../types/tipodocumento';
import globalTypes from '../types/global';

const state = {
  tiposdocumento: [],
  pagination: {
    current_page: 0,
    from: 0,
    last_page: 0,
    per_page: 0,
    to: 0,
    total: 0
  },
  query: {
    search: '',
    page: 1
  }
};

const actions = {
  [types.actions.fetchTiposdocumento]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.get('api/tiposdocumento?page='+object.page+'&token='+object.token).then(tiposdocumento => {
      commit(types.mutations.receivedTiposdocumento, {apiResponse: tiposdocumento});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de tipos de documento a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.create]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.post(`api/tiposdocumento?token=${object.token}`, {
      nombre_tipo_documento: object.nombre_tipo_documento
    }).then(tipodocumento => {
      toastr.success('Tipo de documento registrado', 'REGISTRO');
      commit(types.mutations.setTiposdocumento, {apiResponse: tipodocumento, operacion: 1});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de creación de tipo de documento a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.update]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.put(`api/tiposdocumento/${object.cod_tipo_documento}?token=${object.token}`, {
      nombre_tipo_documento: object.nombre_tipo_documento
    }).then(tipodocumento => {
      toastr.success('Tipo de documento actualizado', 'ACTUALIZAR');
      commit(types.mutations.setTiposdocumento, {apiResponse: tipodocumento, operacion: 2});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de actualización de tipo de documento a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.delete]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.delete(`api/tiposdocumento/${object.cod_tipo_documento}?token=${object.token}`).then(tipodocumento => {
      toastr.success('Tipo de documento eliminado', 'ELIMINAR');
      commit(types.mutations.setTiposdocumento, {apiResponse: object.cod_tipo_documento, operacion: 3});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de eliminación de tipo de documento a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
};

const getters = {
  [types.getters.searchTipodocumento]: (state) => {
    return state.query.search;
  },
  [types.getters.pagination]: (state) => {
    return state.pagination;
  },
  [types.getters.tiposdocumento]: (state) => {
    let tiposdocumento = state.tiposdocumento;
    if(state.query.search) {
      tiposdocumento = tiposdocumento.filter(tipodocumento => tipodocumento.nombre_tipo_documento.toLowerCase().includes(state.query.search));
    }
    return tiposdocumento;
  },
};

const mutations = {
  [types.mutations.receivedTiposdocumento]: (state, {apiResponse}) => {
    state.tiposdocumento = apiResponse.data.data.data;
    // Pagination propierties
    state.pagination.current_page = apiResponse.data.data.current_page;
    state.pagination.from = apiResponse.data.data.from;
    state.pagination.last_page = apiResponse.data.data.last_page;
    state.pagination.per_page = apiResponse.data.data.per_page;
    state.pagination.to = apiResponse.data.data.to;
    state.pagination.total = apiResponse.data.data.total;
  },
  [types.mutations.setTiposdocumento]: (state, {apiResponse, operacion}) => {
    switch (operacion) {
      case 1: // Crear
        state.tiposdocumento = [apiResponse.data.data, ...state.tiposdocumento];
      break;
      case 2: // Actualizar
        var index = state.tiposdocumento.findIndex(tipodocumento => tipodocumento.cod_tipo_documento === apiResponse.data.data.cod_tipo_documento);
        state.tiposdocumento = [...state.tiposdocumento.slice(0, index), apiResponse.data.data, ...state.tiposdocumento.slice(index+1)];
      break;
      case 3: // Eliminar
        var index = state.tiposdocumento.findIndex(tipodocumento => tipodocumento.cod_tipo_documento === apiResponse);
        state.tiposdocumento = [...state.tiposdocumento.slice(0, index), ...state.tiposdocumento.slice(index+1)];
      break;
    }
  },
  [types.mutations.setSearchTipodocumento]: (state, query) => {
    state.query.search = query;
  },
  [types.mutations.clearFilterTipodocumento]: (state) => {
    state.query = {
      search: '',
      page: 1
    }
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
