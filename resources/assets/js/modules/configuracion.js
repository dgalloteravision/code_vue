import types from '../types/configuracion';
import globalTypes from '../types/global';

const state = {
  configuraciones: [],
  query: {
    search: ''
  }
};

const actions = {
};

const getters = {

};

const mutations = {

};

export default {
  state,
  actions,
  getters,
  mutations
};
