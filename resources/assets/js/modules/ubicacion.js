import types from '../types/ubicacion';
import globalTypes from '../types/global';

const state = {
  ubicaciones: [],
  query: {
    search: ''
  }
};

const actions = {
};

const getters = {

};

const mutations = {

};

export default {
  state,
  actions,
  getters,
  mutations
};
