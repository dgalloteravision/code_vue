import types from '../types/estado';
import globalTypes from '../types/global';

const state = {
  estados: [],
  pagination: {
    current_page: 0,
    from: 0,
    last_page: 0,
    per_page: 0,
    to: 0,
    total: 0
  },
  query: {
    search: '',
    page: 1
  }
};

const actions = {
  [types.actions.fetchEstados]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    console.log(object.token);
    //let token = store.getters["auth:token"];
    axios.get(`api/estados?page=${object.page}&token=${object.token}`).then(estados => {
      commit(types.mutations.receivedEstados, {apiResponse: estados});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de estados a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.createEstado]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.post(`api/estados?token=${object.token}`, {
      nombre_estado: object.nombre_estado
    }).then(estado => {
      toastr.success('Estado registrado', 'REGISTRO');
      commit(types.mutations.setEstados, {apiResponse: estado, operacion: 1});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de creación de estado a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.updateEstado]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.put(`api/estados/${object.cod_estado}?token=${object.token}`, {
      nombre_estado: object.nombre_estado
    }).then(estado => {
      toastr.success('Estado actualizado', 'ACTUALIZAR');
      commit(types.mutations.setEstados, {apiResponse: estado, operacion: 2});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de actualización de estado a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.deleteEstado]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.delete(`api/estados/${object.cod_estado}?token=${object.token}`).then(estado => {
      toastr.success('Estado eliminado', 'ELIMINAR');
      commit(types.mutations.setEstados, {apiResponse: object.cod_estado, operacion: 3});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de eliminación de estado a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
};

const getters = {
  [types.getters.searchEstado]: (state) => {
    return state.query.search;
  },
  [types.getters.pagination]: (state) => {
    return state.pagination;
  },
  [types.getters.estados]: (state) => {
    let estados = state.estados;
    if(state.query.search) {
      estados = estados.filter(estado => estado.nombre_estado.toLowerCase().includes(state.query.search));
    }
    return estados;
  },
};

const mutations = {
  [types.mutations.receivedEstados]: (state, {apiResponse}) => {
    state.estados = apiResponse.data.data.data;
    // Pagination propierties
    state.pagination.current_page = apiResponse.data.data.current_page;
    state.pagination.from = apiResponse.data.data.from;
    state.pagination.last_page = apiResponse.data.data.last_page;
    state.pagination.per_page = apiResponse.data.data.per_page;
    state.pagination.to = apiResponse.data.data.to;
    state.pagination.total = apiResponse.data.data.total;
  },
  [types.mutations.setEstados]: (state, {apiResponse, operacion}) => {
    switch (operacion) {
      case 1: // Crear
        state.estados = [apiResponse.data.data, ...state.estados];
      break;
      case 2: // Actualizar
        var index = state.estados.findIndex(estado => estado.cod_estado === apiResponse.data.data.cod_estado);
        state.estados = [...state.estados.slice(0, index), apiResponse.data.data, ...state.estados.slice(index+1)];
      break;
      case 3: // Eliminar
        var index = state.estados.findIndex(estado => estado.cod_estado === apiResponse);
        state.estados = [...state.estados.slice(0, index), ...state.estados.slice(index+1)];
      break;
    }
  },
  [types.mutations.setSearchEstado]: (state, query) => {
    state.query.search = query;
  },
  [types.mutations.clearFilterEstado]: (state) => {
    state.query = {
      search: '',
      page: 1
    }
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
