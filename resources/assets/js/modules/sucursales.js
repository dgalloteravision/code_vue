import globalTypes from '../types/global';
import authTypes from '../types/auth';
import types from '../types/sucursales';

const state = {
  sucursales: [],
  query: {
    search: '',
    page: 1
  }
};

const actions = {
  [types.actions.fetchSucursales]: ({ commit }) => {
    commit(globalTypes.mutations.startProcessing);
    let token = authTypes.getters.token;
    axios.get('api/sucursales?page='+page+'token='+token).then(sucursales => {
      resolve(response);
      commit(types.mutations.receivedSucursales, {apiResponse: sucursales});
    }).catch(error => {
      reject(error);
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  }
};

const getters = {
  [types.getters.search]: (state) => {
    return state.query.search;
  },
  [types.getters.page]: (state) => {
    return state.query.page;
  },
  [types.getters.sucursales]: (state) => {
    let sucursales = state.sucursales;
    if(state.query.search) {
      sucursales = sucursales.filter(sucursal => sucursal.nombre_sucursal.toLowerCase().includes(state.query.search));
    }
    return sucursales;
  },
};

const mutations = {
  [types.mutations.receivedSucursales]: (state, {apiResponse}) => {
    state.sucursales = apiResponse.data;
  },
  [types.mutations.setSearch]: (state, query) => {
    state.query.search = query;
  },
  [types.mutations.clearFilter]: (state) => {
    state.query = {
      search: '',
      page: 1
    }
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
