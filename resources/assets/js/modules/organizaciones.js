import globalTypes from '../types/global';
import authTypes from '../types/auth';
import types from '../types/organizaciones';

const state = {
  organizaciones: [],
  query: {
    search: '',
    page: 1
  }
};

const actions = {
  [types.actions.fetchOrganizaciones]: ({ commit }) => {
    commit(globalTypes.mutations.startProcessing);
    let token = authTypes.getters.token;
    axios.get('api/organizaciones?page='+page+'token='+token).then(organizaciones => {
      resolve(response);
      commit(types.mutations.receivedOrganizaciones, {apiResponse: organizaciones});
    }).catch(error => {
      reject(error);
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  }
};

const getters = {
  [types.getters.search]: (state) => {
    return state.query.search;
  },
  [types.getters.page]: (state) => {
    return state.query.page;
  },
  [types.getters.organizaciones]: (state) => {
    let organizaciones = state.organizaciones;
    if(state.query.search) {
      organizaciones = organizaciones.filter(organizacion => organizacion.nombre_organizacion.toLowerCase().includes(state.query.search));
    }
    return organizaciones;
  },
};

const mutations = {
  [types.mutations.receivedOrganizaciones]: (state, {apiResponse}) => {
    state.organizaciones = apiResponse.data;
  },
  [types.mutations.setSearch]: (state, query) => {
    state.query.search = query;
  },
  [types.mutations.clearFilter]: (state) => {
    state.query = {
      search: '',
      page: 1
    }
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
