import types from '../types/pregunta';
import globalTypes from '../types/global';

const state = {
  preguntas: [],
  query: {
    search: ''
  }
};

const actions = {
};

const getters = {

};

const mutations = {

};

export default {
  state,
  actions,
  getters,
  mutations
};
