import globalTypes from '../types/global';
import types from '../types/departamento';

const state = {
  departamentos: [],
  pagination: {
    current_page: 0,
    from: 0,
    last_page: 0,
    per_page: 0,
    to: 0,
    total: 0
  },
  query: {
    search: '',
    page: 1
  }
};

const actions = {
  [types.actions.fetchDepartamentos]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.get('api/departamentos?page='+object.page+'&token='+object.token).then(departamentos => {
      commit(types.mutations.receivedDepartamentos, {apiResponse: departamentos});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de departamentos a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.createDepartamento]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.post(`api/departamentos?token=${object.token}`, {
      nombre_departamento: object.nombre_departamento
    }).then(departamento => {
      toastr.success('Departamento registrado', 'REGISTRO');
      commit(types.mutations.setDepartamentos, {apiResponse: departamento, operacion: 1});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de creación de departamento a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.updateDepartamento]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.put(`api/departamentos/${object.cod_departamento}?token=${object.token}`, {
      nombre_departamento: object.nombre_departamento
    }).then(departamento => {
      toastr.success('Departamento actualizado', 'ACTUALIZAR');
      commit(types.mutations.setDepartamentos, {apiResponse: departamento, operacion: 2});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de actualización de departamento a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
  [types.actions.deleteDepartamento]: ({ commit }, object) => {
    commit(globalTypes.mutations.startProcessing);
    axios.delete(`api/departamentos/${object.cod_departamento}?token=${object.token}`).then(departamento => {
      toastr.success('Departamento eliminado', 'ELIMINAR');
      commit(types.mutations.setDepartamentos, {apiResponse: object.cod_departamento, operacion: 3});
    }).catch(error => {
      console.log(error);
      toastr.error('Error en servicio', 'La solicitud de eliminación de departamento a presentado inconvenientes!!');
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  },
};

const getters = {
  [types.getters.search]: (state) => {
    return state.query.search;
  },
  [types.getters.pagination]: (state) => {
    return state.pagination;
  },
  [types.getters.departamentos]: (state) => {
    let departamentos = state.departamentos;
    if(state.query.search) {
      departamentos = departamentos.filter(departamento => departamento.nombre_departamento.toLowerCase().includes(state.query.search));
    }
    return departamentos;
  },
};

const mutations = {
  [types.mutations.receivedDepartamentos]: (state, {apiResponse}) => {
    state.departamentos = apiResponse.data.data.data;
    // Pagination propierties
    state.pagination.current_page = apiResponse.data.data.current_page;
    state.pagination.from = apiResponse.data.data.from;
    state.pagination.last_page = apiResponse.data.data.last_page;
    state.pagination.per_page = apiResponse.data.data.per_page;
    state.pagination.to = apiResponse.data.data.to;
    state.pagination.total = apiResponse.data.data.total;
  },
  [types.mutations.setDepartamentos]: (state, {apiResponse, operacion}) => {
    switch (operacion) {
      case 1: // Crear
        state.departamentos = [apiResponse.data.data, ...state.departamentos];
      break;
      case 2: // Actualizar
        var index = state.departamentos.findIndex(departamento => departamento.cod_departamento === apiResponse.data.data.cod_departamento);
        state.departamentos = [...state.departamentos.slice(0, index), apiResponse.data.data, ...state.departamentos.slice(index+1)];
      break;
      case 3: // Eliminar
        var index = state.departamentos.findIndex(departamento => departamento.cod_departamento === apiResponse);
        state.departamentos = [...state.departamentos.slice(0, index), ...state.departamentos.slice(index+1)];
      break;
    }
  },
  [types.mutations.setSearch]: (state, query) => {
    state.query.search = query;
  },
  [types.mutations.clearFilter]: (state) => {
    state.query = {
      search: '',
      page: 1
    }
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
