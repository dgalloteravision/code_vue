import globalTypes from '../types/global';
import authTypes from '../types/auth';
import types from '../types/usuario';

const state = {
  usuarios: [],
  query: {
    search: '',
    page: 1
  }
};

const actions = {
  [types.actions.fetchUsuarios]: ({ commit }) => {
    commit(globalTypes.mutations.startProcessing);
    let token = authTypes.getters.token;
    axios.get('api/usuarios?page='+page+'token='+token).then(usuarios => {
      resolve(response);
      commit(types.mutations.receivedUsuarios, {apiResponse: usuarios});
    }).catch(error => {
      reject(error);
    }).then(function() {
      commit(globalTypes.mutations.stopProcessing);
    });
  }
};

const getters = {
  [types.getters.search]: (state) => {
    return state.query.search;
  },
  [types.getters.page]: (state) => {
    return state.query.page;
  },
  [types.getters.usuarios]: (state) => {
    let usuarios = state.usuarios;
    if(state.query.search) {
      usuarios = usuarios.filter(usuario => usuario.name.toLowerCase().includes(state.query.search));
    }
    return usuarios;
  },
};

const mutations = {
  [types.mutations.receivedUsuarios]: (state, {apiResponse}) => {
    state.usuarios = apiResponse.data;
  },
  [types.mutations.setSearch]: (state, query) => {
    state.query.search = query;
  },
  [types.mutations.clearFilter]: (state) => {
    state.query = {
      search: '',
      page: 1
    }
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
