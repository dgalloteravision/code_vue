import types from '../types/auth';
import globalTypes from '../types/global';

const state = {
    user: null,
    logged: !!window.localStorage.getItem('_user'),
    token: null,
    isregisterform: false
};

const actions = {
    [types.actions.login]: ({ commit }, userInput) => {
        commit(globalTypes.mutations.startProcessing);
        return new Promise((resolve, reject) => {
            axios.post('api/login', {
                email: userInput.email,
                password: userInput.password
            }).then(user => {
                console.log(user);
                toastr.success('Logueado con exito', 'LOGIN');
                window.localStorage.setItem('_user', user.data.result.token);
                commit(types.mutations.setUser);
                resolve(user);
            }).catch(error => {
                console.log(error);
                toastr.error('Datos incorrectos', 'Credenciales Invalidas!');
            }).then(function() {
                commit(globalTypes.mutations.stopProcessing);
            });
        })
    },
    [types.actions.register]: ({ commit }, userInput) => {
        commit(globalTypes.mutations.startProcessing);
        return new Promise((resolve, reject) => {
            axios.post('api/signup', {
                email: userInput.email,
                password: userInput.password
            }).then(user => {
                toastr.success('Cuenta registrada con exito', 'REGISTRO');
                resolve(user);
            }).catch(error => {
                toastr.error('Datos incorrectos', 'Registro Invalido!');
            }).then(function() {
                commit(globalTypes.mutations.stopProcessing);
            });
        })
    },
    [types.actions.updateProfile]: ({ commit }, userInput) => {
        commit(globalTypes.mutations.startProcessing);
        return new Promise((resolve, reject) => {
            Vue.http.put('profile', { user: userInput })
                .then(user => {
                    window.localStorage.setItem('_user', user.data.token);
                    commit(types.mutations.setUser);
                    resolve(user);
                })
                .catch(error => {
                    reject(error);
                })
                .finally(() => {
                    commit(globalTypes.mutations.stopProcessing);
                })
        })
    },
    [types.actions.reset]: ({ commit }, userInput) => {
        commit(globalTypes.mutations.startProcessing);
        return new Promise((resolve, reject) => {
            axios.post('api/reset', {
                email: userInput.email
            }).then(reset => {
                console.log(reset);
                toastr.success('Recordatorio enviado con exito', 'RECORDAR CONTRASEÑA');
                resolve(reset);
            }).catch(error => {
                console.log(error);
                reject(error);
                toastr.error('Datos incorrectos', 'Correo es invalido o no existe!');
            }).then(function() {
                commit(globalTypes.mutations.stopProcessing);
            });
        })
    },
    [types.actions.logout]: ({ commit }) => {
        commit(globalTypes.mutations.startProcessing);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + window.localStorage.getItem('_user');
        axios.post('api/logout', {}).then(user => {
            window.localStorage.removeItem('_user');
            commit(types.mutations.setUser);
        }).catch(error => {
            reject(error);
        }).then(function() {
            commit(globalTypes.mutations.stopProcessing);
        });
    },
    [types.actions.isregisterform]: ({ commit }) => {
        commit(types.mutations.setIsregisterform);
    }
};

const getters = {
    //obtenemos el usuario
    [types.getters.user]: (state) => {
        return state.user;
    },
    //está logueado?
    [types.getters.logged]: (state) => {
        return state.logged;
    },
    // Token obtenido tras logueo de usuario
    [types.getters.token]: (state) => {
        return state.token;
    },
    // ingreso en la vista de registro
    [types.getters.isregisterform]: (state) => {
        return state.isregisterform;
    },
};

const mutations = {
    //establecemos el user a través del token jwt
    [types.mutations.setUser]: (state) => {
        if(window.localStorage.getItem('_user')) {
            const token = window.localStorage.getItem('_user');
            const jwtDecode = require('jwt-decode');
            state.user = jwtDecode(token);
            state.logged = true;
            state.isregister = false;
            state.token = token;
        } else {
            state.logged = false;
            state.user = null;
            state.token = null;
            state.isregister = false;
        }
    },
    // Establecemos el estado del usuario
    [types.mutations.setLogged]: (state, logged) => {
        state.logged = logged;
    },
    // Establecemos el token obtenido
    [types.mutations.setToken]: (state, token) => {
        state.token = token;
    },
    // Establecemos si la ruta es registro
    [types.mutations.setIsregisterform]: (state) => {
        state.isregisterform = !state.isregisterform;
    },
};

export default {
    state,
    actions,
    getters,
    mutations
};
