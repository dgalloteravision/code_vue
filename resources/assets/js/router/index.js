import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);

//types and components
import authTypes from '../types/auth';
import Login from '../components/Auth/Login';
import Register from '../components/Auth/Register';
import Reset from '../components/Auth/Reset';
import Parametrizaciones from '../components/Parametrizaciones/Parametrizaciones';
import Organizaciones from '../components/Organizaciones/Organizaciones';
import Seguridad from '../components/Seguridad/Seguridad';
import Reportes from '../components/Reportes/Reportes';

//global store
import store from '../store';
//.global store

//configurar el router
const router = new Router({
    routes: [{
            path: '/',
            name: 'dashboard',
            component: Login,
            meta: { Auth: false, title: 'Dashboard' },
            beforeEnter: (to, from, next) => {
                if(!store.state.authModule.logged) {
                    next({ path: '/login' });
                } else {
                    next();
                }
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { Auth: false, title: 'Iniciar Sesión' },
            beforeEnter: (to, from, next) => {
                if(store.state.authModule.logged) {
                    next({ path: '/' });
                } else {
                    next();
                }
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: { Auth: true, title: 'Registrarme' },
            beforeEnter: (to, from, next) => {
                if(!store.state.authModule.logged) {
                    next({ path: '/login' });
                } else {
                    next();
                }
            }
        },
        {
            path: '/reset',
            name: 'reset',
            component: Reset,
            meta: { Auth: false, title: 'Recordar contraseña' },
            beforeEnter: (to, from, next) => {
                if(store.state.authModule.logged) {
                    next({ path: '/' });
                } else {
                    next();
                }
            }
        },
        {
          path: '/parametrizaciones',
          name: 'parametrizaciones',
          component: Parametrizaciones,
          meta: { Auth: true, title: 'Parametrizaciones' },
          beforeEnter: (to, from, next) => {
              if(!store.state.authModule.logged) {
                  next({ path: '/login' });
              } else {
                  next();
              }
          }
        },
        {
          path: '/organizaciones',
          name: 'organizaciones',
          component: Organizaciones,
          meta: { Auth: true, title: 'Organizaciones' },
          beforeEnter: (to, from, next) => {
              if(!store.state.authModule.logged) {
                  next({ path: '/login' });
              } else {
                  next();
              }
          }
        },
        {
          path: '/seguridad',
          name: 'seguridad',
          component: Seguridad,
          meta: { Auth: true, title: 'Seguridad' },
          beforeEnter: (to, from, next) => {
              if(!store.state.authModule.logged) {
                  next({ path: '/login' });
              } else {
                  next();
              }
          }
        },
        {
          path: '/reportes',
          name: 'reportes',
          component: Reportes,
          meta: { Auth: true, title: 'Reportes' },
          beforeEnter: (to, from, next) => {
              if(!store.state.authModule.logged) {
                  next({ path: '/login' });
              } else {
                  next();
              }
          }
        },
    ]
});
//.configurar el router

//para cada cambio de ruta
router.beforeEach((to, from, next) => {
    document.title = to.meta.title;
    if(to.meta.Auth && !store.state.authModule.logged) {
        next({ path: '/login' });
    } else {
        if(store.state.authModule.logged) {
            store.commit(authTypes.mutations.setUser);
        }
        next();
    }

    if(to.name == "register") {
        store.commit(authTypes.mutations.setIsregisterform);
    }
});
//.para cada cambio de ruta

export default router;
