require('./bootstrap');

window.Vue = require('vue');

//vue resource
import VueResource from 'vue-resource';
window.VueResource = VueResource;
Vue.use(VueResource);
Vue.http.options.root = 'http://procredito.com/api/';
Vue.http.interceptors.push((request, next) => {
  let token = (window.localStorage.getItem('_token')) ? window.localStorage.getItem('_token') : document.head.querySelector('meta[name="csrf-token"]');
  console.log('token',token);
  request.headers.set('Authorization', `Bearer ${token}`);
  next();
});

import VModal from 'vue-js-modal';
window.VModal = VModal;
Vue.use(VModal, { dynamic: true, injectModalsContainer: true });

import BootstrapVue from 'bootstrap-vue'
window.BootstrapVue = BootstrapVue;
Vue.use(BootstrapVue);

// sweetalert
import swal from 'sweetalert';
window.swal = swal;

// vuex
import Vuex from 'vuex';
window.Vuex = Vuex;

// blockui
import BlockUI from 'vue-blockui';
window.BlockUI = BlockUI;
Vue.use(BlockUI);

// Importando libreria notificación
import toastr from 'toastr';
window.toastr = toastr;

// Importando libreria formateo de fechas en el cliente
import moment from 'moment';
window.moment = moment;
moment.locale('es');

// vue-tables-2
import {ClientTable} from 'vue-tables-2';
window.ClientTable = ClientTable;
Vue.use(ClientTable, {}, false, 'bootstrap4', 'default');

// Importando el store de la aplicación
import store from './store';
window.store = store;

// Importando el store de la aplicación
import router from './router';
window.router = router;

// Importando el componente principal
import App from './App.vue';

// vue traducciones
import VueI18n from 'vue-i18n';
window.VueI18n = VueI18n;
Vue.use(VueI18n);

import VueI18nDirectives from 'vue-i18n-directives';
window.VueI18nDirectives = VueI18nDirectives;
Vue.use(VueI18nDirectives);

import messages from './translations';

const i18n = new VueI18n({
  locale: store.state.language,
  messages
});

window.i18n = i18n;

const app = new Vue({
    el: '#app',
    store,
    router,
    i18n,
    render: h => h(App),
});
