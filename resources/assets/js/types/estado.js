import namespace from '../utils/namespace'

export default namespace('estado', {
  getters: [
    'estados',
    'searchEstado',
    'pagination'
  ],
  actions: [
    'fetchEstados',
    'createEstado',
    'updateEstado',
    'deleteEstado'
  ],
  mutations: [
    'receivedEstados',
    'setEstados',
    'setSearchEstado',
    'clearFilterEstado',
  ]
});
