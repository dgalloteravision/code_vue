import namespace from '../utils/namespace'

export default namespace('organizaciones', {
  getters: [
    'organizaciones',
    'search'
  ],
  actions: [
    'fetchOrganizaciones',
    'createOrganizacion',
    'updateOrganizacion',
    'deleteOrganizacion'
  ],
  mutations: [
    'receivedOrganizaciones',
    'setSearch',
    'clearFilter'
  ]
});
