import namespace from '../utils/namespace'

export default namespace('rol', {
  getters: [
    'perfiles',
    'search'
  ],
  actions: [
    'fetchPerfiles',
    'createPerfil',
    'updatePerfil',
    'deletePerfil'
  ],
  mutations: [
    'receivedPerfiles',
    'setSearch',
    'clearFilter'
  ]
});
