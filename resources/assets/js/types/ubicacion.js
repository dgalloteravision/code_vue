import namespace from '../utils/namespace'

export default namespace('ubicacion', {
  getters: [
    'ubicaciones',
    'search'
  ],
  actions: [
    'fetchUbicaciones',
    'createUbicacion',
    'updateUbicacion',
    'deleteUbicacion'
  ],
  mutations: [
    'receivedUbicacion',
    'setSearch',
    'clearFilter'
  ]
});
