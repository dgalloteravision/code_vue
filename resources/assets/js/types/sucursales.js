import namespace from '../utils/namespace'

export default namespace('sucursales', {
  getters: [
    'sucursales',
    'search'
  ],
  actions: [
    'fetchSucursales',
    'createSucursal',
    'updateSucursal',
    'deleteSucursal'
  ],
  mutations: [
    'receivedSucursales',
    'setSearch',
    'clearFilter'
  ]
});
