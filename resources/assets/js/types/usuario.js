import namespace from '../utils/namespace'

export default namespace('usuario', {
  getters: [
    'usuarios',
    'search'
  ],
  actions: [
    'fetchUsuarios',
    'createUsuario',
    'updateUsuario',
    'deleteUsuario'
  ],
  mutations: [
    'receivedUsuarios',
    'setSearch',
    'clearFilter'
  ]
});
