import namespace from '../utils/namespace'

export default namespace('permiso', {
  getters: [
    'permisos',
    'search'
  ],
  actions: [
    'fetchPermisos',
    'createPermiso',
    'updatePermiso',
    'deletePermiso'
  ],
  mutations: [
    'receivedPermisos',
    'setSearch',
    'clearFilter'
  ]
});
