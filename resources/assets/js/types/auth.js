import namespace from '../utils/namespace'

export default namespace('auth', {
    getters: [
        'user',
        'logged',
        'token',
        'isregisterform'
    ],
    actions: [
        'login',
        'register',
        'logout',
        'reset',
        'updateProfile',
        'isregisterform'
    ],
    mutations: [
        'setUser',
        'setLogged',
        'setToken',
        'setIsregisterform'
    ]
});