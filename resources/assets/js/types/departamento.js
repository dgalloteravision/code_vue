import namespace from '../utils/namespace'

export default namespace('departamento', {
  getters: [
    'departamentos',
    'search',
    'pagination'
  ],
  actions: [
    'fetchDepartamentos',
    'createDepartamento',
    'updateDepartamento',
    'deleteDepartamento'
  ],
  mutations: [
    'receivedDepartamentos',
    'setDepartamentos',
    'setSearch',
    'clearFilter'
  ]
});
