import namespace from '../utils/namespace'

export default namespace('pregunta', {
  getters: [
    'preguntas',
    'search'
  ],
  actions: [
    'fetchPreguntas',
    'createPregunta',
    'updatePregunta',
    'deletePregunta'
  ],
  mutations: [
    'receivedPregunta',
    'setSearch',
    'clearFilter'
  ]
});
