import namespace from '../utils/namespace'

export default namespace('tipodocumento', {
  getters: [
    'tiposdocumento',
    'searchTipodocumento',
    'pagination'
  ],
  actions: [
    'fetchTiposdocumento',
    'create',
    'update',
    'delete'
  ],
  mutations: [
    'receivedTiposdocumento',
    'setTiposdocumento',
    'setSearchTipodocumento',
    'clearFilterTipodocumento'
  ]
});
