import namespace from '../utils/namespace'

export default namespace('municipio', {
  getters: [
    'municipios',
    'search',
    'pagination'
  ],
  actions: [
    'fetchMunicipios',
    'createMunicipio',
    'updateMunicipio',
    'deleteMunicipio'
  ],
  mutations: [
    'receivedMunicipios',
    'setMunicipios',
    'setSearch',
    'clearFilter'
  ]
});
