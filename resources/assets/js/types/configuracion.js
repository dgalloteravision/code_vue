import namespace from '../utils/namespace'

export default namespace('configuracion', {
  getters: [
    'configuraciones',
    'search'
  ],
  actions: [
    'fetchConfiguraciones',
    'createConfiguracion',
    'updateConfiguracion',
    'deleteConfiguracion'
  ],
  mutations: [
    'receivedConfiguracion',
    'setSearch',
    'clearFilter'
  ]
});
