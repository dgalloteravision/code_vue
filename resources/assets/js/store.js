import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
// modulos y tipos
import globalTypes from './types/global';
// vee-validate
import VeeValidate, {Validator} from 'vee-validate';
import validatorEs from './validator/es';
import validatorEn from './validator/en';
Validator.localize('es', validatorEs);
Vue.use(VeeValidate);
// Importando el modulo de autenticación
import authModule from './modules/auth';
// Importando el modulo de estados
import estadoModule from './modules/estado';
// Importando el modulo de tipos documento
import tipodocumentoModule from './modules/tipodocumento';
// Importando el modulo de departamentos
import departamentoModule from './modules/departamento';
// Importando el modulo de municipios
import municipioModule from './modules/municipio';
// Importando el modulo de organizaciones
import organizacionesModule from './modules/organizaciones';
// Importando el modulo de sucursales
import sucursalesModule from './modules/sucursales';
// Importando el modulo de permisos
import permisoModule from './modules/permiso';
// Importando el modulo de roles
import rolModule from './modules/rol';
// Importando el modulo de usuarios
import usuarioModule from './modules/usuario';

//almacén global de datos con vuex
const store = new Vuex.Store({
  state: {
    processing: false,
    language: 'es'
  },
  actions: {
    [globalTypes.actions.changeLanguage]: ({commit}, lang) => {
      commit(globalTypes.mutations.setLanguage, lang);
      switch (lang) {
        case 'en': {
          Validator.localize('en', validatorEn);
          break;
        }
        case 'es': {
          Validator.localize('es', validatorEs);
          break;
        }
      }
    }
  },
  getters: {
    [globalTypes.getters.processing]: state => state.processing,
    [globalTypes.getters.language]: state => state.language,
  },
  mutations: {
    [globalTypes.mutations.startProcessing] (state) {
      state.processing = true;
    },
    [globalTypes.mutations.stopProcessing] (state) {
      state.processing = false;
    },
    [globalTypes.mutations.setLanguage] (state, lang) {
      state.language = lang;
    }
  },
  modules: {
    authModule,
    estadoModule,
    tipodocumentoModule,
    departamentoModule,
    municipioModule,
    //organizacionesModule,
    //sucursalesModule,
    //permisoModule,
    //rolModule,
    //usuarioModule
  }
});

export default store;
