@extends('layouts.app')

@section('content')

<div id="page-container" class="side-trans-enabled">
  <main id="main-container">
    @if (session('status'))
      <div class="row no-gutters justify-content-center bg-body-dark">
        <div class="col-md-6">
        <div class="alert alert-success d-flex align-items-center" role="alert">
          <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
          </div>
          <div class="flex-fill ml-3">
            <p class="mb-0">{{ session('status') }}</p>
          </div>
        </div>
      </div>
    @endif
    <div class="row no-gutters justify-content-center bg-body-dark">
      <div class="hero-static col-sm-10 col-md-8 col-xl-6 d-flex align-items-center p-2 px-sm-0">
          <div class="block block-rounded block-transparent block-fx-pop w-100 mb-0 overflow-hidden bg-image" style="background-image: url('{{ asset('img/logofenalco.svg')}}'); background-size: 250px; background-position: 50px 50px; background-repeat: no-repeat;">
              <div class="row no-gutters">
                  <div class="col-md-6 order-md-1 bg-white">
                      <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
                          <div class="mb-2 text-center">
                              <a class="link-fx text-warning font-w700 font-size-h1" href="{{ route('home') }}">
                                  <span class="text-dark">Pro</span><span class="text-warning">credito</span>
                              </a>
                              <p class="text-uppercase font-w700 font-size-sm text-muted">Recordatorio de contraseña</p>
                          </div>
                          <form class="js-validation-reminder" method="POST" action="{{ route('password.email') }}" novalidate="novalidate">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" class="form-control form-control-alt is-invalid" id="email" name="email" placeholder="Email" value="{{ old('email') }}" aria-describedby="email-error" aria-invalid="true" required>
                                @if ($errors->has('email'))
                                  <div id="email-error" class="invalid-feedback animated fadeIn">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </div>
                                @endif
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-block btn-hero-warning">
                                    <i class="fa fa-fw fa-reply mr-1"></i> Restablecer Contraseña
                                </button>
                            </div>
                          </form>
                      </div>
                  </div>
                  <div class="col-md-6 order-md-0 bg-gd-sun-op d-flex align-items-center">
                      <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6 text-center">
                          <p class="font-size-h2 font-w700 text-white mb-0">
                            Este acceso es para empresarios PROCRÉDITO.
                          </p>
                          <p class="font-size-h3 font-w600 text-white-75 mb-0">
                            Fenalco, Antioquia
                          </p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </main>
</div>

<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection
