@extends('layouts.app')

@section('content')

<div id="page-container" class="side-trans-enabled">
  <main id="main-container">
    <div class="row no-gutters justify-content-center bg-body-dark">
      <div class="hero-static col-sm-10 col-md-8 col-xl-6 d-flex align-items-center p-2 px-sm-0">
        <div class="block block-rounded block-transparent block-fx-pop w-100 mb-0 overflow-hidden bg-video" data-vide-bg="assets/media/videos/city_night" data-vide-options="posterType: jpg" style="position: relative;"><div style="position: absolute; z-index: -1; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 50% 50%; background-image: none;"><video autoplay="" loop="" muted="" style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 50%; transform: translate(-50%, -50%); visibility: visible; opacity: 1; width: auto; height: 382px;"><source src="{{ asset('media/city_night.mp4')}}" type="video/mp4"><source src="assets/media/videos/city_night.webm" type="video/webm"><source src="assets/media/videos/city_night.ogv" type="video/ogg"></video></div>
            <div class="row no-gutters">
                <div class="col-md-6 order-md-1 bg-white">
                    <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
                        <div class="mb-2 text-center">
                            <a class="link-fx text-danger font-w700 font-size-h1" href="{{ route('home') }}">
                                <span class="text-dark">Pro</span><span class="text-danger">credito</span>
                            </a>
                            <p class="text-uppercase font-w700 font-size-sm text-muted">Restablecer la contraseña</p>
                        </div>
                        <form class="js-validation-lock" method="POST" action="{{ route('password.request') }}" novalidate="novalidate">
                          {{ csrf_field() }}
                          <input type="hidden" name="token" value="{{ $token }}">
                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control form-control-alt is-invalid" id="email" name="email" placeholder="Email.." aria-describedby="email-error" aria-invalid="true" required autofocus>
                            @if ($errors->has('email'))
                              <div id="email-error" class="invalid-feedback animated fadeIn">
                                <strong>{{ $errors->first('email') }}</strong>
                              </div>
                            @endif
                          </div>
                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control form-control-alt is-invalid" name="password" placeholder="New password.." aria-describedby="password-error" aria-invalid="true" required>
                            @if ($errors->has('password'))
                              <div id="password-error" class="invalid-feedback animated fadeIn">
                                    <strong>{{ $errors->first('password') }}</strong>
                              </div>
                            @endif
                          </div>
                          <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input id="password-confirm" type="password" class="form-control form-control-alt is-invalid" name="password_confirmation" placeholder="New password confirmation.." aria-describedby="password-confirm-error" aria-invalid="true" required>
                            @if ($errors->has('password_confirmation'))
                            <div id="password-confirm-error" class="invalid-feedback animated fadeIn">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                              </span>
                            @endif
                          </div>
                          <div class="form-group text-center">
                              <button type="submit" class="btn btn-block btn-hero-danger">
                                  <i class="fa fa-fw fa-lock-open mr-1"></i> Reset Password
                              </button>
                          </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 order-md-0 bg-danger-op d-flex align-items-center">
                    <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6 text-center">
                        <img class="img-avatar img-avatar-thumb" src="{{ asset('img/logo.svg')}}" alt="">
                        <a class="d-block text-white font-size-lg font-w600 mt-3" href="javascript:void(0)">Procredito</a>
                        <div class="text-white-75">Fenalco, Antioquia</div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </main>
</div>

<!--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection
