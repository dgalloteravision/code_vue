@extends('layouts.app')

@section('content')

<div id="page-container" class="side-trans-enabled">
  <main id="main-container">
    <div class="row no-gutters justify-content-center bg-body-dark">
        <div class="hero-static col-sm-10 col-md-8 col-xl-6 d-flex align-items-center p-2 px-sm-0">
            <div class="block block-rounded block-transparent block-fx-pop w-100 mb-0 overflow-hidden bg-image" style="background-image: url('{{ asset('img/logofenalco.svg')}}'); background-size: 250px; background-position: 50px 50px; background-repeat: no-repeat;">
                <div class="row no-gutters">
                    <div class="col-md-6 order-md-1 bg-white">
                        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
                            <div class="mb-2 text-center">
                                <a class="link-fx font-w700 font-size-h1" href="{{ route('home') }}">
                                    <span class="text-dark">Pro</span><span class="text-primary">credito</span>
                                </a>
                                <p class="text-uppercase font-w700 font-size-sm text-muted">Login</p>
                            </div>
                            <form class="js-validation-signin" method="POST" action="{{ route('login') }}" novalidate="novalidate">
                              {{ csrf_field() }}
                              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" class="form-control form-control-alt is-invalid" id="email" name="email" placeholder="Email" aria-describedby="login-username-error" value="{{ old('email') }}" required autofocus>
                                <!--<div id="login-username-error" class="invalid-feedback animated fadeIn" style="display:none;">Por favor ingrese un nombre de usuario</div>-->
                                @if ($errors->has('email'))
                                  <div id="login-username-error" class="invalid-feedback animated fadeIn">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </div>
                                @endif
                              </div>
                              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input type="password" class="form-control form-control-alt is-invalid" id="password" name="password" placeholder="Password" aria-describedby="login-password-error" aria-invalid="true" required>
                                <!--<div id="login-password-error" class="invalid-feedback animated fadeIn" style="display:none;">Por favor ingrese una contraseña</div>-->
                                @if ($errors->has('password'))
                                  <div id="login-password-error" class="invalid-feedback animated fadeIn">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </div>
                                @endif
                              </div>
                              <div class="form-group">
                                  <div class="col-md-6 col-md-offset-4">
                                      <div class="checkbox">
                                          <label>
                                              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <button type="submit" class="btn btn-block btn-hero-primary">
                                      <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Login
                                  </button>
                                  <p class="mt-3 mb-0 d-lg-flex justify-content-lg-between">
                                    <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1" href="{{ route('password.request') }}">
                                        <i class="fa fa-exclamation-triangle text-muted mr-1"></i> ¿Olvidó su contraseña?
                                    </a>
                                  </p>
                                  <p>
                                    <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1" href="{{ route('register') }}">
                                        <i class="fa fa-plus text-muted mr-1"></i> ¿Registrarse en procredito?
                                    </a>
                                  </p>
                              </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 order-md-0 bg-primary-dark-op d-flex align-items-center">
                        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
                            <div class="media">
                                <a class="img-link mr-3" href="javascript:void(0)">
                                    <img class="img-avatar img-avatar-thumb" src="{{ asset('img/logo.svg')}}" alt="Procredito">
                                </a>
                                <div class="media-body">
                                    <p class="text-white font-w600 mb-1">
                                      Este acceso es para empresarios PROCRÉDITO.
                                      Para conocer más información sobre este servicio, haga clic en Inicio.
                                    </p>
                                    <a class="text-white-75 font-w600" href="https://www.fenalcoantioquia.com/" target="_blank">Fenalco, Antioquia</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </main>
</div>

<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection
