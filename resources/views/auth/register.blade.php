@extends('layouts.app')

@section('content')

<div id="page-container" class="side-trans-enabled">
  <main id="main-container">
    <div class="row no-gutters justify-content-center bg-body-dark">
      <div class="hero-static col-sm-10 col-md-8 col-xl-6 d-flex align-items-center p-2 px-sm-0">
        <div class="block block-rounded block-transparent block-fx-pop w-100 mb-0 overflow-hidden bg-image" style="background-image: url('{{ asset('img/logofenalco.svg')}}'); background-size: 250px; background-position: 50px 50px; background-repeat: no-repeat;">
            <div class="row no-gutters">
                <div class="col-md-6 order-md-1 bg-white">
                    <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
                        <div class="mb-2 text-center">
                            <a class="link-fx text-success font-w700 font-size-h1" href="{{ route('home') }}">
                                <span class="text-dark">Pro</span><span class="text-success">credito</span>
                            </a>
                            <p class="text-uppercase font-w700 font-size-sm text-muted">Crear una nueva cuenta</p>
                        </div>
                        <form class="js-validation-signup" method="POST" action="{{ route('register') }}" novalidate="novalidate">
                          {{ csrf_field() }}
                          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" class="form-control form-control-alt is-invalid" id="name" name="name" value="{{ old('name') }}" placeholder="Nombre Completo" aria-describedby="name-error" aria-invalid="true" required autofocus>
                            @if ($errors->has('name'))
                              <div id="name-error" class="invalid-feedback animated fadeIn">
                                <strong>{{ $errors->first('name') }}</strong>
                              </div>
                            @endif
                          </div>
                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control form-control-alt is-invalid" id="email" name="email" placeholder="Email" value="{{ old('email') }}" aria-describedby="email-error" aria-invalid="true" required>
                            @if ($errors->has('email'))
                              <div id="email-error" class="invalid-feedback animated fadeIn">
                                <strong>{{ $errors->first('email') }}</strong>
                              </div>
                            @endif
                          </div>
                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control form-control-alt is-invalid" id="password" name="password" placeholder="Password" aria-describedby="password-error" aria-invalid="true" required>
                            @if ($errors->has('password'))
                              <div id="password-error" class="invalid-feedback animated fadeIn">
                                <strong>{{ $errors->first('password') }}</strong>
                              </div>
                            @endif
                          </div>
                          <div class="form-group">
                              <input type="password" class="form-control form-control-alt" id="password-confirm" name="password_confirmation" placeholder="Password Confirm" required>
                          </div>
                          <div class="form-group">
                              <a href="#" data-toggle="modal" data-target="#modal-terms">Terminos &amp; Condiciones</a>
                              <div class="custom-control custom-checkbox custom-control-primary">
                                  <input type="checkbox" class="custom-control-input" id="signup-terms" name="signup-terms">
                                  <label class="custom-control-label" for="signup-terms">Estoy de acuerdo</label>
                              </div>
                          </div>
                          <div class="form-group">
                              <button type="submit" class="btn btn-block btn-hero-success">
                                  <i class="fa fa-fw fa-plus mr-1"></i> Regístrate
                              </button>
                          </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 order-md-0 bg-xeco-op d-flex align-items-center">
                    <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
                        <div class="media">
                            <a class="img-link mr-3" href="javascript:void(0)">
                                <img class="img-avatar img-avatar-thumb" src="{{ asset('img/logo.svg')}}" alt="Procredito">
                            </a>
                            <div class="media-body">
                                <p class="text-white font-w600 mb-1">
                                  Este acceso es para empresarios PROCRÉDITO.
                                  Para conocer más información sobre este servicio, haga clic en Inicio.
                                </p>
                                <a class="text-white-75 font-w600" href="https://www.fenalcoantioquia.com/" target="_blank">Fenalco, Antioquia</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </main>
</div>

<div class="modal fade" id="modal-terms" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title">Terminos &amp; Condiciones</h3>
            <div class="block-options">
              <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                <i class="fa fa-fw fa-times"></i>
              </button>
            </div>
          </div>
          <div class="block-content">
            <p><a class="text-black-75 font-w600" href="https://www.fenalcoantioquia.com/sites/default/files/pictures/p-pgc-6_politicas_de_tratamientos_web_version_0.pdf" target="_blank">Politicas de tratamiento de datos.</a></p>
            <p><a class="text-black-75 font-w600" href="https://www.fenalcoantioquia.com/sites/default/files/pictures/a-pgc-10_manual_interno_de_politicas_y_procedimientos_v_0.pdf" target="_blank">Manual interno de politicas y procedimientos.</a></p>
          </div>
          <div class="block-content block-content-full text-right bg-light">
          <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Aceptar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection
