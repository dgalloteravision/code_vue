@extends('welcome')

@section('content')

<div id="page-container" class="side-trans-enabled">
  <main id="main-container">
    <div class="row no-gutters justify-content-center bg-body-dark">
        <div class="hero-static col-sm-10 col-md-8 col-xl-6 d-flex align-items-center p-2 px-sm-0">
            <div class="block block-rounded block-transparent block-fx-pop w-100 mb-0 overflow-hidden bg-image" style="background-image: url('img/logofenalco.svg'); background-size: 250px; background-position: 50px 50px; background-repeat: no-repeat;">
                <div class="row no-gutters">
                    <div class="col-md-6 order-md-1 bg-white">
                        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
                            <div class="mb-2 text-center">
                                <a class="link-fx font-w700 font-size-h1" href="index.html">
                                    <span class="text-dark">Pro</span><span class="text-primary">credito</span>
                                </a>
                                <p class="text-uppercase font-w700 font-size-sm text-muted">Login</p>
                            </div>
                            <form class="js-validation-signin" action="#" method="post" novalidate="novalidate">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-alt is-invalid" id="login-username" name="login-username" placeholder="Username" aria-describedby="login-username-error">
                                <div id="login-username-error" class="invalid-feedback animated fadeIn" style="display:none;">Por favor ingrese un nombre de usuario</div></div>
                                <div class="form-group">
                                    <input type="password" class="form-control form-control-alt is-invalid" id="login-password" name="login-password" placeholder="Password" aria-describedby="login-password-error" aria-invalid="true">
                                <div id="login-password-error" class="invalid-feedback animated fadeIn" style="display:none;">Por favor ingrese una contraseña</div></div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-block btn-hero-primary">
                                        <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Sign In
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 order-md-0 bg-primary-dark-op d-flex align-items-center">
                        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
                            <div class="media">
                                <a class="img-link mr-3" href="javascript:void(0)">
                                    <img class="img-avatar img-avatar-thumb" src="img/logo.svg" alt="">
                                </a>
                                <div class="media-body">
                                    <p class="text-white font-w600 mb-1">
                                    Este acceso es para empresarios PROCRÉDITO.
                                    Para conocer más información sobre este servicio, haga clic en Inicio.
                                    </p>
                                    <a class="text-white-75 font-w600" href="javascript:void(0)">Fenalco, Antioquia</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </main>
</div>

@endsection
