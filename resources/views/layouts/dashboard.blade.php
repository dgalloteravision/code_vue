<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <link rel="shortcut icon" href="{{ asset('img/favicon-16x16.png') }}">
      <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('img/android-icon-192x192.png') }}">
      <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-icon-180x180.png') }}">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
      <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  </head>
  <body>
  <div id="page-container" class="enable-page-overlay side-scroll page-header-fixed page-header-dark main-content-narrow side-trans-enabled sidebar-o">
    <div id="page-overlay"></div>
    <header id="page-header">
      <div class="content-header">
          <div>
              <button type="button" class="btn btn-dual mr-1" data-toggle="layout" data-action="sidebar_toggle">
                  <i class="fa fa-fw fa-bars"></i>
              </button>
              <button type="button" class="btn btn-dual" data-toggle="layout" data-action="header_search_on">
                  <i class="fa fa-fw fa-search"></i> <span class="ml-1 d-none d-sm-inline-block">Search</span>
              </button>
          </div>
          <div>
              <div class="dropdown d-inline-block">
                  <button type="button" class="btn btn-dual" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-fw fa-user d-sm-none"></i>
                      <span class="d-none d-sm-inline-block">Admin</span>
                      <i class="fa fa-fw fa-angle-down ml-1 d-none d-sm-inline-block"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="page-header-user-dropdown">
                      <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                        User Options
                      </div>
                      <div class="p-2">
                          <a class="dropdown-item" href="be_pages_generic_profile.html">
                              <i class="far fa-fw fa-user mr-1"></i> Profile
                          </a>
                          <a class="dropdown-item d-flex align-items-center justify-content-between" href="be_pages_generic_inbox.html">
                              <span><i class="far fa-fw fa-envelope mr-1"></i> Inbox</span>
                              <span class="badge badge-primary">3</span>
                          </a>
                          <a class="dropdown-item" href="be_pages_generic_invoice.html">
                              <i class="far fa-fw fa-file-alt mr-1"></i> Invoices
                          </a>
                          <div role="separator" class="dropdown-divider"></div>
                          <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                              <i class="far fa-fw fa-building mr-1"></i> Settings
                          </a>
                          <div role="separator" class="dropdown-divider"></div>
                          <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                              <i class="far fa-fw fa-arrow-alt-circle-left mr-1"></i> Logout
                          </a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                          </form>
                      </div>
                  </div>
              </div>
              <div class="dropdown d-inline-block">
                  <button type="button" class="btn btn-dual" id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-fw fa-bell"></i>
                      <span class="badge badge-secondary badge-pill">5</span>
                  </button>
                  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0" aria-labelledby="page-header-notifications-dropdown">
                      <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                        Notifications
                      </div>
                      <ul class="nav-items my-2">
                          <li>
                              <a class="text-dark media py-2" href="javascript:void(0)">
                                  <div class="mx-3">
                                      <i class="fa fa-fw fa-check-circle text-success"></i>
                                  </div>
                                  <div class="media-body font-size-sm pr-2">
                                      <div class="font-w600">App was updated to v5.6!</div>
                                      <div class="text-muted font-italic">3 min ago</div>
                                  </div>
                              </a>
                          </li>
                          <li>
                              <a class="text-dark media py-2" href="javascript:void(0)">
                                  <div class="mx-3">
                                      <i class="fa fa-fw fa-user-plus text-info"></i>
                                  </div>
                                  <div class="media-body font-size-sm pr-2">
                                      <div class="font-w600">New Subscriber was added! You now have 2580!</div>
                                      <div class="text-muted font-italic">10 min ago</div>
                                  </div>
                              </a>
                          </li>
                          <li>
                              <a class="text-dark media py-2" href="javascript:void(0)">
                                  <div class="mx-3">
                                      <i class="fa fa-fw fa-times-circle text-danger"></i>
                                  </div>
                                  <div class="media-body font-size-sm pr-2">
                                      <div class="font-w600">Server backup failed to complete!</div>
                                      <div class="text-muted font-italic">30 min ago</div>
                                  </div>
                              </a>
                          </li>
                          <li>
                              <a class="text-dark media py-2" href="javascript:void(0)">
                                  <div class="mx-3">
                                      <i class="fa fa-fw fa-exclamation-circle text-warning"></i>
                                  </div>
                                  <div class="media-body font-size-sm pr-2">
                                      <div class="font-w600">You are running out of space. Please consider upgrading your plan.</div>
                                      <div class="text-muted font-italic">1 hour ago</div>
                                  </div>
                              </a>
                          </li>
                          <li>
                              <a class="text-dark media py-2" href="javascript:void(0)">
                                  <div class="mx-3">
                                      <i class="fa fa-fw fa-plus-circle text-primary"></i>
                                  </div>
                                  <div class="media-body font-size-sm pr-2">
                                      <div class="font-w600">New Sale! + $30</div>
                                      <div class="text-muted font-italic">2 hours ago</div>
                                  </div>
                              </a>
                          </li>
                      </ul>
                      <div class="p-2 border-top">
                          <a class="btn btn-light btn-block text-center" href="javascript:void(0)">
                              <i class="fa fa-fw fa-eye mr-1"></i> View All
                          </a>
                      </div>
                  </div>
              </div>
              <button type="button" class="btn btn-dual" data-toggle="layout" data-action="side_overlay_toggle">
                  <i class="far fa-fw fa-list-alt"></i>
              </button>
          </div>
      </div>
      <div id="page-header-search" class="overlay-header bg-primary">
          <div class="content-header">
              <form class="w-100" action="be_pages_generic_search.html" method="post">
                  <div class="input-group">
                      <div class="input-group-prepend">
                          <button type="button" class="btn btn-primary" data-toggle="layout" data-action="header_search_off">
                              <i class="fa fa-fw fa-times-circle"></i>
                          </button>
                      </div>
                      <input type="text" class="form-control border-0" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                  </div>
              </form>
        </div>
      </div>
      <div id="page-header-loader" class="overlay-header bg-primary-darker">
          <div class="content-header">
              <div class="w-100 text-center">
                  <i class="fa fa-fw fa-2x fa-sun fa-spin text-white"></i>
              </div>
          </div>
      </div>
    </header>

      <div class="content" style="max-width: 100% !important;">
        @yield('content')
      </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>
