<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon-16x16.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('img/android-icon-192x192.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-icon-180x180.png') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  </head>
  <body>
    <div id="app">
      @guest
        @yield('content')
      @else
        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-fixed page-header-dark side-trans-enabled">
          <div id="page-overlay"></div>
          <aside id="side-overlay" data-simplebar="init">
            <div class="simplebar-track vertical" style="visibility: visible;">
              <div class="simplebar-scrollbar" style="visibility: visible; top: 0px; height: 89px;"></div>
            </div>
            <div class="simplebar-track horizontal" style="visibility: hidden;">
              <div class="simplebar-scrollbar"></div>
            </div>
            <div class="simplebar-scroll-content" style="padding-right: 17px; margin-bottom: -34px;">
              <div class="simplebar-content" style="padding-bottom: 17px; margin-right: -17px;">
                <div class="bg-image" style="background-image: url('{{ asset('img/logo.png') }}');">
                  <div class="bg-primary-op">
                    <div class="content-header">
                      <a class="img-link mr-1" href="be_pages_generic_profile.html">
                        <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                      </a>
                      <div class="ml-2">
                      <a class="text-white font-w600" href="be_pages_generic_profile.html">{{ Auth::user()->name }}</a>
                      <div class="text-white-75 font-size-sm font-italic">Empleado...</div>
                    </div>
                    <a class="ml-auto text-white" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_close">
                      <i class="fa fa-times-circle"></i>
                    </a>
                  </div>
                </div>
              </div>
                      <div class="content-side">
                        <div class="block block-transparent pull-x pull-t">
                          <ul class="nav nav-tabs nav-tabs-block nav-justified js-tabs-enabled" data-toggle="tabs" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link" href="#so-settings">
                                <i class="fa fa-fw fa-cog"></i>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="#so-people">
                                <i class="far fa-fw fa-user-circle"></i>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link active show" href="#so-profile">
                                <i class="far fa-fw fa-edit"></i>
                              </a>
                            </li>
                          </ul>
                          <div class="block-content tab-content overflow-hidden">
                            <div class="tab-pane pull-x fade fade-up" id="so-settings" role="tabpanel">
                              <div class="block mb-0">
                                <div class="block-content block-content-sm block-content-full bg-body">
                                  <span class="text-uppercase font-size-sm font-w700">Color Themes</span>
                                </div>
                                <div class="block-content block-content-full">
                                <div class="row gutters-tiny text-center">
                                <div class="col-4 mb-1">
                                  <a class="d-block py-3 text-white font-size-sm font-w600 bg-default active" data-toggle="theme" data-theme="default" href="#">
                                    Default
                                  </a>
                                </div>
                                <div class="col-4 mb-1">
                                  <a class="d-block py-3 text-white font-size-sm font-w600 bg-xwork" data-toggle="theme" data-theme="assets/css/themes/xwork.min-1.2.css" href="#">
                                    xWork
                                  </a>
                                </div>
                                <div class="col-4 mb-1">
                                  <a class="d-block py-3 text-white font-size-sm font-w600 bg-xmodern" data-toggle="theme" data-theme="assets/css/themes/xmodern.min-1.2.css" href="#">
                                    xModern
                                  </a>
                                </div>
                                <div class="col-4 mb-1">
                                  <a class="d-block py-3 text-white font-size-sm font-w600 bg-xeco" data-toggle="theme" data-theme="assets/css/themes/xeco.min-1.2.css" href="#">
                                    xEco
                                  </a>
                                </div>
                                <div class="col-4 mb-1">
                                  <a class="d-block py-3 text-white font-size-sm font-w600 bg-xsmooth" data-toggle="theme" data-theme="assets/css/themes/xsmooth.min-1.2.css" href="#">
                                    xSmooth
                                  </a>
                                </div>
                                <div class="col-4 mb-1">
                                  <a class="d-block py-3 text-white font-size-sm font-w600 bg-xinspire" data-toggle="theme" data-theme="assets/css/themes/xinspire.min-1.2.css" href="#">
                                    xInspire
                                  </a>
                                </div>
                                <div class="col-12">
                                  <a class="d-block py-3 bg-body-dark font-w600 text-dark" href="be_ui_color_themes.html">All Color Themes</a>
                                </div>
                              </div>
                            </div>
                            <div class="block-content block-content-sm block-content-full bg-body">
                              <span class="text-uppercase font-size-sm font-w700">Sidebar</span>
                            </div>
                            <div class="block-content block-content-full">
                              <div class="row gutters-tiny text-center">
                                <div class="col-6 mb-1">
                                  <a class="d-block py-3 bg-body-dark font-w600 text-dark" data-toggle="layout" data-action="sidebar_style_dark" href="javascript:void(0)">Dark</a>
                                </div>
                                <div class="col-6 mb-1">
                                  <a class="d-block py-3 bg-body-dark font-w600 text-dark" data-toggle="layout" data-action="sidebar_style_light" href="javascript:void(0)">Light</a>
                                </div>
                              </div>
                            </div>
                            <div class="block-content block-content-sm block-content-full bg-body">
                              <span class="text-uppercase font-size-sm font-w700">Header</span>
                            </div>
                            <div class="block-content block-content-full">
                              <div class="row gutters-tiny text-center mb-2">
                                <div class="col-6 mb-1">
                                  <a class="d-block py-3 bg-body-dark font-w600 text-dark" data-toggle="layout" data-action="header_style_dark" href="javascript:void(0)">Dark</a>
                                </div>
                                <div class="col-6 mb-1">
                                  <a class="d-block py-3 bg-body-dark font-w600 text-dark" data-toggle="layout" data-action="header_style_light" href="javascript:void(0)">Light</a>
                                </div>
                                <div class="col-6 mb-1">
                                  <a class="d-block py-3 bg-body-dark font-w600 text-dark" data-toggle="layout" data-action="header_mode_fixed" href="javascript:void(0)">Fixed</a>
                                </div>
                                <div class="col-6 mb-1">
                                  <a class="d-block py-3 bg-body-dark font-w600 text-dark" data-toggle="layout" data-action="header_mode_static" href="javascript:void(0)">Static</a>
                                </div>
                              </div>
                            </div>
                            <div class="block-content block-content-sm block-content-full bg-body">
                              <span class="text-uppercase font-size-sm font-w700">Content</span>
                            </div>
                            <div class="block-content block-content-full">
                              <div class="row gutters-tiny text-center">
                                <div class="col-6 mb-1">
                                    <a class="d-block py-3 bg-body-dark font-w600 text-dark" data-toggle="layout" data-action="content_layout_boxed" href="javascript:void(0)">Boxed</a>
                                </div>
                                <div class="col-6 mb-1">
                                    <a class="d-block py-3 bg-body-dark font-w600 text-dark" data-toggle="layout" data-action="content_layout_narrow" href="javascript:void(0)">Narrow</a>
                                </div>
                                <div class="col-12 mb-1">
                                    <a class="d-block py-3 bg-body-dark font-w600 text-dark" data-toggle="layout" data-action="content_layout_full_width" href="javascript:void(0)">Full Width</a>
                                </div>
                              </div>
                            </div>
                            <div class="block-content row justify-content-center border-top">
                              <div class="col-9">
                                <a class="btn btn-block btn-hero-primary" href="be_layout_api.html">
                                    <i class="fa fa-fw fa-flask mr-1"></i> Layout API
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane pull-x fade fade-up" id="so-people" role="tabpanel">
                          <div class="block mb-0">
                            <div class="block-content block-content-sm block-content-full bg-body">
                            <span class="text-uppercase font-size-sm font-w700">Online</span>
                          </div>
                          <div class="block-content">
                            <ul class="nav-items">
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                  <div class="mx-3 overlay-container">
                                    <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                    <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-success"></span>
                                  </div>
                                  <div class="media-body">
                                    <div class="font-w600">Alice Moore</div>
                                    <div class="font-size-sm text-muted">Photographer</div>
                                  </div>
                                </a>
                              </li>
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                  <div class="mx-3 overlay-container">
                                    <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                    <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-success"></span>
                                  </div>
                                  <div class="media-body">
                                    <div class="font-w600">Jose Mills</div>
                                    <div class="font-w400 font-size-sm text-muted">Web Designer</div>
                                  </div>
                                </a>
                              </li>
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                  <div class="mx-3 overlay-container">
                                    <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                    <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-success"></span>
                                  </div>
                                  <div class="media-body">
                                    <div class="font-w600">Danielle Jones</div>
                                    <div class="font-w400 font-size-sm text-muted">Web Developer</div>
                                  </div>
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div class="block-content block-content-sm block-content-full bg-body">
                            <span class="text-uppercase font-size-sm font-w700">Busy</span>
                          </div>
                          <div class="block-content">
                            <ul class="nav-items">
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                  <div class="mx-3 overlay-container">
                                    <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                    <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-danger"></span>
                                  </div>
                                  <div class="media-body">
                                    <div class="font-w600">Laura Carr</div>
                                    <div class="font-w400 font-size-sm text-muted">UI Designer</div>
                                  </div>
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div class="block-content block-content-sm block-content-full bg-body">
                            <span class="text-uppercase font-size-sm font-w700">Away</span>
                            </div>
                            <div class="block-content">
                            <ul class="nav-items">
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                    <div class="mx-3 overlay-container">
                                        <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                        <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-warning"></span>
                                    </div>
                                    <div class="media-body">
                                        <div class="font-w600">Jose Parker</div>
                                        <div class="font-w400 font-size-sm text-muted">Copywriter</div>
                                    </div>
                                </a>
                              </li>
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                    <div class="mx-3 overlay-container">
                                        <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                        <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-warning"></span>
                                    </div>
                                    <div class="media-body">
                                        <div class="font-w600">Helen Jacobs</div>
                                        <div class="font-w400 font-size-sm text-muted">Writer</div>
                                    </div>
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div class="block-content block-content-sm block-content-full bg-body">
                            <span class="text-uppercase font-size-sm font-w700">Offline</span>
                          </div>
                          <div class="block-content">
                            <ul class="nav-items">
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                  <div class="mx-3 overlay-container">
                                      <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                      <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-muted"></span>
                                  </div>
                                  <div class="media-body">
                                      <div class="font-w600">Justin Hunt</div>
                                      <div class="font-w400 font-size-sm text-muted">Teacher</div>
                                  </div>
                                </a>
                              </li>
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                  <div class="mx-3 overlay-container">
                                    <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                    <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-muted"></span>
                                  </div>
                                  <div class="media-body">
                                    <div class="font-w600">Lori Moore</div>
                                    <div class="font-w400 font-size-sm text-muted">Photographer</div>
                                  </div>
                                </a>
                              </li>
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                  <div class="mx-3 overlay-container">
                                    <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                    <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-muted"></span>
                                  </div>
                                  <div class="media-body">
                                    <div class="font-w600">Alice Moore</div>
                                    <div class="font-w400 font-size-sm text-muted">Front-end Developer</div>
                                  </div>
                                </a>
                              </li>
                              <li>
                                <a class="media py-2" href="be_pages_generic_profile.html">
                                  <div class="mx-3 overlay-container">
                                    <img class="img-avatar img-avatar48" src="{{ asset('img/default_avatar_male.jpg') }}" alt="">
                                    <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-muted"></span>
                                  </div>
                                  <div class="media-body">
                                    <div class="font-w600">Brian Cruz</div>
                                    <div class="font-w400 font-size-sm text-muted">UX Specialist</div>
                                  </div>
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div class="block-content row justify-content-center border-top">
                            <div class="col-9">
                              <a class="btn btn-block btn-hero-primary" href="javascript:void(0)">
                                <i class="fa fa-fw fa-plus mr-1"></i> Add People
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane pull-x fade fade-up active show" id="so-profile" role="tabpanel">
                        <form action="be_pages_dashboard.html" method="post" onsubmit="return false;">
                          <div class="block mb-0">
                            <div class="block-content block-content-sm block-content-full bg-body">
                              <span class="text-uppercase font-size-sm font-w700">Personal</span>
                            </div>
                            <div class="block-content block-content-full">
                              <div class="form-group">
                                <label>Username</label>
                                <input type="text" readonly="" class="form-control" id="staticEmail" value="Admin">
                              </div>
                              <div class="form-group">
                                <label for="so-profile-name">Name</label>
                                <input type="text" class="form-control" id="so-profile-name" name="so-profile-name" value="George Taylor">
                              </div>
                              <div class="form-group">
                                <label for="so-profile-email">Email</label>
                                <input type="email" class="form-control" id="so-profile-email" name="so-profile-email" value="g.taylor@example.com">
                              </div>
                            </div>
                            <div class="block-content block-content-sm block-content-full bg-body">
                              <span class="text-uppercase font-size-sm font-w700">Password Update</span>
                            </div>
                            <div class="block-content block-content-full">
                              <div class="form-group">
                                <label for="so-profile-password">Current Password</label>
                                <input type="password" class="form-control" id="so-profile-password" name="so-profile-password">
                              </div>
                              <div class="form-group">
                                <label for="so-profile-new-password">New Password</label>
                                <input type="password" class="form-control" id="so-profile-new-password" name="so-profile-new-password">
                              </div>
                              <div class="form-group">
                                <label for="so-profile-new-password-confirm">Confirm New Password</label>
                                <input type="password" class="form-control" id="so-profile-new-password-confirm" name="so-profile-new-password-confirm">
                              </div>
                            </div>
                            <div class="block-content block-content-sm block-content-full bg-body">
                              <span class="text-uppercase font-size-sm font-w700">Options</span>
                            </div>
                            <div class="block-content">
                              <div class="custom-control custom-checkbox custom-control-primary mb-1">
                                <input type="checkbox" class="custom-control-input" id="so-settings-status" name="so-settings-status" value="1">
                                <label class="custom-control-label" for="so-settings-status">Online Status</label>
                              </div>
                              <p class="text-muted font-size-sm">
                                Make your online status visible to other users of your app
                              </p>
                              <div class="custom-control custom-checkbox custom-control-primary mb-1">
                                <input type="checkbox" class="custom-control-input" id="so-settings-notifications" name="so-settings-notifications" value="1" checked="">
                                <label class="custom-control-label" for="so-settings-notifications">Notifications</label>
                              </div>
                              <p class="text-muted font-size-sm">
                                Receive desktop notifications regarding your projects and sales
                              </p>
                              <div class="custom-control custom-checkbox custom-control-primary mb-1">
                                <input type="checkbox" class="custom-control-input" id="so-settings-updates" name="so-settings-updates" value="1" checked="">
                                <label class="custom-control-label" for="so-settings-updates">Auto Updates</label>
                              </div>
                              <p class="text-muted font-size-sm">
                                If enabled, we will keep all your applications and servers up to date with the most recent features automatically
                              </p>
                            </div>
                            <div class="block-content row justify-content-center border-top">
                              <div class="col-9">
                                <button type="submit" class="btn btn-block btn-hero-primary">
                                  <i class="fa fa-fw fa-save mr-1"></i> Save
                                </button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </aside>
          <nav id="sidebar" aria-label="Main Navigation" data-simplebar="init">
            <div class="simplebar-track vertical" style="visibility: visible;">
              <div class="simplebar-scrollbar" style="visibility: visible; top: 81px; height: 91px;"></div>
            </div>
            <div class="simplebar-track horizontal" style="visibility: hidden;">
              <div class="simplebar-scrollbar"></div>
            </div>
            <div class="simplebar-scroll-content" style="padding-right: 17px; margin-bottom: -34px;">
              <div class="simplebar-content" style="padding-bottom: 17px; margin-right: -17px;">
                <div class="bg-header-dark">
                  <div class="content-header bg-white-10">
                    <a class="link-fx font-w600 font-size-lg text-white" href="index.html">
                      <span class="text-white-75">Pro</span><span class="text-white">credito</span>
                    </a>
                    <div>
                      <a class="js-class-toggle text-white-75" data-target="#sidebar-style-toggler" data-class="fa-toggle-off fa-toggle-on" data-toggle="layout" data-action="sidebar_style_toggle" href="javascript:void(0)">
                        <i class="fa fa-toggle-off" id="sidebar-style-toggler"></i>
                      </a>
                      <a class="d-lg-none text-white ml-2" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                        <i class="fa fa-times-circle"></i>
                      </a>
                    </div>
                  </div>
                </div>
            <div class="content-side content-side-full">
                <ul class="nav-main">
                    <li class="nav-main-item">
        <a class="nav-main-link" href="{{ route('home') }}">
        <i class="nav-main-link-icon si si-cursor"></i>
        <span class="nav-main-link-name">Dashboard</span>
        <span class="nav-main-link-badge badge badge-pill badge-success">5</span>
        </a>
        </li>
        <li class="nav-main-heading">Configuraciones</li>
        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <i class="nav-main-link-icon si si-grid"></i>
        <span class="nav-main-link-name">Configuraciones</span>
        </a>
        </li>

        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
        <i class="nav-main-link-icon si si-star"></i>
        <span class="nav-main-link-name">Layout</span>
        </a>
        </li>
        <li class="nav-main-heading">Usuarios</li>
        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <i class="nav-main-link-icon si si-users"></i>
        <span class="nav-main-link-name">Usuarios</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_icons.html">
        <span class="nav-main-link-name">Listado</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_grid.html">
        <span class="nav-main-link-name">Ubicaciones</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_typography.html">
        <span class="nav-main-link-name">Configuraciones</span>
        </a>
        </li>
        <li class="nav-main-item">
          <a data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#" class="nav-main-link nav-main-link-submenu">
          <span class="nav-main-link-name">Seguridad</span> <span class="nav-main-link-badge badge badge-pill badge-primary">3</span></a>
          <ul class="nav-main-submenu">
            <li class="nav-main-item">
              <a href="#" class="nav-main-link">
                <i class="nav-main-link-icon si si-tag"></i>
                <span class="nav-main-link-name">Preguntas</span> <span class="nav-main-link-badge badge badge-pill badge-info">2</span></a>
            </li>
            <li class="nav-main-item">
              <a href="#" class="nav-main-link">
                <i class="nav-main-link-icon si si-pie-chart"></i>
                <span class="nav-main-link-name">Respuestas</span>
                <span class="nav-main-link-badge badge badge-pill badge-danger">2</span></a>
            </li> <li class="nav-main-item"><a href="#" class="nav-main-link"><i class="nav-main-link-icon si si-note"></i> <span class="nav-main-link-name">2.3 Item</span> <span class="nav-main-link-badge badge badge-pill badge-warning">3</span></a></li> <li class="nav-main-item"><a data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#" class="nav-main-link nav-main-link-submenu"><span class="nav-main-link-name">Sub Level 3</span></a> <ul class="nav-main-submenu"><li class="nav-main-item"><a href="#" class="nav-main-link"><i class="nav-main-link-icon si si-map"></i> <span class="nav-main-link-name">3.1 Item</span></a></li> <li class="nav-main-item"><a href="#" class="nav-main-link"><i class="nav-main-link-icon si si-cup"></i> <span class="nav-main-link-name">3.2 Item</span></a></li> <li class="nav-main-item"><a href="#" class="nav-main-link"><i class="nav-main-link-icon si si-user-female"></i> <span class="nav-main-link-name">3.3 Item</span></a></li> <li class="nav-main-item"><a data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#" class="nav-main-link nav-main-link-submenu"><span class="nav-main-link-name">Sub Level 4</span></a> <ul class="nav-main-submenu"><li class="nav-main-item"><a href="#" class="nav-main-link"><i class="nav-main-link-icon si si-heart"></i> <span class="nav-main-link-name">4.1 Item</span></a></li> <li class="nav-main-item"><a href="#" class="nav-main-link"><i class="nav-main-link-icon si si-magnifier"></i> <span class="nav-main-link-name">4.2 Item</span></a></li> <li class="nav-main-item"><a href="#" class="nav-main-link"><i class="nav-main-link-icon si si-microphone"></i> <span class="nav-main-link-name">4.3 Item</span></a></li></ul></li></ul></li></ul></li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_tabs.html">
        <span class="nav-main-link-name">Perfiles</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_buttons.html">
        <span class="nav-main-link-name">Buttons</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_buttons_groups.html">
        <span class="nav-main-link-name">Button Groups</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_dropdowns.html">
        <span class="nav-main-link-name">Dropdowns</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_progress.html">
        <span class="nav-main-link-name">Progress</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_alerts.html">
        <span class="nav-main-link-name">Alerts</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_tooltips.html">
        <span class="nav-main-link-name">Tooltips</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_popovers.html">
        <span class="nav-main-link-name">Popovers</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_accordion.html">
        <span class="nav-main-link-name">Accordion</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_modals.html">
        <span class="nav-main-link-name">Modals</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_images.html">
        <span class="nav-main-link-name">Images Overlay</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_timeline.html">
        <span class="nav-main-link-name">Timeline</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_ribbons.html">
        <span class="nav-main-link-name">Ribbons</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_animations.html">
        <span class="nav-main-link-name">Animations</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_ui_color_themes.html">
        <span class="nav-main-link-name">Color Themes</span>
        </a>
        </li>
        </ul>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <i class="nav-main-link-icon si si-cup"></i>
        <span class="nav-main-link-name">Tables</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_tables_styles.html">
        <span class="nav-main-link-name">Styles</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_tables_responsive.html">
        <span class="nav-main-link-name">Responsive</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_tables_helpers.html">
        <span class="nav-main-link-name">Helpers</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_tables_pricing.html">
        <span class="nav-main-link-name">Pricing</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_tables_datatables.html">
        <span class="nav-main-link-name">DataTables</span>
        </a>
        </li>
        </ul>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <i class="nav-main-link-icon si si-notebook"></i>
        <span class="nav-main-link-name">Forms</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_forms_elements.html">
        <span class="nav-main-link-name">Elements</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_forms_custom_controls.html">
        <span class="nav-main-link-name">Custom Controls</span>
        <span class="nav-main-link-badge badge badge-pill badge-success">New</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_forms_layouts.html">
        <span class="nav-main-link-name">Layouts</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_forms_input_groups.html">
        <span class="nav-main-link-name">Input Groups</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_forms_plugins.html">
        <span class="nav-main-link-name">Plugins</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_forms_editors.html">
        <span class="nav-main-link-name">Editors</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_forms_validation.html">
        <span class="nav-main-link-name">Validation</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_forms_wizard.html">
        <span class="nav-main-link-name">Wizard</span>
        </a>
        </li>
        </ul>
        </li>
        <li class="nav-main-heading">Gestión</li>
        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <i class="nav-main-link-icon si si-wrench"></i>
        <span class="nav-main-link-name">Auditorias</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_dialogs.html">
        <span class="nav-main-link-name">Realizar Auditoria</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_notifications.html">
        <span class="nav-main-link-name">Notifications</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_loaders.html">
        <span class="nav-main-link-name">Loaders</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_charts.html">
        <span class="nav-main-link-name">Charts</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_gallery.html">
        <span class="nav-main-link-name">Gallery</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_sliders.html">
        <span class="nav-main-link-name">Sliders</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_rating.html">
        <span class="nav-main-link-name">Rating</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_appear.html">
        <span class="nav-main-link-name">Appear</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_calendar.html">
        <span class="nav-main-link-name">Calendar</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_image_cropper.html">
        <span class="nav-main-link-name">Image Cropper</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_maps_google.html">
        <span class="nav-main-link-name">Google Maps</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_maps_vector.html">
        <span class="nav-main-link-name">Vector Maps</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_comp_syntax_highlighting.html">
        <span class="nav-main-link-name">Syntax Highlighting</span>
        </a>
        </li>
        </ul>
        </li>

        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <i class="nav-main-link-icon si si-settings"></i>
        <span class="nav-main-link-name">Notificaciones</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-drawer"></i>
        <span class="nav-main-link-name">1.1 Item</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-fire"></i>
        <span class="nav-main-link-name">1.2 Item</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-graph"></i>
        <span class="nav-main-link-name">1.3 Item</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <span class="nav-main-link-name">Sub Level 2</span>
        <span class="nav-main-link-badge badge badge-pill badge-primary">3</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-tag"></i>
        <span class="nav-main-link-name">2.1 Item</span>
        <span class="nav-main-link-badge badge badge-pill badge-info">2</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-pie-chart"></i>
        <span class="nav-main-link-name">2.2 Item</span>
        <span class="nav-main-link-badge badge badge-pill badge-danger">2</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-note"></i>
        <span class="nav-main-link-name">2.3 Item</span>
        <span class="nav-main-link-badge badge badge-pill badge-warning">3</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <span class="nav-main-link-name">Sub Level 3</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-map"></i>
        <span class="nav-main-link-name">3.1 Item</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-cup"></i>
        <span class="nav-main-link-name">3.2 Item</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-user-female"></i>
        <span class="nav-main-link-name">3.3 Item</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <span class="nav-main-link-name">Sub Level 4</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-heart"></i>
        <span class="nav-main-link-name">4.1 Item</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-magnifier"></i>
        <span class="nav-main-link-name">4.2 Item</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="#">
        <i class="nav-main-link-icon si si-microphone"></i>
        <span class="nav-main-link-name">4.3 Item</span>
        </a>
        </li>
        </ul>
        </li>
        </ul>
        </li>
        </ul>
        </li>
        </ul>
        </li>

        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <i class="nav-main-link-icon si si-puzzle"></i>
        <span class="nav-main-link-name">Solicitudes</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_widgets_tiles.html">
        <span class="nav-main-link-name">Tiles</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_widgets_stats.html">
        <span class="nav-main-link-name">Statistics</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_widgets_media.html">
        <span class="nav-main-link-name">Media</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_widgets_users.html">
        <span class="nav-main-link-name">Users</span>
        </a>
        </li>
        </ul>
        </li>

        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <i class="nav-main-link-icon si si-puzzle"></i>
        <span class="nav-main-link-name">Tareas</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_widgets_tiles.html">
        <span class="nav-main-link-name">Tiles</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_widgets_stats.html">
        <span class="nav-main-link-name">Statistics</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_widgets_media.html">
        <span class="nav-main-link-name">Media</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_widgets_users.html">
        <span class="nav-main-link-name">Users</span>
        </a>
        </li>
        </ul>
        </li>

        <li class="nav-main-heading">Pages</li>
        <li class="nav-main-item">
        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
        <i class="nav-main-link-icon si si-ghost"></i>
        <span class="nav-main-link-name">Opción</span>
        </a>
        <ul class="nav-main-submenu">
        <li class="nav-main-item">
        <a class="nav-main-link" href="be_pages_error_all.html">
        <span class="nav-main-link-name">Opción</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="op_error_400.html">
        <span class="nav-main-link-name">Opción</span>
        </a>
        </li>
        <li class="nav-main-item">
        <a class="nav-main-link" href="op_error_401.html">
        <span class="nav-main-link-name">Opción</span>
        </a>
        </li>
        </ul>
        </li>
                </ul>
            </div>
        </div></div>
            </nav>
          <header id="page-header">
            <div class="content-header">
              <div>
                <button type="button" class="btn btn-dual mr-1" data-toggle="layout" data-action="sidebar_toggle">
                  <i class="fa fa-fw fa-bars"></i>
                </button>
                <button type="button" class="btn btn-dual" data-toggle="layout" data-action="header_search_on">
                  <i class="fa fa-fw fa-search"></i> <span class="ml-1 d-none d-sm-inline-block">Search</span>
                </button>
              </div>
              <div>
                <div class="dropdown d-inline-block">
                  <button type="button" class="btn btn-dual" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-fw fa-user d-sm-none"></i>
                    <span class="d-none d-sm-inline-block">Admin</span>
                    <i class="fa fa-fw fa-angle-down ml-1 d-none d-sm-inline-block"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="page-header-user-dropdown">
                    <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                      User Options
                    </div>
                    <div class="p-2">
                      <a class="dropdown-item" href="be_pages_generic_profile.html">
                        <i class="far fa-fw fa-user mr-1"></i> Perfil
                      </a>
                      <a class="dropdown-item d-flex align-items-center justify-content-between" href="be_pages_generic_inbox.html">
                        <span><i class="far fa-fw fa-envelope mr-1"></i> Inbox</span>
                        <span class="badge badge-primary">3</span>
                      </a>
                      <a class="dropdown-item" href="be_pages_generic_invoice.html">
                        <i class="far fa-fw fa-file-alt mr-1"></i> Invoices
                      </a>
                      <div role="separator" class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                        <i class="far fa-fw fa-building mr-1"></i> Configuraciones
                      </a>
                      <div role="separator" class="dropdown-divider"></div>
                      <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        <i class="far fa-fw fa-arrow-alt-circle-left mr-1"></i> Logout
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                    </div>
                  </div>
                </div>
                <div class="dropdown d-inline-block">
                  <button type="button" class="btn btn-dual" id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-fw fa-bell"></i>
                    <span class="badge badge-secondary badge-pill">5</span>
                  </button>
                  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0" aria-labelledby="page-header-notifications-dropdown">
                    <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                       Notifications
                    </div>
                    <ul class="nav-items my-2">
                      <li>
                        <a class="text-dark media py-2" href="javascript:void(0)">
                          <div class="mx-3">
                            <i class="fa fa-fw fa-check-circle text-success"></i>
                          </div>
                          <div class="media-body font-size-sm pr-2">
                            <div class="font-w600">App was updated to v5.6!</div>
                            <div class="text-muted font-italic">3 min ago</div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a class="text-dark media py-2" href="javascript:void(0)">
                          <div class="mx-3">
                              <i class="fa fa-fw fa-user-plus text-info"></i>
                          </div>
                          <div class="media-body font-size-sm pr-2">
                              <div class="font-w600">New Subscriber was added! You now have 2580!</div>
                              <div class="text-muted font-italic">10 min ago</div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a class="text-dark media py-2" href="javascript:void(0)">
                          <div class="mx-3">
                              <i class="fa fa-fw fa-times-circle text-danger"></i>
                          </div>
                          <div class="media-body font-size-sm pr-2">
                              <div class="font-w600">Server backup failed to complete!</div>
                              <div class="text-muted font-italic">30 min ago</div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a class="text-dark media py-2" href="javascript:void(0)">
                          <div class="mx-3">
                            <i class="fa fa-fw fa-exclamation-circle text-warning"></i>
                          </div>
                          <div class="media-body font-size-sm pr-2">
                            <div class="font-w600">You are running out of space. Please consider upgrading your plan.</div>
                            <div class="text-muted font-italic">1 hour ago</div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a class="text-dark media py-2" href="javascript:void(0)">
                          <div class="mx-3">
                              <i class="fa fa-fw fa-plus-circle text-primary"></i>
                          </div>
                          <div class="media-body font-size-sm pr-2">
                              <div class="font-w600">New Sale! + $30</div>
                              <div class="text-muted font-italic">2 hours ago</div>
                          </div>
                        </a>
                      </li>
                    </ul>
                    <div class="p-2 border-top">
                      <a class="btn btn-light btn-block text-center" href="javascript:void(0)">
                        <i class="fa fa-fw fa-eye mr-1"></i> View All
                      </a>
                    </div>
                  </div>
                </div>
                <button type="button" class="btn btn-dual" data-toggle="layout" data-action="side_overlay_toggle">
                  <i class="far fa-fw fa-list-alt"></i>
                </button>
              </div>
            </div>
            <div id="page-header-search" class="overlay-header bg-primary">
              <div class="content-header">
                <form class="w-100" action="be_pages_generic_search.html" method="post">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <button type="button" class="btn btn-primary" data-toggle="layout" data-action="header_search_off">
                        <i class="fa fa-fw fa-times-circle"></i>
                      </button>
                    </div>
                    <input type="text" class="form-control border-0" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                  </div>
                </form>
              </div>
            </div>
            <div id="page-header-loader" class="overlay-header bg-primary-darker">
              <div class="content-header">
                <div class="w-100 text-center">
                  <i class="fa fa-fw fa-2x fa-sun fa-spin text-white"></i>
                </div>
              </div>
            </div>
          </header>
          <main id="main-container">
            <div class="bg-body-light">
              <div class="content content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                  <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Full Width Content</h1>
                  <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">Layout</li>
                      <li class="breadcrumb-item">Content</li>
                      <li class="breadcrumb-item active" aria-current="page">Full Width</li>
                    </ol>
                  </nav>
                </div>
              </div>
            </div>
            <div class="content">
              <estado></estado>
            </div>
          </main>
          <footer id="page-footer" class="bg-body-light">
            <div class="content py-0">
              <div class="row font-size-sm">
                <div class="col-sm-6 order-sm-2 mb-1 mb-sm-0 text-center text-sm-right">
                  Hecho <i class="fa fa-heart text-danger"></i> por <a class="font-w600" href="http://bit.ly/2y93Ykh" target="_blank">Fenalco, Antioquia</a>
                </div>
                <div class="col-sm-6 order-sm-1 text-center text-sm-left">
                  <a class="font-w600" href="http://bit.ly/2y93Ykh" target="_blank">Procredito 1.0</a> © <span data-toggle="year-copy" class="js-year-copy-enabled">2018</span>
                </div>
              </div>
            </div>
          </footer>
        </div>
      @endguest
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- <script src="{{ asset('js/procredito.core.min.js') }}"></script>
    <script src="{{ asset('js/procredito.app.min.js') }}"></script> -->
  </body>
</html>
