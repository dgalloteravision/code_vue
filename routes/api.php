<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'], function () {
  Route::post('login', 'ApiAuthController@login');
  Route::group(['middleware' => 'jwt.auth'], function () {
      Route::post('logout', 'ApiAuthController@logout');
      Route::get('user', 'ApiAuthController@getAuthUser');
      Route::get('register', 'ApiAuthController@register');
      Route::apiResource('usuarios', 'UsuarioController', ['except' => 'create', 'edit']);
      Route::apiResource('configuraciones', '\Procredito\Modules\Seguridad\Http\Controllers\ConfiguracionController', ['except' => 'create', 'edit']);
      Route::apiResource('departamentos', '\Procredito\Modules\Seguridad\Http\Controllers\DepartamentoController', ['except' => 'create', 'edit']);
      Route::apiResource('municipios', '\Procredito\Modules\Seguridad\Http\Controllers\MunicipioController', ['except' => 'create', 'edit']);
      Route::apiResource('perfiles', '\Procredito\Modules\Seguridad\Http\Controllers\RolController', ['except' => 'create', 'edit']);
      Route::apiResource('permisos', '\Procredito\Modules\Seguridad\Http\Controllers\PermisoController', ['except' => 'create', 'edit']);
      Route::apiResource('preguntas', '\Procredito\Modules\Seguridad\Http\Controllers\SeguridadPreguntaController', ['except' => 'create', 'edit']);
      Route::apiResource('tiposdocumento', '\Procredito\Modules\Seguridad\Http\Controllers\TipoDocumentoController', ['except' => 'create', 'edit']);
      Route::apiResource('ubicaciones', '\Procredito\Modules\Seguridad\Http\Controllers\UbicacionController', ['except' => 'create', 'edit']);
      Route::apiResource('estados', '\Procredito\Modules\Seguridad\Http\Controllers\EstadoController', ['except' => 'create', 'edit']);
      Route::apiResource('organizaciones', '\Procredito\Modules\Seguridad\Http\Controllers\OrganizacionesController', ['except' => 'create', 'edit']);
      Route::apiResource('sucursales', '\Procredito\Modules\Seguridad\Http\Controllers\SucursalesController', ['except' => 'create', 'edit']);
  });
});

