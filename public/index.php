<?php
/*try {
  $ip1= '192.168.10.10';
  $ip2= '192.168.10.1';
  $ip3= '127.0.0.1';
  $conn =  new PDO("pgsql:host=".$ip1.";dbname=procreditoDB;port=54320;sslmode=prefer", "homestead", "secret", []);

  // query
  $sql = "SELECT * from users where email =  'dany338@gmail.com' ";
  $q = $conn->prepare($sql);
  $q->execute();
  $resultados = $q->fetchAll(PDO::FETCH_ASSOC);

	echo json_encode($resultados);
  $q->execute();
} catch (Exception $e) {
  $conn = $e;
}
exit(0);
return false;*/
/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */

define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);
