<?php

namespace Procredito;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Procredito\Modules\Seguridad\Models\TipoDocumento;
use Procredito\Modules\Seguridad\Models\Sucursales;
use Procredito\Modules\Seguridad\Models\Rol;
use Procredito\Modules\Seguridad\Models\UsuarioSucursal;
use Procredito\Modules\Seguridad\Models\RolUsuario;
use Procredito\Modules\Seguridad\Models\OrganizacionesUsuario;
use Procredito\Modules\Seguridad\Models\PasswordUsuario;
use Procredito\Modules\Seguridad\Models\SeguridadPregunta;
use Procredito\Modules\Seguridad\Models\Configuracion;
use Procredito\Modules\Seguridad\Models\Ubicacion;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'imagen',
        'cod_tipo_documento',
        'numero_documento_usuario',
        'primer_nombre_usuario',
        'segundo_nombre_usuario',
        'primer_apellido_usuario',
        'segundo_apellido_usuario',
        'nombre_usuario_usuario',
        'cod_estado',
        'fecha_creacion_usuario',
        'fecha_modificacion_usuario'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the tipo documento that owns the user.
     */
    public function tipodocumento()
    {
      return $this->belongsTo(TipoDocumento::class, 'cod_tipo_documento', 'cod_tipo_documento')->select('cod_tipo_documento', 'nombre_tipo_documento');
    }

    // belongsToMany
    public function sucursales()
    {
      return $this->belongsToMany(Sucursales::class, 'usuario_sucursal', 'cod_usuario', 'cod_sucursal');
    }

    public function roles()
    {
      return $this->belongsToMany(Rol::class, 'rol_usuario', 'cod_usuario', 'cod_rol');
    }

    public function organizaciones()
    {
      return $this->belongsToMany(Rol::class, 'organizaciones_usuario', 'cod_usuario', 'cod_organizacion');
    }

    // hasMany
    public function usuariosucursales()
    {
      return $this->hasMany(UsuarioSucursal::class, 'cod_usuario', 'id');
    }

    public function rolesusuario()
    {
      return $this->hasMany(RolUsuario::class, 'cod_usuario', 'id');
    }

    public function organizacionesusuario()
    {
      return $this->hasMany(OrganizacionesUsuario::class, 'cod_usuario', 'id');
    }

    public function passwordusuarios()
    {
      return $this->hasMany(PasswordUsuario::class, 'cod_usuario', 'id')->select('cod_password_usuario','cod_usuario','password_password_usuario','password_audio_respuesta_password_usuario');
    }

    public function seguridadpreguntas()
    {
      return $this->hasMany(SeguridadPregunta::class, 'cod_usuario', 'id')->select('cod_seguridad_pregunta','cod_usuario','texto_pregunta');
    }

    public function configuracion()
    {
      return $this->hasOne(Configuracion::class, 'cod_usuario', 'id');
    }

    public function ubicacion()
    {
      return $this->hasOne(Ubicacion::class, 'cod_usuario', 'id');
    }
}
