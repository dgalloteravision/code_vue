<?php

namespace Procredito\Http\Controllers;

use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use JWTAuth;
use JWTAuthException;
use Procredito\User;
use Validator, DB, Hash, Mail;

class ApiAuthController extends Controller
{
  public function __construct()
  {
      $this->user = new User;
      $this->middleware('guest', ['except' => ['logout', 'register', 'getAuthUser']]);
  }

  public function login(Request $request) {
      $credentials = $request->only('email', 'password');
      $token = null;
      try {
          if (!$token = JWTAuth::attempt($credentials)) {
              return response()->json([
                  'response' => 'error',
                  'message' => 'invalid_email_or_password',
              ]);
          }
      } catch (JWTAuthException $e) {
          return response()->json([
              'response' => 'error',
              'message' => 'failed_to_create_token',
          ]);
      }
      return response()->json([
          'response' => 'success',
          'result' => [
              'token' => $token,
          ],
      ]);
  }

  /**
   * Log out
   * Invalidate the token, so user cannot use it anymore
   * They have to relogin to get a new token
   *
   * @param Request $request
   */
  public function logout(Request $request) {
    $this->validate($request, ['token' => 'required']);

    try {
        JWTAuth::invalidate($request->input('token'));
        return response()->json(['success' => true, 'message'=> "You have successfully logged out."]);
    } catch (JWTException $e) {
        // something went wrong whilst attempting to encode the token
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  public function getAuthUser(Request $request) {
      $user = JWTAuth::toUser($request->token);
      return response()->json(['result' => $user]);
  }

  /**
   * API Register
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function register(Request $request)
  {
      $credentials = $request->only('name', 'cod_tipo_documento', 'email', 'password');

      $rules = [
          'name' => 'required|max:255',
          'email' => 'required|email|max:255|unique:users'
      ];
      $validator = Validator::make($credentials, $rules);
      if($validator->fails()) {
          return response()->json(['success'=> false, 'error'=> $validator->messages()]);
      }
      $name               = $request->name;
      $email              = $request->email;
      $cod_tipo_documento = $request->cod_tipo_documento;
      $password           = $request->password;

      $user = User::create([
        'name'               => $name,
        'email'              => $email,
        'cod_tipo_documento' => $cod_tipo_documento,
        'password'           => bcrypt($password)
      ]);
      $verification_code = str_random(30); //Generate verification code
      $subject = "Please verify your email address.";
      Mail::send('email.verify', ['name' => $name, 'verification_code' => $verification_code],
          function($mail) use ($email, $name, $subject){
              $mail->from(getenv('FROM_EMAIL_ADDRESS'), "From User/Company Name Goes Here");
              $mail->to($email, $name);
              $mail->subject($subject);
          }
      );
      return response()->json(['success'=> true, 'message'=> 'Thanks for signing up! Please check your email to complete your registration.']);
  }
}
