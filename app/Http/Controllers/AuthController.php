<?php

namespace Procredito\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Procredito\Http\Requests\RegisterFormRequest;
use Procredito\User;
use JWTAuth;
use Auth;
class AuthController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('guest', ['except' => 'logout', 'show', 'token', 'delete']);
  }

  // use SendsPasswordResetEmails;
  public function register(Request $request)
  {
    $user = new User;
    $user->email              = $request->email;
    $user->name               = $request->name;
    $user->cod_tipo_documento = $request->cod_tipo_documento;
    $user->password           = bcrypt($request->password);
    $user->save();

    /*
    $user = User::create([
      'name' => $request['name'],
      'email' => $request['email'],
      'password' => $request['password']
    ]);
     */

    return response([
        'status' => 'success',
        'data' => $user
    ], 200);
  }

  public function login(Request $request)
  {
    $credentials = $request->only('email', 'password');
    if(!$token = JWTAuth::attempt($credentials)) {
      return response([
          'status' => 'error',
          'error' => 'invalid.credentials',
          'msg' => 'Invalid Credentials.'
      ], 400);
    }

    /*
    try {
      if(! $token = JWTAuth::attempt($credentials)) {
        // return $this->response->error(['error' => 'User credentials are not correct!'], 401);
        return $this->response->errorUnauthorized();
      }
    } catch (JWException $ex) {
      //return $this->response->error(['error' => 'Something went wrong'], 500)
      return $this->response->errorInternal();
    }

    return $this->response->array(compact('token'))->setStatusCode(200);
     */

    return response([
            'status' => 'success',
            'token' => $token
    ]);
  }

  public function reset(Request $request) {

    //$credentials = $request->only('email');
    $this->validateEmail($request);

    $response = $this->broker()->sendResetLink(
        $request->only('email')
    );

    $data = ($response == Password::RESET_LINK_SENT) ? $this->sendResetLinkResponse($response) : $this->sendResetLinkFailedResponse($request, $response);

    return response([
      'status' => 'success',
      'data' => $data
    ]);
  }

  // Es la unica consulta de base a datos
  //
  public function user(Request $request) {
      $user = User::find(Auth::user()->id);
      return response([
              'status' => 'success',
              'data' => $user
          ]);
  }

  public function show() {
    try {
      $user = JWTAuth::parseToken()->toUser();

      if(! $user) {
        return $this->response->errorNotFound('User not found');
      }
    } catch(\Tymon\JWTAuth\Exceptions\JWException $ex) {
      return $this->response->error('Someting went wrong');
    }

    return $this->response->array(compact('user'))->setStatusCode(200);
    //return JWTAuth::parseToken()->toUser();
  }

  public function destroy() {
    $user = JWTAuth::parseToken()->authenticate();

    if(! $user) {
      // Fail the delete process
    }

    // if not go with the delete process
    $user->delete();
  }

  public function getToken() {

    $token = JWTAuth::getToken();
    if(! $token) {
      return $this->response->errorUnauthorized('Token is invalid');
    }

    try {
      $refreshedToken = JWTAuth::refresh($token);
    } catch(\Tymon\JWTAuth\Exceptions\JWException $ex) {
      return $this->response->error('Someting went wrong');
    }

    return $this->response->array(compact('refreshedToken'))->setStatusCode(200);
  }

  public function logout() {
      JWTAuth::invalidate();
      return response([
              'status' => 'success',
              'msg' => 'Logged out Successfully.'
          ], 200);
  }

  public function refresh() {
      return response([
      'status' => 'success'
      ]);
  }

  /**
   * Get the broker to be used during password reset.
   *
   * @return \Illuminate\Contracts\Auth\PasswordBroker
   */
  public function broker()
  {
    return Password::broker();
  }

  /**
   * Get the response for a successful password reset link.
   *
   * @param  string  $response
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
   */
  protected function sendResetLinkResponse($response)
  {
      return back()->with('status', trans($response));
  }

  /**
   * Get the response for a failed password reset link.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  string  $response
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
   */
  protected function sendResetLinkFailedResponse(Request $request, $response)
  {
      return back()->withErrors(
          ['email' => trans($response)]
      );
  }

  /**
   * Validate the email for the given request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return void
   */
  protected function validateEmail(Request $request)
  {
    $this->validate($request, ['email' => 'required|email']);
  }
}
