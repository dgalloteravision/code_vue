<?php

namespace Procredito\Http\Controllers;

use Procredito\User;
use Illuminate\Http\Request;
use Procredito\Modules\Seguridad\Models\TipoDocumento;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class UsuarioController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \DB::table('users')
          ->where('cod_estado', 1)
          ->orderBy('id','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('users')
            ->where('id', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'name' => 'required',
        'email' => 'required',
        'password' => 'required',
        'cod_tipo_documento' => 'required',
        'numero_documento_usuario' => 'required',
        'primer_nombre_usuario' => 'required',
        'segundo_nombre_usuario' => 'required',
        'primer_apellido_usuario' => 'required',
        'segundo_apellido_usuario' => 'required',
        'nombre_usuario_usuario' => 'required'
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $fecha           = date('Y-m-d');
      $nuevafecha      = strtotime ( '+5 month' , strtotime ( $fecha ) ) ;
      $nuevafecha      = date ( 'Y-m-d', $nuevafecha );

      $usuario = User::create([
        'name'                        => $request->name,
        'email'                       => $request->email,
        'password'                    => $request->password,
        'imagen'                      => 'http://procredito.com/img/default_avatar_male.jpg',
        'cod_tipo_documento'          => $request->cod_tipo_documento,
        'numero_documento_usuario'    => $request->numero_documento_usuario,
        'primer_nombre_usuario'       => $request->primer_nombre_usuario,
        'segundo_nombre_usuario'      => $request->segundo_nombre_usuario,
        'primer_apellido_usuario'     => $request->primer_apellido_usuario,
        'segundo_apellido_usuario'    => $request->segundo_apellido_usuario,
        'nombre_usuario_usuario'      => $request->nombre_usuario_usuario,
        'cod_estado'                  => Estado::ACTIVE,
        'caducidad_cuenta_usuario'    => $nuevafecha,
        'fecha_creacion_usuario'      => date('Y-m-d h:i:s'),
        'fecha_modificacion_usuario'  => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Usuario registrado correctamente!!',
        'data'    => $usuario
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Usuario no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'name' => 'required',
        'email' => 'required',
        'cod_tipo_documento' => 'required',
        'numero_documento_usuario' => 'required',
        'primer_nombre_usuario' => 'required',
        'segundo_nombre_usuario' => 'required',
        'primer_apellido_usuario' => 'required',
        'segundo_apellido_usuario' => 'required',
        'nombre_usuario_usuario' => 'required'
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $usuario = User::find($id);
      $usuario->name                       = $request->name;
      $usuario->email                      = $request->email;
      $usuario->cod_tipo_documento         = $request->cod_tipo_documento;
      $usuario->numero_documento_usuario   = $request->numero_documento_usuario;
      $usuario->primer_nombre_usuario      = $request->primer_nombre_usuario;
      $usuario->segundo_nombre_usuario     = $request->segundo_nombre_usuario;
      $usuario->primer_apellido_usuario    = $request->primer_apellido_usuario;
      $usuario->segundo_apellido_usuario   = $request->segundo_apellido_usuario;
      $usuario->nombre_usuario_usuario     = $request->nombre_usuario_usuario;
      $usuario->cod_estado                 = Estado::ACTIVE;
      $usuario->fecha_modificacion_usuario = date('Y-m-d h:i:s');
      $usuario->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Usuario actualizado correctamente!!',
        'data'    => $usuario
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Usuario no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $usuario = User::destroy($id);
      if($usuario) {
        $message = 'Usuario eliminado correctamente!!';
      } else {
        $message = 'El usuario no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $usuario
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Usuario no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
