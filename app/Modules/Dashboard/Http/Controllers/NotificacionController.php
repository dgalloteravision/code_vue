<?php

namespace Procredito\Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Dashboard\Models\Notificacion;

class NotificacionController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return Notificacion::where('cod_estado', 1)->orderBy('cod_notificacion','DESC')->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'tipo_notificacion' => 'required',
      'descripcion_notificacion' => 'required',
    ]);

    $notificacion = new Notificacion();
    $notificacion->tipo_notificacion        = $request->tipo_notificacion;
    $notificacion->descripcion_notificacion = $request->descripcion_notificacion;
    $notificacion->cod_estado               = 1;
    $notificacion->cod_usuario_notificacion = auth()->id;
    $notificacion->save();

    return $notificacion;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $notificacion = Notificacion::find($id);
    $notificacion->tipo_notificacion        = $request->tipo_notificacion;
    $notificacion->descripcion_notificacion = $request->descripcion_notificacion;
    $notificacion->cod_estado               = 1;
    $notificacion->cod_usuario_notificacion = auth()->id;
    $notificacion->save();

    return $notificacion;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $notificacion = Notificacion::find($id);
    $notificacion->delete();
  }
}
