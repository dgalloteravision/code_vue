<?php

namespace Procredito\Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Dashboard\Models\Tarea;

class TareaController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return Tarea::where('cod_estado', 1)->orderBy('cod_tarea','DESC')->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'descripcion_tarea' => 'required',
    ]);

    $tarea = new Tarea();
    $tarea->descripcion_tarea        = $request->descripcion_tarea;
    $tarea->cod_estado               = 1;
    $tarea->cod_usuario_modificacion = auth()->id;
    $tarea->save();

    return $tarea;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $tarea = Tarea::find($id);
    $tarea->descripcion_tarea        = $request->descripcion_tarea;
    $tarea->cod_estado               = 1;
    $tarea->cod_usuario_modificacion = auth()->id;
    $tarea->save();

    return $tarea;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $tarea = Tarea::find($id);
    $tarea->delete();
  }
}
