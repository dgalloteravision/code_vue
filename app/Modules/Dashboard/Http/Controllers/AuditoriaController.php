<?php

namespace Procredito\Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Dashboard\Models\Auditoria;

class AuditoriaController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return Auditoria::where('cod_estado', 1)->orderBy('cod_auditoria','DESC')->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'accion_realizada_auditoria' => 'required',
    ]);

    $auditoria = new Auditoria();
    $auditoria->cod_usuario_realizo        = auth()->id;
    $auditoria->cod_usuario_afecto         = auth()->id;
    $auditoria->accion_realizada_auditoria = $request->accion_realizada_auditoria;
    $auditoria->cod_estado                 = 1;
    $auditoria->cod_usuario_auditoria      = auth()->id;
    $auditoria->save();

    return $auditoria;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $auditoria = Auditoria::find($id);
    $auditoria->cod_usuario_realizo        = auth()->id;
    $auditoria->cod_usuario_afecto         = auth()->id;
    $auditoria->accion_realizada_auditoria = $request->accion_realizada_auditoria;
    $auditoria->cod_estado                 = 1;
    $auditoria->cod_usuario_auditoria      = auth()->id;
    $auditoria->save();

    return $auditoria;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $auditoria = Auditoria::find($id);
    $auditoria->delete();
  }
}
