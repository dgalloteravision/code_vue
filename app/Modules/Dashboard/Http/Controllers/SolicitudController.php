<?php

namespace Procredito\Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Dashboard\Models\Solicitud;

class SolicitudController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return Solicitud::where('cod_estado', 1)->orderBy('cod_solicitud','DESC')->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'tipo_solicitud' => 'required',
      'descripcion_solicitud' => 'required',
    ]);

    $solicitud = new Solicitud();
    $solicitud->tipo_solicitud        = $request->tipo_solicitud;
    $solicitud->descripcion_solicitud = $request->descripcion_solicitud;
    $solicitud->cod_estado            = 1;
    $solicitud->cod_usuario_solicitud = auth()->id;
    $solicitud->save();

    return $solicitud;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $solicitud = Solicitud::find($id);
    $solicitud->tipo_solicitud        = $request->tipo_solicitud;
    $solicitud->descripcion_solicitud = $request->descripcion_solicitud;
    $solicitud->cod_estado            = 1;
    $solicitud->cod_usuario_solicitud = auth()->id;
    $solicitud->save();

    return $solicitud;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $solicitud = Solicitud::find($id);
    $solicitud->delete();
  }
}
