<?php

namespace Procredito\Modules\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'auditoria';
  protected $primaryKey = 'cod_auditoria';

  protected $fillable = [
    'cod_usuario_realizo',
    'cod_usuario_afecto',
    'accion_realizada_auditoria',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_auditoria',
    'fecha_modificacion_auditoria'
  ];
}
