<?php

namespace Procredito\Modules\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'solicitud';
  protected $primaryKey = 'cod_solicitud';

  protected $fillable = [
    'tipo_solicitud',
    'descripcion_solicitud',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_solicitud',
    'fecha_modificacion_solicitud'
  ];
}
