<?php

namespace Procredito\Modules\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'tarea';
  protected $primaryKey = 'cod_tarea';

  protected $fillable = [
    'descripcion_tarea',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_tarea',
    'fecha_modificacion_tarea'
  ];
}
