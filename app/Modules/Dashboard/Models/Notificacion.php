<?php

namespace Procredito\Modules\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'notificacion';
  protected $primaryKey = 'cod_notificacion';

  protected $fillable = [
    'tipo_notificacion',
    'descripcion_notificacion',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_notificacion',
    'fecha_modificacion_notificacion'
  ];
}
