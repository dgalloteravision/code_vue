<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud', function (Blueprint $table) {
          $table->increments('cod_solicitud');
          $table->string('tipo_solicitud', 100);
          $table->string('descripcion_solicitud', 255);
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_solicitud')->nullable();
          $table->dateTime('fecha_modificacion_solicitud')->nullable();
          $table->timestamps();
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud');
    }
}
