<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarea', function (Blueprint $table) {
          $table->increments('cod_tarea');
          $table->string('descripcion_tarea', 255);
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_tarea')->nullable();
          $table->dateTime('fecha_modificacion_tarea')->nullable();
          $table->timestamps();
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarea');
    }
}
