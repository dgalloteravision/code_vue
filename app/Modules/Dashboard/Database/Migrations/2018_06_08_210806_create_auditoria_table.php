<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditoria', function (Blueprint $table) {
          $table->increments('cod_auditoria');
          $table->integer('cod_usuario_realizo');
          $table->integer('cod_usuario_afecto');
          $table->string('accion_realizada_auditoria', 100);
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_auditoria')->nullable();
          $table->dateTime('fecha_modificacion_auditoria')->nullable();
          $table->timestamps();
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditoria');
    }
}
