<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\SeguridadPregunta;
use JWTAuth;

class SeguridadPreguntaController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \DB::table('seguridad_pregunta')
          ->where('cod_estado', 1)
          ->orderBy('seguridad_pregunta','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('seguridad_pregunta')
            ->where('seguridad_pregunta', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'texto_pregunta' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $seguridadpregunta = SeguridadPregunta::create([
        'cod_usuario'                           => $user->id,
        'texto_pregunta'                        => $request->texto_pregunta,
        'cod_estado'                            => Estado::ACTIVE,
        'cod_usuario_modificacion'              => $user->id,
        'fecha_creacion_seguridad_pregunta'     => date('Y-m-d h:i:s'),
        'fecha_modificacion_seguridad_pregunta' => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Seguridad pregunta registrado correctamente!!',
        'data'    => $seguridadpregunta
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Seguridad pregunta no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_estado' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $seguridadpregunta = SeguridadPregunta::find($id);
      $seguridadpregunta->cod_usuario                           = $user->id;
      $seguridadpregunta->texto_pregunta                        = $request->texto_pregunta;
      $seguridadpregunta->cod_estado                            = Estado::ACTIVE;
      $seguridadpregunta->cod_usuario_modificacion              = $user->id;
      $seguridadpregunta->fecha_modificacion_seguridad_pregunta = date('Y-m-d h:i:s');
      $seguridadpregunta->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Seguridad pregunta actualizada correctamente!!',
        'data'    => $seguridadpregunta
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Seguridad pregunta no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $seguridadpregunta  = SeguridadPregunta::destroy($id);
      if($seguridadpregunta) {
        $message = 'Seguridad pregunta eliminada correctamente!!';
      } else {
        $message = 'La seguridad pregunta no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $seguridadpregunta
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Seguridad pregunta no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
