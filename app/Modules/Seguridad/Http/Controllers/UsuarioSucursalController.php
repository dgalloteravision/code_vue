<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\UsuarioSucursal;
use Procredito\Modules\Seguridad\Models\Estado;

class UsuarioSucursalController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return UsuarioSucursal::where('cod_estado', 1)->orderBy('cod_usuario_sucursal','DESC')->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'cod_usuario' => 'required',
      'cod_sucursal' => 'required',
    ]);

    $usuariosucursal = new UsuarioSucursal();
    $usuariosucursal->cod_usuario              = $request->cod_usuario;
    $usuariosucursal->cod_sucursal             = $request->cod_sucursal;
    $usuariosucursal->cod_estado               = Estado::ACTIVE;
    $usuariosucursal->cod_usuario_modificacion = auth()->id;
    $usuariosucursal->save();

    return $usuariosucursal;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $usuariosucursal = UsuarioSucursal::find($id);
    $usuariosucursal->cod_usuario              = $request->cod_usuario;
    $usuariosucursal->cod_sucursal             = $request->cod_sucursal;
    $usuariosucursal->cod_estado               = Estado::ACTIVE;
    $usuariosucursal->cod_usuario_modificacion = auth()->id;
    $usuariosucursal->save();

    return $usuariosucursal;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $usuariosucursal = UsuarioSucursal::find($id);
    $usuariosucursal->delete();
  }
}
