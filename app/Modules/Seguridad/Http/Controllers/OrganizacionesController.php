<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Organizaciones;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class OrganizacionesController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \DB::table('organizaciones')
          ->where('cod_estado', 1)
          ->orderBy('cod_organizacion','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('organizaciones')
            ->where('cod_organizacion', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'cod_municipio' => 'required',
        'nombre_organizacion' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $organizaciones = Organizaciones::create([
        'cod_municipio'                   => $request->cod_municipio,
        'nombre_organizacion'             => $request->nombre_organizacion,
        'cod_estado'                      => Estado::ACTIVE,
        'cod_usuario_modificacion'        => $user->id,
        'fecha_creacion_organizacion'     => date('Y-m-d h:i:s'),
        'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Organización registrada correctamente!!',
        'data'    => $organizaciones
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Organización no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'cod_municipio' => 'required',
        'nombre_organizacion' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $organizaciones = Organizaciones::find($id);
      $organizaciones->cod_municipio                   = $request->cod_municipio;
      $organizaciones->nombre_organizacion             = $request->nombre_organizacion;
      $organizaciones->cod_estado                      = Estado::ACTIVE;
      $organizaciones->cod_usuario_modificacion        = $user->id;
      $organizaciones->fecha_modificacion_organizacion = date('Y-m-d h:i:s');
      $organizaciones->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Organizacion actualizada correctamente!!',
        'data'    => $organizaciones
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Organizacion no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $organizaciones = Organizaciones::destroy($id);
      if($organizaciones) {
        $message = 'Organizacion eliminada correctamente!!';
      } else {
        $message = 'La organizacion no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $organizaciones
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Organizacion no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
