<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Usuario;

class UsuarioController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return Usuario::where('cod_estado', 1)->orderBy('cod_usuario','DESC')->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'cod_tipo_documento' => 'required',
      'numero_documento_usuario' => 'required',
      'primer_nombre_usuario' => 'required',
      'segundo_nombre_usuario' => 'required',
      'primer_apellido_usuario' => 'required',
      'segundo_apellido_usuario' => 'required',
      'nombre_usuario_usuario' => 'required',
      'password_usuario_usuario' => 'required',
      'correo_usuario' => 'required',
    ]);

    $usuario = new Usuario();
    $usuario->cod_tipo_documento       = $request->cod_tipo_documento;
    $usuario->numero_documento_usuario = $request->numero_documento_usuario;
    $usuario->primer_nombre_usuario    = $request->primer_nombre_usuario;
    $usuario->segundo_nombre_usuario   = $request->segundo_nombre_usuario;
    $usuario->primer_apellido_usuario  = $request->primer_apellido_usuario;
    $usuario->segundo_apellido_usuario = $request->segundo_apellido_usuario;
    $usuario->nombre_usuario_usuario   = $request->nombre_usuario_usuario;
    $usuario->password_usuario_usuario = $request->password_usuario_usuario;
    $usuario->correo_usuario           = $request->correo_usuario;
    $usuario->horarios_acceso_usuario  = $request->horarios_acceso_usuario;
    $usuario->ip_acceso_usuario        = $request->ip_acceso_usuario;
    $usuario->caducidad_cuenta_usuario = $request->caducidad_cuenta_usuario;
    $usuario->vencimiento_cuenta_usuario= $request->vencimiento_cuenta_usuario;
    $usuario->activacion_cuenta_temporal_usuario= $request->activacion_cuenta_temporal_usuario;
    $usuario->terminacion_cuenta_temporal_usuario= $request->terminacion_cuenta_temporal_usuario;
    $usuario->ultima_vez_conexion_usuario= $request->ultima_vez_conexion_usuario;
    $usuario->intentos_acceso_usuario  = $request->intentos_acceso_usuario;
    $usuario->cod_usuario_modificacion = auth()->id;
    $usuario->save();

    return $usuario;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $usuario = Usuario::find($id);
    $usuario->cod_tipo_documento       = $request->cod_tipo_documento;
    $usuario->numero_documento_usuario = $request->numero_documento_usuario;
    $usuario->primer_nombre_usuario    = $request->primer_nombre_usuario;
    $usuario->segundo_nombre_usuario   = $request->segundo_nombre_usuario;
    $usuario->primer_apellido_usuario  = $request->primer_apellido_usuario;
    $usuario->segundo_apellido_usuario = $request->segundo_apellido_usuario;
    $usuario->nombre_usuario_usuario   = $request->nombre_usuario_usuario;
    $usuario->password_usuario_usuario = $request->password_usuario_usuario;
    $usuario->correo_usuario           = $request->correo_usuario;
    $usuario->horarios_acceso_usuario  = $request->horarios_acceso_usuario;
    $usuario->ip_acceso_usuario        = $request->ip_acceso_usuario;
    $usuario->caducidad_cuenta_usuario = $request->caducidad_cuenta_usuario;
    $usuario->vencimiento_cuenta_usuario= $request->vencimiento_cuenta_usuario;
    $usuario->activacion_cuenta_temporal_usuario= $request->activacion_cuenta_temporal_usuario;
    $usuario->terminacion_cuenta_temporal_usuario= $request->terminacion_cuenta_temporal_usuario;
    $usuario->ultima_vez_conexion_usuario= $request->ultima_vez_conexion_usuario;
    $usuario->intentos_acceso_usuario  = $request->intentos_acceso_usuario;
    $usuario->cod_usuario_modificacion = auth()->id;
    $usuario->save();

    return $usuario;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $usuario = Usuario::find($id);
    $usuario->delete();
  }
}
