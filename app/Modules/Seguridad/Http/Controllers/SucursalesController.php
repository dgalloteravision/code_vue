<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Sucursales;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class SucursalesController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \DB::table('sucursales')
          ->where('cod_estado', 1)
          ->orderBy('cod_sucursal','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('sucursales')
            ->where('cod_sucursal', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'cod_organizacion' => 'required',
        'cod_municipio' => 'required',
        'nombre_sucursal' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $sucursal = Sucursales::create([
        'cod_organizacion'            => $request->cod_organizacion,
        'cod_municipio'               => $request->cod_municipio,
        'nombre_sucursal'             => $request->nombre_sucursal,
        'cod_estado'                  => Estado::ACTIVE,
        'cod_usuario_modificacion'    => $user->id,
        'fecha_creacion_sucursal'     => date('Y-m-d h:i:s'),
        'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Sucursal registrada correctamente!!',
        'data'    => $sucursal
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Sucursal no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'cod_organizacion' => 'required',
        'cod_municipio' => 'required',
        'nombre_sucursal' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $sucursal = Sucursales::find($id);
      $sucursal->cod_municipio               = $request->cod_municipio;
      $sucursal->nombre_sucursal             = $request->nombre_sucursal;
      $sucursal->cod_estado                  = Estado::ACTIVE;
      $sucursal->cod_usuario_modificacion    = $user->id;
      $sucursal->fecha_modificacion_sucursal = date('Y-m-d h:i:s');
      $sucursal->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Sucursal actualizada correctamente!!',
        'data'    => $sucursal
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Sucursal no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $sucursal  = Sucursales::destroy($id);
      if($sucursal) {
        $message = 'Sucursal eliminada correctamente!!';
      } else {
        $message = 'La sucursal no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $sucursal
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Sucursal no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
