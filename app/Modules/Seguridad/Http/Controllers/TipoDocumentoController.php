<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\TipoDocumento;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class TipoDocumentoController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $tiposdocumento = \DB::table('tipo_documento')
          ->where('cod_estado', 1)
          ->orderBy('cod_tipo_documento','DESC')
          ->paginate(15);

    return [
      'total'        => $tiposdocumento->total(),
      'current_page' => $tiposdocumento->currentPage(),
      'per_page'     => $tiposdocumento->perPage(),
      'last_page'    => $tiposdocumento->lastPage(),
      'from'         => $tiposdocumento->firstItem(),
      'to'           => $tiposdocumento->lastPage(),
      'data'         => $tiposdocumento
    ];
  }

  public function show($id)
  {
    return \DB::table('tipo_documento')
            ->where('cod_tipo_documento', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_tipo_documento' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $tipodocumento = TipoDocumento::create([
        'nombre_tipo_documento'             => $request->nombre_tipo_documento,
        'cod_estado'                        => Estado::ACTIVE,
        'cod_usuario_modificacion'          => $user->id,
        'fecha_creacion_tipo_documento'     => date('Y-m-d h:i:s'),
        'fecha_modificacion_tipo_documento' => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Tipo documento registrado correctamente!!',
        'data'    => $tipodocumento
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Tipo documento no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_tipo_documento' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $tipodocumento = TipoDocumento::find($id);
      $tipodocumento->nombre_tipo_documento             = $request->nombre_tipo_documento;
      $tipodocumento->cod_estado                        = Estado::ACTIVE;
      $tipodocumento->cod_usuario_modificacion          = $user->id;
      $tipodocumento->fecha_modificacion_tipo_documento = date('Y-m-d h:i:s');
      $tipodocumento->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Tipo documento actualizado correctamente!!',
        'data'    => $tipodocumento
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Tipo documento no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $tipodocumento  = TipoDocumento::destroy($id);
      if($tipodocumento) {
        $message = 'Tipo documento eliminado correctamente!!';
      } else {
        $message = 'El tipo documento no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $tipodocumento
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Tipo documento no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
