<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Configuracion;
use JWTAuth;

class ConfiguracionController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \DB::table('configuration')
          ->where('cod_estado', 1)
          ->orderBy('cod_configuracion','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('configuration')
            ->where('cod_configuracion', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'contrasenia_usuarios_importados_configuracion' => 'required',
        'dias_caducidad_usuarios_configuracion'         => 'required',
        'horarios_acceso_usuarios_configuracion'        => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $configuracion = Configuracion::create([
        'contrasenia_usuarios_importados_configuracion' => $request->contrasenia_usuarios_importados_configuracion,
        'dias_caducidad_usuarios_configuracion'         => $request->dias_caducidad_usuarios_configuracion,
        'horarios_acceso_usuarios_configuracion'        => $request->horarios_acceso_usuarios_configuracion,
        'cod_estado'                                    => Estado::ACTIVE,
        'cod_usuario_modificacion'                      => $user->id,
        'fecha_creacion_configuracion'                  => date('Y-m-d h:i:s'),
        'fecha_modificacion_configuracion'              => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Configuracion registrada correctamente!!',
        'data'    => $configuracion
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Configuracion no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'contrasenia_usuarios_importados_configuracion' => 'required',
        'dias_caducidad_usuarios_configuracion'         => 'required',
        'horarios_acceso_usuarios_configuracion'        => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $configuracion = Configuracion::find($id);
      $configuracion->contrasenia_usuarios_importados_configuracion = $request->contrasenia_usuarios_importados_configuracion;
      $configuracion->dias_caducidad_usuarios_configuracion         = $request->dias_caducidad_usuarios_configuracion;
      $configuracion->horarios_acceso_usuarios_configuracion        = $request->horarios_acceso_usuarios_configuracion;
      $configuracion->cod_estado                                    = Estado::ACTIVE;
      $configuracion->cod_usuario_modificacion                      = $user->id;
      $configuracion->fecha_modificacion_configuracion              = date('Y-m-d h:i:s');
      $configuracion->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Configuracion actualizada correctamente!!',
        'data'    => $configuracion
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Configuracion no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $configuracion  = Configuracion::destroy($id);
      if($configuracion) {
        $message = 'Configuracion eliminado correctamente!!';
      } else {
        $message = 'El configuracion no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $configuracion
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Configuracion no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
