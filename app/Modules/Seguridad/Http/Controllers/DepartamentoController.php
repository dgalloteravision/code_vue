<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Departamento;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class DepartamentoController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $departamentos =  \DB::table('departamento')
          ->where('cod_estado', 1)
          ->orderBy('cod_departamento','DESC')
          ->paginate(15);

    return [
      'total'        => $departamentos->total(),
      'current_page' => $departamentos->currentPage(),
      'per_page'     => $departamentos->perPage(),
      'last_page'    => $departamentos->lastPage(),
      'from'         => $departamentos->firstItem(),
      'to'           => $departamentos->lastPage(),
      'data'         => $departamentos
    ];
  }

  public function show($id)
  {
    return \DB::table('departamento')
            ->where('cod_departamento', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_departamento' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $departamento = Departamento::create([
        'nombre_departamento'             => $request->nombre_departamento,
        'cod_estado'                      => Estado::ACTIVE,
        'cod_usuario_modificacion'        => $user->id,
        'fecha_creacion_departamento'     => date('Y-m-d h:i:s'),
        'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Departamento registrado correctamente!!',
        'data'    => $departamento
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Departamento no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_departamento' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $departamento = Departamento::find($id);
      $departamento->nombre_departamento             = $request->nombre_departamento;
      $departamento->cod_estado                      = Estado::ACTIVE;
      $departamento->cod_usuario_modificacion        = $user->id;
      $departamento->fecha_modificacion_departamento = date('Y-m-d h:i:s');
      $departamento->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Departamento actualizado correctamente!!',
        'data'    => $departamento
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Departamento no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $departamento  = Departamento::destroy($id);
      if($departamento) {
        $message = 'Departamento eliminado correctamente!!';
      } else {
        $message = 'El departamento no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $departamento
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Departamento no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
