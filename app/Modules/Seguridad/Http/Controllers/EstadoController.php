<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class EstadoController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $estados = \DB::table('estado')
          ->where('estado', 1)
          ->orderBy('cod_estado','DESC')
          ->paginate(15);

    return [
      'total'        => $estados->total(),
      'current_page' => $estados->currentPage(),
      'per_page'     => $estados->perPage(),
      'last_page'    => $estados->lastPage(),
      'from'         => $estados->firstItem(),
      'to'           => $estados->lastPage(),
      'data'         => $estados
    ];
  }

  public function show($id)
  {
    return \DB::table('estado')
            ->where('cod_estado', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_estado' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $estado = Estado::create([
        'nombre_estado'            => $request->nombre_estado,
        'estado'                   => Estado::ACTIVE,
        'cod_usuario_modificacion' => $user->id,
        'fecha_creacion_estado'    => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado'=> date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Estado registrado correctamente!!',
        'data'    => $estado
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Estado no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_estado' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $estado = Estado::find($id);
      $estado->nombre_estado             = $request->nombre_estado;
      $estado->estado                    = Estado::ACTIVE;
      $estado->cod_usuario_modificacion  = $user->id;
      $estado->fecha_modificacion_estado = date('Y-m-d h:i:s');
      $estado->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Estado actualizado correctamente!!',
        'data'    => $estado
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Estado no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $estado  = Estado::destroy($id);
      if($estado) {
        $message = 'Estado eliminado correctamente!!';
      } else {
        $message = 'El estado no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $estado
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Estado no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
