<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Rol;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class RolController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
    // $this->middleware('auth:api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \DB::table('rol')
          ->where('cod_estado', Estado::ACTIVE)
          ->orderBy('cod_rol','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('rol')
            ->where('cod_rol', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_rol' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $rol = Rol::create([
        'nombre_rol'               => $request->nombre_rol,
        'cod_estado'               => Estado::ACTIVE,
        'cod_usuario_modificacion' => $user->id,
        'fecha_creacion_rol'       => date('Y-m-d h:i:s'),
        'fecha_modificacion_rol'   => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Rol registrado correctamente!!',
        'data'    => $rol
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Rol no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_rol' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $rol = Rol::find($id);
      $rol->nombre_rol               = $request->nombre_rol;
      $rol->cod_estado               = Estado::ACTIVE;
      $rol->cod_usuario_modificacion = $user->id;
      $rol->fecha_modificacion_rol   = date('Y-m-d h:i:s');
      $rol->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Rol actualizado correctamente!!',
        'data'    => $rol
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Rol no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $rol = Rol::destroy($id);
      if($rol) {
        $message = 'Rol eliminado correctamente!!';
      } else {
        $message = 'El rol no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $rol
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Rol no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
