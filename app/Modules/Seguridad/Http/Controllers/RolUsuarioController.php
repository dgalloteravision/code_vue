<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\RolUsuario;

class RolUsuarioController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return RolUsuario::where('cod_estado', 1)->orderBy('cod_rol_usuario','DESC')->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'cod_rol' => 'required',
      'cod_usuario' => 'required',
    ]);

    $rolusuario = new RolUsuario();
    $rolusuario->cod_rol                  = $request->cod_rol;
    $rolusuario->cod_usuario              = $request->cod_usuario;
    $rolusuario->cod_estado               = 1;
    $rolusuario->cod_usuario_modificacion = auth()->id;
    $rolusuario->save();

    return $rolusuario;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $rolusuario = RolUsuario::find($id);
    $rolusuario->cod_rol                  = $request->cod_rol;
    $rolusuario->cod_usuario              = $request->cod_usuario;
    $rolusuario->cod_estado               = 1;
    $rolusuario->cod_usuario_modificacion = auth()->id;
    $rolusuario->save();

    return $rolusuario;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $rolusuario = RolUsuario::find($id);
    $rolusuario->delete();
  }
}
