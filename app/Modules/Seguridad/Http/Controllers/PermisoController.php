<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Permiso;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class PermisoController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    return \DB::table('permiso')
          ->where('cod_estado', Estado::ACTIVE)
          ->orderBy('cod_permiso','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('permiso')
            ->where('cod_permiso', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_permiso' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $permiso = Permiso::create([
        'nombre_permiso'             => $request->nombre_permiso,
        'cod_estado'                 => Estado::ACTIVE,
        'cod_usuario_modificacion'   => $user->id,
        'fecha_creacion_permiso'     => date('Y-m-d h:i:s'),
        'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Permiso registrado correctamente!!',
        'data'    => $permiso
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Permiso no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_permiso' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $permiso = Permiso::find($id);
      $permiso->nombre_permiso             = $request->nombre_permiso;
      $permiso->cod_estado                 = Estado::ACTIVE;
      $permiso->cod_usuario_modificacion   = $user->id;
      $permiso->fecha_creacion_permiso     = date('Y-m-d h:i:s');
      $permiso->fecha_modificacion_permiso = date('Y-m-d h:i:s');
      $permiso->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Permiso actualizado correctamente!!',
        'data'    => $permiso
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Permiso no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $permiso  = Permiso::destroy($id);
      if($permiso) {
        $message = 'Permiso eliminado correctamente!!';
      } else {
        $message = 'El permiso no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $permiso
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Permiso no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
