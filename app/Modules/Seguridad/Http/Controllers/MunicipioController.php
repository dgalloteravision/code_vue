<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Municipio;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class MunicipioController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $municipios = \DB::table('municipio')
          ->where('cod_estado', 1)
          ->orderBy('cod_municipio','DESC')
          ->paginate(15);

    return [
      'total'        => $municipios->total(),
      'current_page' => $municipios->currentPage(),
      'per_page'     => $municipios->perPage(),
      'last_page'    => $municipios->lastPage(),
      'from'         => $municipios->firstItem(),
      'to'           => $municipios->lastPage(),
      'data'         => $municipios
    ];
  }

  public function show($id)
  {
    return \DB::table('municipio')
            ->where('cod_municipio', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'cod_departamento' => 'required',
        'nombre_municipio' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $municipio = Municipio::create([
        'cod_departamento'            => $request->cod_departamento,
        'nombre_municipio'            => $request->nombre_municipio,
        'cod_estado'                  => Estado::ACTIVE,
        'cod_usuario_modificacion'    => $user->id,
        'fecha_creacion_municipio'    => date('Y-m-d h:i:s'),
        'fecha_modificacion_municipio'=> date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Municipio registrado correctamente!!',
        'data'    => $municipio
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Municipio no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'cod_departamento' => 'required',
        'nombre_municipio' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $municipio = Municipio::find($id);
      $municipio->cod_departamento             = $request->cod_departamento;
      $municipio->nombre_municipio             = $request->nombre_municipio;
      $municipio->cod_estado                   = Estado::ACTIVE;
      $municipio->cod_usuario_modificacion     = $user->id;
      $municipio->fecha_modificacion_municipio = date('Y-m-d h:i:s');
      $municipio->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Municipio actualizado correctamente!!',
        'data'    => $municipio
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Municipio no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $municipio  = Municipio::destroy($id);
      if($municipio) {
        $message = 'Municipio eliminado correctamente!!';
      } else {
        $message = 'El municipio no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $municipio
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Municipio no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
