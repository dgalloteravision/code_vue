<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\PasswordUsuario;

class PasswordUsuarioController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return PasswordUsuario::where('cod_estado', 1)->orderBy('cod_password_usuario','DESC')->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'password_password_usuario' => 'required',
      'password_audio_respuesta_password_usuario' => 'required',
    ]);

    $passwordusuario = new PasswordUsuario();
    $passwordusuario->password_password_usuario                 = $request->password_password_usuario;
    $passwordusuario->password_audio_respuesta_password_usuario = $request->password_audio_respuesta_password_usuario;
    $passwordusuario->cod_estado                                = 1;
    $passwordusuario->cod_usuario_modificacion                  = auth()->id;
    $passwordusuario->save();

    return $passwordusuario;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $passwordusuario = PasswordUsuario::find($id);
    $passwordusuario->password_password_usuario                 = $request->password_password_usuario;
    $passwordusuario->password_audio_respuesta_password_usuario = $request->password_audio_respuesta_password_usuario;
    $passwordusuario->cod_estado                                = 1;
    $passwordusuario->cod_usuario_modificacion                  = auth()->id;
    $passwordusuario->save();

    return $passwordusuario;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $passwordusuario = PasswordUsuario::find($id);
    $passwordusuario->delete();
  }
}
