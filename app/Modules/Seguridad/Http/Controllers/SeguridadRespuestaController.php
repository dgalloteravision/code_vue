<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\SeguridadRespuesta;
use Procredito\Modules\Seguridad\Models\Estado;

class SeguridadRespuestaController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \DB::table('seguridad_respuesta')
          ->where('cod_estado', 1)
          ->orderBy('cod_seguridad_respuesta','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('seguridad_respuesta')
            ->where('cod_seguridad_respuesta', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'cod_seguridad_pregunta' => 'required',
        'respuesta_seguridad_respuesta' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $seguridadrespuesta = SeguridadRespuesta::create([
        'cod_seguridad_pregunta'                 => $request->cod_seguridad_pregunta,
        'respuesta_seguridad_respuesta'          => $request->respuesta_seguridad_respuesta,
        'cod_estado'                             => Estado::ACTIVE,
        'cod_usuario_modificacion'               => $user->id,
        'fecha_creacion_seguridad_respuesta'     => date('Y-m-d h:i:s'),
        'fecha_modificacion_seguridad_respuesta' => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Seguridad respuesta registrada correctamente!!',
        'data'    => $seguridadrespuesta
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Seguridad respuesta no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'cod_seguridad_pregunta' => 'required',
        'respuesta_seguridad_respuesta' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $seguridadrespuesta = SeguridadRespuesta::find($id);
      $seguridadrespuesta->cod_seguridad_pregunta        = $request->cod_seguridad_pregunta;
      $seguridadrespuesta->respuesta_seguridad_respuesta = $request->respuesta_seguridad_respuesta;
      $seguridadrespuesta->cod_estado                    = Estado::ACTIVE;
      $seguridadrespuesta->cod_usuario_modificacion      = $user->id;
      $seguridadrespuesta->fecha_modificacion_seguridad_respuesta = date('Y-m-d h:i:s');
      $seguridadrespuesta->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Seguridad respuesta actualizada correctamente!!',
        'data'    => $seguridadrespuesta
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Seguridad respuesta no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $seguridadrespuesta  = SeguridadRespuesta::destroy($id);
      if($sucursal) {
        $message = 'Seguridad respuesta eliminada correctamente!!';
      } else {
        $message = 'La Seguridad respuesta no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $seguridadrespuesta
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Seguridad respuesta no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
