<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\RolPermiso;

class RolPermisoController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return RolPermiso::where('cod_estado', 1)->orderBy('cod_rol_permiso','DESC')->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'cod_rol' => 'required',
      'cod_permiso' => 'required',
    ]);

    $rolpermiso = new RolPermiso();
    $rolpermiso->cod_rol                  = $request->cod_rol;
    $rolpermiso->cod_permiso              = $request->cod_permiso;
    $rolpermiso->cod_estado               = 1;
    $rolpermiso->cod_usuario_modificacion = auth()->id;
    $rolpermiso->save();

    return $rolpermiso;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $rolpermiso = RolPermiso::find($id);
    $rolpermiso->cod_rol                  = $request->cod_rol;
    $rolpermiso->cod_permiso              = $request->cod_permiso;
    $rolpermiso->cod_estado               = 1;
    $rolpermiso->cod_usuario_modificacion = auth()->id;
    $rolpermiso->save();

    return $rolpermiso;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $rolpermiso = RolPermiso::find($id);
    $rolpermiso->delete();
  }
}
