<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;

use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Ubicacion;
use Procredito\Modules\Seguridad\Models\Estado;

class UbicacionController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \DB::table('ubicacion')
          ->where('cod_estado', 1)
          ->orderBy('cod_ubicacion','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('ubicacion')
            ->where('cod_ubicacion', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'cod_departamento'    => 'required',
        'cod_municipio'       => 'required',
        'direccion_ubicacion' => 'required',
        'telefono1_ubicacion' => 'required',
        'telefono2_ubicacion' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $ubicacion = Ubicacion::create([
        'cod_usuario'                  => $user->id,
        'cod_departamento'             => $request->cod_departamento,
        'cod_municipio'                => $request->cod_municipio,
        'direccion_ubicacion'          => $request->direccion_ubicacion,
        'telefono1_ubicacion'          => $request->telefono1_ubicacion,
        'telefono2_ubicacion'          => $request->telefono2_ubicacion,
        'cod_estado'                   => Estado::ACTIVE,
        'cod_usuario_modificacion'     => $user->id,
        'fecha_creacion_ubicacion'     => date('Y-m-d h:i:s'),
        'fecha_modificacion_ubicacion' => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Ubicación registrada correctamente!!',
        'data'    => $ubicacion
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Ubicación no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_estado' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $ubicacion = Ubicacion::find($id);
      $ubicacion->cod_departamento             = $request->cod_departamento;
      $ubicacion->cod_municipio                = $request->cod_municipio;
      $ubicacion->direccion_ubicacion          = $request->direccion_ubicacion;
      $ubicacion->telefono1_ubicacion          = $request->telefono1_ubicacion;
      $ubicacion->telefono2_ubicacion          = $request->telefono2_ubicacion;
      $ubicacion->cod_estado                   = Estado::ACTIVE;
      $ubicacion->cod_usuario_modificacion     = $user->id;
      $ubicacion->fecha_modificacion_ubicacion = date('Y-m-d h:i:s');
      $ubicacion->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Ubicación actualizada correctamente!!',
        'data'    => $ubicacion
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Ubicación no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $ubicacion  = Ubicacion::destroy($id);
      if($ubicacion) {
        $message = 'Ubicacion eliminada correctamente!!';
      } else {
        $message = 'La ubicacion no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $ubicacion
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Ubicación no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
