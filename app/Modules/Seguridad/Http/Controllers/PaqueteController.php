<?php

namespace Procredito\Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use Procredito\Http\Controllers\Controller;
use Procredito\Modules\Seguridad\Models\Paquete;
use Procredito\Modules\Seguridad\Models\Estado;
use JWTAuth;

class PaqueteController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \DB::table('paquete')
          ->where('cod_estado', Estado::ACTIVE)
          ->orderBy('cod_paquete','DESC')
          ->paginate(15);
  }

  public function show($id)
  {
    return \DB::table('paquete')
            ->where('cod_paquete', $id)
            ->get();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_paquete' => 'required',
        'descripcion_paquete' => 'required',
        'especificacion_paquete' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $paquete = Paquete::create([
        'nombre_paquete'             => $request->nombre_paquete,
        'descripcion_paquete'        => $request->descripcion_paquete,
        'especificacion_paquete'     => $request->especificacion_paquete,
        'cod_estado'                 => Estado::ACTIVE,
        'cod_usuario_modificacion'   => $user->id,
        'fecha_creacion_paquete'     => date('Y-m-d h:i:s'),
        'fecha_modificacion_paquete' => date('Y-m-d h:i:s')
      ]);
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Paquete registrado correctamente!!',
        'data'    => $paquete
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Paquete no se pudo registrar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      \DB::beginTransaction();

      $this->validate($request, [
        'nombre_paquete' => 'required',
        'descripcion_paquete' => 'required',
        'especificacion_paquete' => 'required',
      ]);

      $user = JWTAuth::parseToken()->toUser();

      $paquete = Paquete::find($id);
      $paquete->nombre_paquete             = $request->nombre_paquete;
      $paquete->descripcion_paquete        = $request->descripcion_paquete;
      $paquete->especificacion_paquete     = $request->especificacion_paquete;
      $paquete->cod_estado                 = Estado::ACTIVE;
      $paquete->cod_usuario_modificacion   = $user->id;
      $paquete->fecha_modificacion_paquete = date('Y-m-d h:i:s');
      $paquete->save();

      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => 'Paquete actualizado correctamente!!',
        'data'    => $paquete
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Paquete no se pudo actualizar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      \DB::beginTransaction();

      $paquete = Paquete::destroy($id);
      if($paquete) {
        $message = 'Paquete eliminado correctamente!!';
      } else {
        $message = 'El paquete no existe';
      }
      $success = true;
    } catch (\Exception $exception) {
      \DB::rollBack();
      $success = $exception->getMessage();
    }

    if ($success === true) {
      \DB::commit();
      return response([
        'status'  => 'success',
        'code'    => 200,
        'message' => $message,
        'data'    => $paquete
      ], 200);
    } else {
      return response([
        'status'  => 'error',
        'code'    => 500,
        'message' => 'Paquete no se pudo eliminar correctamente!!',
        'data'    => $success
      ], 500);
    }
  }
}
