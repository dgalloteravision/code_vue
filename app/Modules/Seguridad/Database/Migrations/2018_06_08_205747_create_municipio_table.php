<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipio', function (Blueprint $table) {
          $table->increments('cod_municipio');
          $table->integer('cod_departamento');
          $table->string('nombre_municipio', 100);
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_municipio')->nullable();
          $table->dateTime('fecha_modificacion_municipio')->nullable();
          $table->timestamps();
          $table->foreign('cod_departamento')->references('cod_departamento')->on('departamento');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipio');
    }
}
