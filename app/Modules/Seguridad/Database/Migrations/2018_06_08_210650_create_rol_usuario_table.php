<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol_usuario', function (Blueprint $table) {
          $table->increments('cod_rol_usuario');
          $table->integer('cod_rol');
          $table->integer('cod_usuario');
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_rol_usuario')->nullable();
          $table->dateTime('fecha_modificacion_rol_usuario')->nullable();
          $table->timestamps();
          $table->foreign('cod_rol')->references('cod_rol')->on('rol');
          $table->foreign('cod_usuario')->references('id')->on('users');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol_usuario');
    }
}
