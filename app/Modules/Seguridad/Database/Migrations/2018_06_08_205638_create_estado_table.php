<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado', function (Blueprint $table) {
            $table->increments('cod_estado');
            $table->string('nombre_estado', 50);
            $table->tinyInteger('estado');
            $table->integer('cod_usuario_modificacion');
            $table->dateTime('fecha_creacion_estado')->nullable();
            $table->dateTime('fecha_modificacion_estado')->nullable();
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estado');
    }
}
