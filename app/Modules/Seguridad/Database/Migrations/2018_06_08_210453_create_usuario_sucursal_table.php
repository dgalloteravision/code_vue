<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioSucursalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_sucursal', function (Blueprint $table) {
          $table->increments('cod_usuario_sucursal');
          $table->integer('cod_usuario');
          $table->integer('cod_sucursal');
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_usuario_sucursal')->nullable();
          $table->dateTime('fecha_modificacion_usuario_sucursal')->nullable();
          $table->timestamps();
          $table->foreign('cod_usuario')->references('id')->on('users');
          $table->foreign('cod_sucursal')->references('cod_sucursal')->on('sucursales');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_sucursal');
    }
}
