<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departamento', function (Blueprint $table) {
            $table->increments('cod_departamento');
            $table->string('nombre_departamento', 100);
            $table->integer('cod_estado');
            $table->integer('cod_usuario_modificacion');
            $table->dateTime('fecha_creacion_departamento')->nullable();
            $table->dateTime('fecha_modificacion_departamento')->nullable();
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departamento');
    }
}
