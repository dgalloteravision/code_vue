<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Nota: Esta tabla ya no aplica para esto se utiliza password_resets la que crea por defecto laravel
        /*Schema::create('token', function (Blueprint $table) {
          $table->increments('cod_token');
          $table->integer('cod_usuario');
          $table->string('tipo_token', 50);
          $table->string('token_token', 100);
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_token')->nullable();
          $table->dateTime('fecha_modificacion_token')->nullable();
          $table->timestamps();
          $table->foreign('cod_usuario')->references('id')->on('users');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('token');
    }
}
