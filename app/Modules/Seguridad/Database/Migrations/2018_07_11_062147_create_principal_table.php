<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrincipalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizaciones', function (Blueprint $table) {
            $table->increments('cod_organizacion');
            $table->integer('cod_municipio');
            $table->string('nombre_organizacion', 200);
            $table->string('email_organizacion')->unique()->nullable();
            $table->integer('cod_estado');
            $table->integer('cod_usuario_modificacion');
            $table->dateTime('fecha_creacion_organizacion')->nullable();
            $table->dateTime('fecha_modificacion_organizacion')->nullable();
            $table->timestamps();
            $table->foreign('cod_municipio')->references('cod_municipio')->on('municipio');
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizaciones');
    }
}
