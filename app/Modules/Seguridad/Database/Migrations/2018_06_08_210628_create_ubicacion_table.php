<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbicacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubicacion', function (Blueprint $table) {
          $table->increments('cod_ubicacion');
          $table->integer('cod_usuario');
          $table->integer('cod_departamento');
          $table->integer('cod_municipio');
          $table->string('direccion_ubicacion', 100);
          $table->string('telefono1_ubicacion', 100);
          $table->string('telefono2_ubicacion', 100)->nullable();
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_ubicacion')->nullable();
          $table->dateTime('fecha_modificacion_ubicacion')->nullable();
          $table->timestamps();
          $table->foreign('cod_usuario')->references('id')->on('users');
          $table->foreign('cod_departamento')->references('cod_departamento')->on('departamento');
          $table->foreign('cod_municipio')->references('cod_municipio')->on('municipio');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ubicacion');
    }
}
