<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeguridadRespuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguridad_respuesta', function (Blueprint $table) {
          $table->increments('cod_seguridad_respuesta');
          $table->integer('cod_seguridad_pregunta');
          $table->string('respuesta_seguridad_respuesta', 255);
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_seguridad_respuesta')->nullable();
          $table->dateTime('fecha_modificacion_seguridad_respuesta')->nullable();
          $table->timestamps();
          $table->foreign('cod_seguridad_pregunta')->references('cod_seguridad_pregunta')->on('seguridad_pregunta');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seguridad_respuesta');
    }
}
