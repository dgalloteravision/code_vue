<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('password_usuario', function (Blueprint $table) {
          $table->increments('cod_password_usuario');
          $table->integer('cod_usuario');
          $table->string('password_password_usuario', 100);
          $table->string('password_audio_respuesta_password_usuario', 100);
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_password_usuario')->nullable();
          $table->dateTime('fecha_modificacion_password_usuario')->nullable();
          $table->timestamps();
          $table->foreign('cod_usuario')->references('id')->on('users');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_usuario');
    }
}
