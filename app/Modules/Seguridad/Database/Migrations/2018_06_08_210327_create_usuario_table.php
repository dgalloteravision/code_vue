<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
          $table->increments('cod_usuario');
          $table->integer('cod_tipo_documento');
          $table->string('numero_documento_usuario', 20);
          $table->string('primer_nombre_usuario', 50);
          $table->string('segundo_nombre_usuario', 50)->nullable();
          $table->string('primer_apellido_usuario', 50);
          $table->string('segundo_apellido_usuario', 50)->nullable();
          $table->string('nombre_usuario_usuario', 20)->nullable();
          $table->string('password_usuario_usuario', 50)->nullable();
          $table->string('correo_usuario', 50)->unique();
          $table->text('horarios_acceso_usuario')->nullable();
          $table->string('ip_acceso_usuario', 20);
          $table->dateTime('caducidad_cuenta_usuario');
          $table->dateTime('vencimiento_cuenta_usuario');
          $table->dateTime('activacion_cuenta_temporal_usuario')->nullable();
          $table->dateTime('terminacion_cuenta_temporal_usuario')->nullable();
          $table->dateTime('ultima_vez_conexion_usuario');
          $table->tinyInteger('intentos_acceso_usuario');
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_usuario')->nullable();
          $table->dateTime('fecha_modificacion_usuario')->nullable();
          $table->timestamps();
          $table->foreign('cod_tipo_documento')->references('cod_tipo_documento')->on('tipo_documento');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });

        /*DB:table('usuarios')->insert(
          [
            'cod_tipo_documento'        => 1,
            'numero_documento_usuario'  => '1053764579',
            'primer_nombre_usuario'     => 'Administrador',
            'primer_apellido_usuario'   => 'Procredito',
            'nombre_usuario_usuario'    => 'superadmin',
            'password_usuario_usuario'  => Hash::make('123456'),
            'correo_usuario'            => 'dany338@gmail.com',
            'ip_acceso_usuario'         => '192.168.10.10',
            'caducidad_cuenta_usuario'  => '2018-07-01 11:00:00',
            'vencimiento_cuenta_usuario'=> '2018-07-01 11:00:00',
            'ultima_vez_conexion_usuario'=> '2018-07-01 11:00:00',
            'intentos_acceso_usuario'   => 0,
            'cod_estado'                => 1,
            'cod_usuario_modificacion'  => 1,
            'fecha_creacion_usuario'    => '2018-07-01 11:00:00',
            'fecha_modificacion_usuario'=> '2018-07-01 11:00:00'
          ]
        );*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
