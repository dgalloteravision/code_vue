<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSucursalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales', function (Blueprint $table) {
          $table->increments('cod_sucursal');
          $table->integer('cod_organizacion');
          $table->integer('cod_municipio');
          $table->string('nombre_sucursal', 200);
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_sucursal')->nullable();
          $table->dateTime('fecha_modificacion_sucursal')->nullable();
          $table->timestamps();
          $table->foreign('cod_municipio')->references('cod_municipio')->on('municipio');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursal');
        Schema::dropIfExists('sucursales');
    }
}
