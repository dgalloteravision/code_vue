<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSucursalesPaqueteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales_paquete', function (Blueprint $table) {
          $table->increments('cod_sucursal_paquete');
          $table->integer('cod_sucursal');
          $table->integer('cod_paquete');
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_sucursal_paquete')->nullable();
          $table->dateTime('fecha_modificacion_sucursal_paquete')->nullable();
          $table->timestamps();
          $table->foreign('cod_sucursal')->references('cod_sucursal')->on('sucursales');
          $table->foreign('cod_paquete')->references('cod_paquete')->on('paquete');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursales_paquete');
    }
}
