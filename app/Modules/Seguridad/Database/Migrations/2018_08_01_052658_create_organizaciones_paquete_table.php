<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizacionesPaqueteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizaciones_paquete', function (Blueprint $table) {
          $table->increments('cod_organizacion_paquete');
          $table->integer('cod_organizacion');
          $table->integer('cod_paquete');
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_organizacion_paquete')->nullable();
          $table->dateTime('fecha_modificacion_organizacion_paquete')->nullable();
          $table->timestamps();
          $table->foreign('cod_organizacion')->references('cod_organizacion')->on('organizaciones');
          $table->foreign('cod_paquete')->references('cod_paquete')->on('paquete');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizaciones_paquete');
    }
}
