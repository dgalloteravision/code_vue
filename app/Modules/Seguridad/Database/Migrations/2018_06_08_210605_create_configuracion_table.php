<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracion', function (Blueprint $table) {
          $table->increments('cod_configuracion');
          $table->integer('cod_usuario');
          $table->string('contrasenia_usuarios_importados_configuracion', 20);
          $table->integer('dias_caducidad_usuarios_configuracion');
          $table->text('horarios_acceso_usuarios_configuracion')->nullable();
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_configuracion')->nullable();
          $table->dateTime('fecha_modificacion_configuracion')->nullable();
          $table->timestamps();
          $table->foreign('cod_usuario')->references('id')->on('users');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuracion');
    }
}
