<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_documento', function (Blueprint $table) {
          $table->increments('cod_tipo_documento');
          $table->string('nombre_tipo_documento', 50);
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_tipo_documento')->nullable();
          $table->dateTime('fecha_modificacion_tipo_documento')->nullable();
          $table->timestamps();
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });

        /*DB:table('tipo_documento')->insert(
          [
            'nombre_tipo_documento'     => 'Cedula de Ciudadania',
            'cod_estado'                => 1,
            'cod_usuario_modificacion'  => 1,
            'fecha_creacion_usuario'    => '2018-07-01 11:00:00',
            'fecha_modificacion_usuario'=> '2018-07-01 11:00:00'
          ]
        );*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_documento');
    }
}
