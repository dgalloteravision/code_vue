<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioPermisoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // NOTA: ESTA TABLA YA NO APLICA
        /*Schema::create('usuario_permiso', function (Blueprint $table) {
          $table->increments('cod_usuario_permiso');
          $table->integer('cod_rol_usuario');
          $table->integer('cod_rol_permiso');
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_usuario_permiso')->nullable();
          $table->dateTime('fecha_modificacion_usuario_permiso')->nullable();
          $table->timestamps();
          $table->foreign('cod_rol_usuario')->references('cod_rol_usuario')->on('rol_usuario');
          $table->foreign('cod_rol_permiso')->references('cod_rol_permiso')->on('rol_permiso');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_permiso');
    }
}
