<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolPermisoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol_permiso', function (Blueprint $table) {
          $table->increments('cod_rol_permiso');
          $table->integer('cod_rol');
          $table->integer('cod_permiso');
          $table->integer('cod_estado');
          $table->integer('cod_usuario_modificacion');
          $table->dateTime('fecha_creacion_rol_permiso')->nullable();
          $table->dateTime('fecha_modificacion_rol_permiso')->nullable();
          $table->timestamps();
          $table->foreign('cod_rol')->references('cod_rol')->on('rol');
          $table->foreign('cod_permiso')->references('cod_permiso')->on('permiso');
          $table->charset = 'utf8';
          $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol_permiso');
    }
}
