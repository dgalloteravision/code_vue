<?php

namespace Procredito\Modules\Seguridad\Database\Seeds;

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\Departamento;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Departamento::create([
        'nombre_departamento' => 'Antioquia',
        'cod_estado'=> 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
        'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
      ]);

      Departamento::create([
        'nombre_departamento' => 'Caldas',
        'cod_estado'=> 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
        'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
      ]);

      Departamento::create([
        'nombre_departamento' => 'Cundinamarca',
        'cod_estado'=> 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
        'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
      ]);
    }
}
