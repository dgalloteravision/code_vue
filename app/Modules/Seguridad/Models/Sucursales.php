<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;
use Procredito\User;

class Sucursales extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'sucursales';
  protected $primaryKey = 'cod_sucursal';

  protected $fillable = [
    'cod_organizacion',
    'cod_municipio',
    'nombre_sucursal',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_sucursal',
    'fecha_modificacion_sucursal'
  ];

  /**
   * Get the municipio that owns the sucursal.
   */
  public function municipio()
  {
    return $this->belongsTo(Municipio::class, 'cod_municipio', 'cod_municipio')->select('cod_municipio','nombre_municipio');
  }

  /**
   * Get the post that owns the comment.
   */
  public function organizacion()
  {
    return $this->belongsTo(Organizaciones::class, 'cod_organizacion', 'cod_organizacion')->select('cod_organizacion','nombre_organizacion');
  }

  public function usuarios()
  {
    return $this->belongsToMany(User::class, 'usuario_sucursal', 'cod_sucursal', 'cod_usuario');
  }

  public function usuariossucursal()
  {
    return $this->hasMany(UsuarioSucursal::class, 'cod_sucursal', 'cod_sucursal');
  }

  public function sucursalpaquetes()
  {
    return $this->hasMany(SucursalesPaquete::class, 'cod_sucursal', 'cod_sucursal');
  }
}
