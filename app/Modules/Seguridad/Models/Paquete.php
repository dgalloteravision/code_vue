<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'paquete';
  protected $primaryKey = 'cod_paquete';

  protected $fillable = [
    'nombre_paquete',
    'descripcion_paquete',
    'especificacion_paquete',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_paquete',
    'fecha_modificacion_paquete'
  ];

  public function paqueteorganizacion()
  {
    return $this->hasMany(OrganizacionesPaquete::class, 'cod_paquete', 'cod_paquete');
  }

  public function paquetesucursal()
  {
    return $this->hasMany(SucursalesPaquete::class, 'cod_paquete', 'cod_paquete');
  }
}
