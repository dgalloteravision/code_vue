<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioSucursal extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'usuario_sucursal';
  protected $primaryKey = 'cod_usuario_sucursal';

  protected $fillable = [
    'cod_usuario',
    'cod_sucursal',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_usuario_sucursal',
    'fecha_modificacion_usuario_sucursal'
  ];

  public function usuario()
  {
    return $this->hasOne(User::class, 'id', 'cod_usuario');
  }

  public function sucursal()
  {
    return $this->hasOne(Sucursales::class, 'cod_sucursal', 'cod_sucursal');
  }
}
