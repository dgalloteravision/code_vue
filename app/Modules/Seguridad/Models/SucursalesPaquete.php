<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class SucursalesPaquete extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'organizaciones_paquete';
  protected $primaryKey = 'cod_sucursal_paquete';

  protected $fillable = [
    'cod_sucursal',
    'cod_paquete',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_sucursal_paquete',
    'fecha_modificacion_sucursal_paquete'
  ];

  public function sucursal()
  {
    return $this->hasOne(Sucursales::class, 'cod_sucursal', 'cod_sucursal');
  }

  public function paquete()
  {
    return $this->hasOne(Paquete::class, 'cod_paquete', 'cod_paquete');
  }
}
