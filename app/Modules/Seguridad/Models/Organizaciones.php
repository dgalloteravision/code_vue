<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class Organizaciones extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'organizaciones';
  protected $primaryKey = 'cod_organizacion';

  protected $fillable = [
    'cod_municipio',
    'nombre_organizacion',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_organizacion',
    'fecha_modificacion_organizacion'
  ];

  /**
   * Get the municipio that owns the principal.
   */
  public function municipio()
  {
    return $this->belongsTo(Municipio::class, 'cod_municipio', 'cod_municipio')->select('cod_municipio','nombre_municipio');
  }

  /**
   * Get the sucursales for the organizaciones.
   */
  public function sucursales()
  {
    return $this->hasMany(Sucursales::class, 'cod_organizacion', 'cod_organizacion')->select('cod_sucursal','cod_organizacion','nombre_sucursal');
  }

  public function usuarios()
  {
    return $this->belongsToMany(User::class, 'organizaciones_usuario', 'cod_organizacion', 'cod_usuario');
  }

  public function organizacionusuarios()
  {
    return $this->hasMany(OrganizacionesUsuario::class, 'cod_organizacion', 'cod_organizacion');
  }

  public function organizacionpaquetes()
  {
    return $this->hasMany(OrganizacionesPaquete::class, 'cod_organizacion', 'cod_organizacion');
  }

}
