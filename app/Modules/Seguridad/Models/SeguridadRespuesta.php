<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class SeguridadRespuesta extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'seguridad_respuesta';
  protected $primaryKey = 'cod_seguridad_pregunta';

  protected $fillable = [
    'cod_seguridad_pregunta',
    'respuesta_seguridad_respuesta',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_seguridad_respuesta',
    'fecha_modificacion_seguridad_respuesta'
  ];

  /**
   * Get the usuario that owns the seguridad pregunta.
   */
  public function seguridadpregunta()
  {
    return $this->belongsTo(SeguridadPregunta::class, 'cod_seguridad_pregunta', 'cod_seguridad_pregunta')->select('cod_seguridad_pregunta','texto_pregunta');
  }
}
