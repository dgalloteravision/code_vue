<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'usuario';

  protected $fillable = [
    'cod_tipo_documento',
    'numero_documento_usuario',
    'primer_nombre_usuario',
    'segundo_nombre_usuario',
    'primer_apellido_usuario',
    'segundo_apellido_usuario',
    'nombre_usuario_usuario',
    'password_usuario_usuario',
    'correo_usuario',
    'horarios_acceso_usuario',
    'ip_acceso_usuario',
    'caducidad_cuenta_usuario',
    'vencimiento_cuenta_usuario',
    'activacion_cuenta_temporal_usuario',
    'terminacion_cuenta_temporal_usuario',
    'ultima_vez_conexion_usuario',
    'intentos_acceso_usuario',
  ];
}
