<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;
use Procredito\User;

class Rol extends Model
{
  const SUPERADMIN        = 1;
  const ADMINSUCURSAL     = 2;
  const ADMINORGANIZACION = 3;
  const NORMAL            = 4;
  const COMERCIANTE       = 5;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'rol';
  protected $primaryKey = 'cod_rol';

  protected $fillable = [
    'nombre_rol',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_rol',
    'fecha_modificacion_rol'
  ];

  public function usuarios()
  {
    return $this->belongsToMany(User::class, 'rol_usuario', 'cod_rol', 'cod_usuario');
  }

  public function permisos()
  {
    return $this->belongsToMany(Permiso::class, 'rol_permiso', 'cod_rol', 'cod_permiso');
  }

  public function rolusuarios()
  {
    return $this->hasMany(RolUsuario::class, 'cod_rol', 'cod_rol');
  }

  public function rolpermisos()
  {
    return $this->hasMany(RolPermiso::class, 'cod_rol', 'cod_rol');
  }
}
