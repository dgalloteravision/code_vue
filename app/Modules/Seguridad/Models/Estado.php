<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
  const ACTIVE = 1; // Activo
  const INACTIVE = 2; // Inactivo
  const BLOCKED = 3; // Bloqueado
  const SLOPE = 4; // Pendiente
  const LOGIC = 5; // Borrado logico
  const UNASSIGNED = 6; // Sin asignar
  const ASSIGNED = 7; // Asignado
  const PROCESS = 8; // Proceso
  const FINALIZED = 9; // Finalizado
  const CANCELLED = 10; // Cancelado

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'estado';
  protected $primaryKey = 'cod_estado';

  protected $fillable = [
    'nombre_estado',
    'estado',
    'cod_usuario_modificacion',
    'fecha_creacion_estado',
    'fecha_modificacion_estado'
  ];
}
