<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'municipio';
  protected $primaryKey = 'cod_municipio';

  protected $fillable = [
    'cod_departamento',
    'nombre_municipio',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_municipio',
    'fecha_modificacion_municipio'
  ];

  /**
     * All of the relationships to be touched.
     *
     * @var array
     */
  //protected $touches = ['departamento'];

  public function departamento()
  {
    return $this->belongsTo(Departamento::class, 'cod_departamento', 'cod_departamento', 'cod_departamento')->select('cod_departamento', 'nombre_departamento');
  }

  public function organizaciones()
  {
    return $this->hasMany(Organizaciones::class, 'cod_municipio', 'cod_municipio')->select('cod_organizacion','nombre_organizacion');
  }

  public function sucursales()
  {
    return $this->hasMany(Sucursales::class, 'cod_municipio', 'cod_municipio')->select('cod_sucursal','nombre_sucursal');
  }
}
