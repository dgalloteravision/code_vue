<?php

namespace Procredito\Modules\Seguridad\Models;
use Procredito\User;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
  const CC = 1;
  const CE = 2;
  const NIT = 3;
  const PASSPORT = 4;
  const OTHER = 5;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'tipo_documento';
  protected $primaryKey = 'cod_tipo_documento';

  protected $fillable = [
    'nombre_tipo_documento',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_tipo_documento',
    'fecha_modificacion_tipo_documento'
  ];

  /**
   * Get the usuarios for the tipo documento.
   */
  public function usuarios()
  {
    return $this->hasMany(User::class, 'cod_tipo_documento', 'cod_tipo_documento')->select('id','cod_tipo_documento','name');
  }

}
