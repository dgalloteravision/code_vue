<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class RolPermiso extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'rol_permiso';
  protected $primaryKey = 'cod_rol_permiso';

  protected $fillable = [
    'cod_rol',
    'cod_permiso',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_rol_permiso',
    'fecha_modificacion_rol_permiso'
  ];

  public function rol()
  {
    return $this->hasOne(Rol::class, 'cod_rol', 'cod_rol');
  }

  public function permiso()
  {
    return $this->hasOne(User::class, 'cod_permiso', 'cod_permiso');
  }
}
