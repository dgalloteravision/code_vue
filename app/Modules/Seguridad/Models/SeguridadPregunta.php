<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;
use Procredito\User;

class SeguridadPregunta extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'seguridad_pregunta';
  protected $primaryKey = 'cod_seguridad_pregunta';

  protected $fillable = [
    'cod_usuario',
    'texto_pregunta',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_seguridad_pregunta',
    'fecha_modificacion_seguridad_pregunta'
  ];

  /**
   * Get the usuario that owns the seguridad pregunta.
   */
  public function usuario()
  {
    return $this->belongsTo(User::class, 'cod_usuario', 'id')->select('id','name');
  }

  public function seguridadrespuesta()
  {
    return $this->hasOne(SeguridadRespuesta::class, 'cod_seguridad_pregunta', 'cod_seguridad_pregunta');
  }
}
