<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizacionesPaquete extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'organizaciones_paquete';
  protected $primaryKey = 'cod_organizacion_paquete';

  protected $fillable = [
    'cod_organizacion',
    'cod_paquete',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_organizacion_paquete',
    'fecha_modificacion_organizacion_paquete'
  ];

  public function organizacion()
  {
    return $this->hasOne(Organizaciones::class, 'cod_organizacion', 'cod_organizacion');
  }

  public function paquete()
  {
    return $this->hasOne(Paquete::class, 'cod_paquete', 'cod_paquete');
  }
}
