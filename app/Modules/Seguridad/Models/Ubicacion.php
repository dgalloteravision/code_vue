<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;
use Procredito\User;

class Ubicacion extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'ubicacion';
  protected $primaryKey = 'cod_ubicacion';

  protected $fillable = [
    'cod_usuario',
    'cod_departamento',
    'cod_municipio',
    'direccion_ubicacion',
    'telefono1_ubicacion',
    'telefono2_ubicacion',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_ubicacion',
    'fecha_modificacion_ubicacion'
  ];

  /**
   * Get the usuario that owns the ubicación.
   */
  public function usuario()
  {
    return $this->belongsTo(User::class, 'id', 'cod_usuario')->select('id','name');
  }

  /**
   * Get the departamento that owns the ubicacion.
   */
  public function departamento()
  {
    return $this->belongsTo(Departamento::class, 'cod_departamento', 'cod_departamento')->select('cod_departamento','nombre_departamento');
  }

  /**
   * Get the departamento that owns the ubicacion.
   */
  public function municipio()
  {
    return $this->belongsTo(Municipio::class, 'cod_municipio', 'cod_municipio')->select('cod_municipio','nombre_municipio');
  }
}
