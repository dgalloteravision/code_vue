<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;
use Procredito\User;

class Configuracion extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'configuracion';
  protected $primaryKey = 'cod_configuracion';

  protected $fillable = [
    'cod_usuario',
    'contrasenia_usuarios_importados_configuracion',
    'dias_caducidad_usuarios_configuracion',
    'horarios_acceso_usuarios_configuracion',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_configuracion',
    'fecha_modificacion_configuracion'
  ];

  /**
   * Get the usuario that owns the configuration.
   */
  public function usuario()
  {
    return $this->belongsTo(User::class, 'cod_usuario', 'id')->select('id','name');
  }
}
