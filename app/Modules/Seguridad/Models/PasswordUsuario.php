<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;
use Procredito\User;

class PasswordUsuario extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'password_usuario';
  protected $primaryKey = 'cod_password_usuario';

  protected $fillable = [
    'cod_usuario',
    'password_password_usuario',
    'password_audio_respuesta_password_usuario',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_password_usuario',
    'fecha_modificacion_password_usuario'
  ];

  /**
   * Get the usuario that owns the password usuario.
   */
  public function usuario()
  {
    return $this->belongsTo(User::class, 'cod_usuario', 'id')->select('id','name');
  }
}
