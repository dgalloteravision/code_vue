<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'departamento';
  protected $primaryKey = 'cod_departamento';

  protected $fillable = [
    'nombre_departamento',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_departamento',
    'fecha_modificacion_departamento'
  ];

  /**
   * Get the municipios for the departamento.
   */
  public function municipios()
  {
    return $this->hasMany(Municipio::class, 'cod_departamento')->select('cod_municipio','cod_departamento','nombre_municipio');
  }
}
