<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;
use Procredito\User;

class RolUsuario extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'rol_usuario';
  protected $primaryKey = 'cod_rol_usuario';

  protected $fillable = [
    'cod_rol',
    'cod_usuario',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_rol_usuario',
    'fecha_modificacion_rol_usuario'
  ];

  public function usuario()
  {
    return $this->hasOne(User::class, 'id', 'cod_usuario');
  }

  public function rol()
  {
    return $this->hasOne(Rol::class, 'cod_rol', 'cod_rol');
  }

}
