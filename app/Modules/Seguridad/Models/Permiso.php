<?php

namespace Procredito\Modules\Seguridad\Models;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'permiso';
  protected $primaryKey = 'cod_permiso';

  protected $fillable = [
    'nombre_permiso',
    'cod_estado',
    'cod_usuario_modificacion',
    'fecha_creacion_permiso',
    'fecha_modificacion_permiso'
  ];

  /**
   * Get the roles that owns the permiso.
   */
  public function roles()
  {
    return $this->belongsToMany(Rol::class, 'rol_permiso', 'cod_permiso', 'cod_rol');
  }

  public function permisoroles()
  {
    return $this->hasMany(RolUsuario::class, 'cod_permiso', 'cod_permiso');
  }
}
