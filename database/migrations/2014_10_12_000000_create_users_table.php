<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('imagen')->nullable();
            $table->string('password');
            $table->integer('cod_tipo_documento')->nullable();
            $table->string('numero_documento_usuario', 20)->unique()->nullable();
            $table->string('primer_nombre_usuario', 50)->nullable();
            $table->string('segundo_nombre_usuario', 50)->nullable();
            $table->string('primer_apellido_usuario', 50)->nullable();
            $table->string('segundo_apellido_usuario', 50)->nullable();
            $table->string('nombre_usuario_usuario', 20)->nullable();
            $table->text('horarios_acceso_usuario')->nullable();
            $table->string('ip_acceso_usuario', 20)->nullable();
            $table->dateTime('caducidad_cuenta_usuario')->nullable();
            $table->dateTime('vencimiento_cuenta_usuario')->nullable();
            $table->dateTime('activacion_cuenta_temporal_usuario')->nullable();
            $table->dateTime('terminacion_cuenta_temporal_usuario')->nullable();
            $table->dateTime('ultima_vez_conexion_usuario')->nullable();
            $table->tinyInteger('intentos_acceso_usuario')->nullable();
            $table->rememberToken();
            $table->integer('cod_estado')->nullable();
            $table->dateTime('fecha_creacion_usuario')->nullable();
            $table->dateTime('fecha_modificacion_usuario')->nullable();
            $table->timestamps();
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
