<?php

use Faker\Generator as Faker;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Procredito\User::class, function (Faker $faker) {
  return [
    'name' => $faker->name,
    'email' => $faker->unique()->safeEmail,
    'imagen' => $faker->imageUrl(640, 480),//$faker->image('/public/img/default_avatar_male.jpg', 76, 76),
    'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // contraseña: secret
    'cod_tipo_documento' => 1,
    'numero_documento_usuario' => $faker->unique()->ssn,
    'primer_nombre_usuario' => $faker->firstName,
    'segundo_nombre_usuario' => $faker->lastName,
    'primer_apellido_usuario' => $faker->name,
    'segundo_apellido_usuario' => $faker->name,
    'nombre_usuario_usuario' => $faker->userName,
    'horarios_acceso_usuario' => $faker->dayOfWeek . ' ' . $faker->time . ' ' . $faker->amPm,
    'ip_acceso_usuario' => $faker->ipv4,
    'caducidad_cuenta_usuario' => $faker->dateTime('2018-11-31 23:55:15', 'America/Bogota'),
    'vencimiento_cuenta_usuario' => $faker->dateTime('2018-12-31 23:55:15', 'America/Bogota'),
    'activacion_cuenta_temporal_usuario' => $faker->dateTime('2019-01-01 23:55:15', 'America/Bogota'),
    'terminacion_cuenta_temporal_usuario' => $faker->dateTime('2019-01-30 23:55:15', 'America/Bogota'),
    'ultima_vez_conexion_usuario' => $faker->dateTime,
    'intentos_acceso_usuario' => $faker->randomDigit,
    'remember_token' => str_random(10),
    'cod_estado' => 1
  ];
});
