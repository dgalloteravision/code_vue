<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Municipio::class, function (Faker $faker) {
  return [
    /*'cod_departamento' =>  function () {
      return factory(Procredito\Modules\Seguridad\Models\Departamento::class)->create()->cod_departamento;
    },*/
    'nombre_municipio' => $faker->city,
    'cod_estado' => 1,
    'cod_usuario_modificacion' =>  function () {
      return factory(Procredito\User::class)->create()->id;
    },
    'fecha_creacion_municipio' => $faker->dateTime,
    'fecha_modificacion_municipio' => $faker->dateTime,
  ];
});
