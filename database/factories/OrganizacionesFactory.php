<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Organizaciones::class, function (Faker $faker) {
  return [
    'cod_municipio' => 1,
    'nombre_organizacion' => $faker->company,
    'email_organizacion' => $faker->unique()->companyEmail,
    'cod_estado' => 1,
    'cod_usuario_modificacion' =>  function () {
      return factory(Procredito\User::class)->create()->id;
    },
    'fecha_creacion_organizacion' => $faker->dateTime,
    'fecha_modificacion_organizacion' => $faker->dateTime,
  ];
});
