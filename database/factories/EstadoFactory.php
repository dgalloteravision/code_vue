<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Estado::class, function (Faker $faker) {
  return [
    'nombre_estado' => $faker->name,
    'estado' => 1,
    'cod_usuario_modificacion' =>  function () {
      return factory(Procredito\User::class)->create()->id;
    },
    'fecha_creacion_estado' => $faker->dateTime,
    'fecha_modificacion_estado' => $faker->dateTime,
  ];
});
