<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Paquete::class, function (Faker $faker) {
  return [
    'nombre_paquete' => $faker->name,
    'descripcion_paquete' => $faker->name,
    'especificacion_paquete' => '{'.$faker->name.'}',
    'cod_estado' => 1,
    'cod_usuario_modificacion' =>  function () {
      return factory(Procredito\User::class)->create()->id;
    },
    'fecha_creacion_paquete' => $faker->dateTime,
    'fecha_modificacion_paquete' => $faker->dateTime,
  ];
});
