<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Sucursales::class, function (Faker $faker) {
  return [
    'cod_municipio' => 1,
    'nombre_sucursal' => $faker->name,
    'cod_estado' => 1,
    'cod_usuario_modificacion' =>  function () {
      return factory(Procredito\User::class)->create()->id;
    },
    'fecha_creacion_sucursal' => $faker->dateTime,
    'fecha_modificacion_sucursal' => $faker->dateTime,
  ];
});
