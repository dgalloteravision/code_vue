<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\TipoDocumento::class, function (Faker $faker) {
  return [
    'nombre_tipo_documento' => $faker->name,
    'cod_estado' => 1,
    'cod_usuario_modificacion' =>  function () {
      return factory(Procredito\User::class)->create()->id;
    },
    'fecha_creacion_tipo_documento' => $faker->dateTime,
    'fecha_modificacion_tipo_documento' => $faker->dateTime,
  ];
});
