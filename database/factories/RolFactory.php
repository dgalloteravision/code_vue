<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Rol::class, function (Faker $faker) {
  return [
    'nombre_rol' => $faker->name,
    'cod_estado' => 1,
    'cod_usuario_modificacion' => 1,
    'fecha_creacion_rol' => $faker->dateTime,
    'fecha_modificacion_rol' => $faker->dateTime,
  ];
});
