<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Ubicacion::class, function (Faker $faker) {
  return [
    'cod_departamento' => 1,
    'cod_municipio' => 1,
    'direccion_ubicacion' => $faker->address,
    'telefono1_ubicacion' => $faker->e164PhoneNumber,
    'telefono2_ubicacion' => $faker->phoneNumber,
    'cod_estado' => 1,
    'cod_usuario_modificacion' =>  function () {
      return factory(Procredito\User::class)->create()->id;
    },
    'fecha_creacion_ubicacion' => $faker->dateTime,
    'fecha_modificacion_ubicacion' => $faker->dateTime,
  ];
});
