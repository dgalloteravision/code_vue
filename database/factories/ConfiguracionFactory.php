<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Configuracion::class, function (Faker $faker) {
  return [
    'contrasenia_usuarios_importados_configuracion' => 'secret',
    'dias_caducidad_usuarios_configuracion' => $faker->randomDigit,
    'horarios_acceso_usuarios_configuracion' => $faker->dayOfWeek . ' ' . $faker->time . ' ' . $faker->amPm,
    'cod_estado' => 1,
    'cod_usuario_modificacion' =>  1,
    'fecha_creacion_configuracion' => $faker->dateTime,
    'fecha_modificacion_configuracion' => $faker->dateTime,
  ];
});
