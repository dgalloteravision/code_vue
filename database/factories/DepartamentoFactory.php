<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Departamento::class, function (Faker $faker) {
    return [
      'nombre_departamento' => $faker->state,
      'cod_estado' => 1,
      'cod_usuario_modificacion' =>  function () {
        return factory(Procredito\User::class)->create()->id;
      },
      'fecha_creacion_departamento' => $faker->dateTime,
      'fecha_modificacion_departamento' => $faker->dateTime,
    ];
});
