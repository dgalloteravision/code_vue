<?php

use Faker\Generator as Faker;

$factory->define(Procredito\Modules\Seguridad\Models\Permiso::class, function (Faker $faker) {
  return [
    'nombre_permiso' => $faker->name,
    'cod_estado' => 1,
    'cod_usuario_modificacion' => 1,
    'fecha_creacion_permiso' => $faker->dateTime,
    'fecha_modificacion_permiso' => $faker->dateTime,
  ];
});
