<?php

use Procredito\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $this->call([
      TiposDocumentoTableSeeder::class,
      EstadosTableSeeder::class,
      DepartamentosTableSeeder::class,
      MunicipiosTableSeeder::class,
      UbicacionesTableSeeder::class,
      UsersTableSeeder::class,
      ConfiguracionesTableSeeder::class,
      OrganizacionesTableSeeder::class,
      SucursalesTableSeeder::class,
      RolesTableSeeder::class,
      PermisosTableSeeder::class,
      PaqueteTableSeeder::class,
    ]);
  }
}
