<?php

use Procredito\Modules\Seguridad\Models\Municipio;
use Illuminate\Database\Seeder;

class MunicipiosTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Medellín',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Abejorral',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Abriaquí',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Alejandría',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Amagá',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Amalfi',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Andes',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Angelópolis',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Angostura',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Anorí',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Santa Fé de Antioquia',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Anzá',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Apartadó',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Arboletes',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Argelia',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Itaguí',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);

    Municipio::create([
      'cod_departamento' => 1,
      'nombre_municipio' => 'Amagá',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_municipio' => date('Y-m-d h:i:s'),
      'fecha_modificacion_municipio' => date('Y-m-d h:i:s')
    ]);
  }
}
