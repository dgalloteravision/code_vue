<?php

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\Paquete;

class PaqueteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Paquete::create([
        'nombre_paquete' => 'Nombre paquete 1 - organización',
        'descripcion_paquete' => 'Descripción del paquete 1',
        'especificacion_paquete' => '{especificacion...}',
        'cod_estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_paquete' => date('Y-m-d h:i:s'),
        'fecha_modificacion_paquete' => date('Y-m-d h:i:s')
      ]);

      Paquete::create([
        'nombre_paquete' => 'Nombre paquete 2 - organización',
        'descripcion_paquete' => 'Descripción del paquete 2',
        'especificacion_paquete' => '{especificacion...}',
        'cod_estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_paquete' => date('Y-m-d h:i:s'),
        'fecha_modificacion_paquete' => date('Y-m-d h:i:s')
      ]);

      Paquete::create([
        'nombre_paquete' => 'Nombre paquete 1 - sucursal',
        'descripcion_paquete' => 'Descripción del paquete 1',
        'especificacion_paquete' => '{especificacion...}',
        'cod_estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_paquete' => date('Y-m-d h:i:s'),
        'fecha_modificacion_paquete' => date('Y-m-d h:i:s')
      ]);

      Paquete::create([
        'nombre_paquete' => 'Nombre paquete 2 - sucursal',
        'descripcion_paquete' => 'Descripción del paquete 2',
        'especificacion_paquete' => '{especificacion...}',
        'cod_estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_paquete' => date('Y-m-d h:i:s'),
        'fecha_modificacion_paquete' => date('Y-m-d h:i:s')
      ]);

      factory(Procredito\Modules\Seguridad\Models\Paquete::class, 35)->create()->each(function ($d) {

      });
    }
}
