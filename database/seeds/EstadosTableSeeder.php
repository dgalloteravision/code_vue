<?php

use Procredito\Modules\Seguridad\Models\Estado;
use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Representa el estado activo: registro visible a los usuarios.
      Estado::create([
        'nombre_estado' => 'Activo',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado de inactivo: registro no visible a los usuarios,
      Estado::create([
        'nombre_estado' => 'Inactivo',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado bloqueado: Aplica para los usuarios que ya están desactivados en el sistema provisional o indefinidamente.
      Estado::create([
        'nombre_estado' => 'Bloqueado',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado pendiente: es un estado que se aplicará a temas de cambio de contraseña dentro de un periodo definido.
      Estado::create([
        'nombre_estado' => 'Pendiente',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado lógico: corresponde a un borrado solo lógico por si más adelante se quiere restablecer ese registro.
      Estado::create([
        'nombre_estado' => 'Lógico',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado sin asignar: proceso solicitado en el sistema pero aún no ha sido atendido.
      Estado::create([
        'nombre_estado' => 'Sin Asignar',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado sin asignar: proceso solicitado en el sistema pero aún no ha sido atendido.
      Estado::create([
        'nombre_estado' => 'Sin Asignar',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado asignado: proceso tomado y asignado en progreso de gestión.
      Estado::create([
        'nombre_estado' => 'Asignado',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado en proceso: proceso que está siendo gestionado.
      Estado::create([
        'nombre_estado' => 'En Proceso',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado en finalizado: proceso que ha finalizado y se le ha dado respuesta.
      Estado::create([
        'nombre_estado' => 'Finalizado',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      // Representa el estado en cancelado: proceso anulado.
      Estado::create([
        'nombre_estado' => 'Cancelado',
        'estado' => 1,
        'cod_usuario_modificacion' => 1,
        'fecha_creacion_estado' => date('Y-m-d h:i:s'),
        'fecha_modificacion_estado' => date('Y-m-d h:i:s')
      ]);

      factory(Procredito\Modules\Seguridad\Models\Estado::class, 35)->create()->each(function ($d) {

      });
    }
}
