<?php

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\Ubicacion;

class UbicacionesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Ubicacion::create([
      'cod_usuario' => 1,
      'cod_departamento' => 1,
      'cod_municipio' => 1,
      'direccion_ubicacion' => 'Cra 1 # 11 - 41 - bosques de la libonia apto 806 torre b',
      'telefono1_ubicacion' => '3044689778',
      'telefono2_ubicacion' => '8915185',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_ubicacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_ubicacion' => date('Y-m-d h:i:s')
    ]);

    Ubicacion::create([
      'cod_usuario' => 2,
      'cod_departamento' => 1,
      'cod_municipio' => 1,
      'direccion_ubicacion' => 'Cra 43B # 70 - 35 - balconez de aranjuez',
      'telefono1_ubicacion' => '3044689778',
      'telefono2_ubicacion' => '8915185',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 2,
      'fecha_creacion_ubicacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_ubicacion' => date('Y-m-d h:i:s')
    ]);

    Ubicacion::create([
      'cod_usuario' => 3,
      'cod_departamento' => 1,
      'cod_municipio' => 1,
      'direccion_ubicacion' => 'Cra 43B # 70 - 35 - balconez de aranjuez',
      'telefono1_ubicacion' => '3044689778',
      'telefono2_ubicacion' => '8763291',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 2,
      'fecha_creacion_ubicacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_ubicacion' => date('Y-m-d h:i:s')
    ]);
  }
}
