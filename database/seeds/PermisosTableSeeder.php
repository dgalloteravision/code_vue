<?php

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\Permiso;

class PermisosTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Permiso::create([
      'nombre_permiso' => 'Listar Información',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Ver Información',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Actualizar Información',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Eliminar Información',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Reportes',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Usuarios',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Organizaciones',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Sucursales',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Perfiles',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Permisos',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Estados',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Tipos Documento',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Departamentos',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Municipios',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    Permiso::create([
      'nombre_permiso' => 'Gestionar Afiliados',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_permiso' => date('Y-m-d h:i:s'),
      'fecha_modificacion_permiso' => date('Y-m-d h:i:s')
    ]);

    factory(Procredito\Modules\Seguridad\Models\Permiso::class, 35)->create()->each(function ($p) {

    });
  }
}
