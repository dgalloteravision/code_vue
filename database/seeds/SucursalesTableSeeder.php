<?php

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\Sucursales;

class SucursalesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Sucursales::create([
      'cod_organizacion' => 1,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Flamingo Norte',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);

    Sucursales::create([
      'cod_organizacion' => 1,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Flamingo Sur',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);

    Sucursales::create([
      'cod_organizacion' => 1,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Flamingo Parque Berrío',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);

    Sucursales::create([
      'cod_organizacion' => 2,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Falabella Norte',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);

    Sucursales::create([
      'cod_organizacion' => 2,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Falabella Sur',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);

    Sucursales::create([
      'cod_organizacion' => 2,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Falabella Poblado',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);

    Sucursales::create([
      'cod_organizacion' => 3,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Éxito Norte',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);

    Sucursales::create([
      'cod_organizacion' => 3,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Éxito Norte',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);

    Sucursales::create([
      'cod_organizacion' => 3,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Éxito Centro',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);

    Sucursales::create([
      'cod_organizacion' => 3,
      'cod_municipio' => 1,
      'nombre_sucursal' => 'Éxito Itaguí',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_sucursal' => date('Y-m-d h:i:s'),
      'fecha_modificacion_sucursal' => date('Y-m-d h:i:s')
    ]);
  }
}
