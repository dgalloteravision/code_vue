<?php

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\Configuracion;

class ConfiguracionesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Configuracion::create([
      'cod_usuario' => 1,
      'contrasenia_usuarios_importados_configuracion' => 'Procredito2018',
      'dias_caducidad_usuarios_configuracion' => 2,
      'horarios_acceso_usuarios_configuracion' => '13:00:00',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_configuracion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_configuracion' => date('Y-m-d h:i:s')
    ]);

    Configuracion::create([
      'cod_usuario' => 2,
      'contrasenia_usuarios_importados_configuracion' => 'Fenalco2018',
      'dias_caducidad_usuarios_configuracion' => 2,
      'horarios_acceso_usuarios_configuracion' => '13:00:00',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_configuracion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_configuracion' => date('Y-m-d h:i:s')
    ]);
  }
}
