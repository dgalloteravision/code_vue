<?php

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\TipoDocumento;

class TiposDocumentoTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    TipoDocumento::create([
      'nombre_tipo_documento' => 'Cedula de Ciudadanía',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_tipo_documento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_tipo_documento' => date('Y-m-d h:i:s')
    ]);

    TipoDocumento::create([
      'nombre_tipo_documento' => 'Cedula de Extranjería',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_tipo_documento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_tipo_documento' => date('Y-m-d h:i:s')
    ]);

    TipoDocumento::create([
      'nombre_tipo_documento' => 'NIT Empresarial',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_tipo_documento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_tipo_documento' => date('Y-m-d h:i:s')
    ]);

    TipoDocumento::create([
      'nombre_tipo_documento' => 'Pasaporte',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_tipo_documento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_tipo_documento' => date('Y-m-d h:i:s')
    ]);

    TipoDocumento::create([
      'nombre_tipo_documento' => 'Otro',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_tipo_documento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_tipo_documento' => date('Y-m-d h:i:s')
    ]);

    factory(Procredito\Modules\Seguridad\Models\TipoDocumento::class, 35)->create()->each(function ($d) {

    });
  }
}
