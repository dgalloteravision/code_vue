<?php

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\Rol;

class RolesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Rol::create([
      'nombre_rol' => 'SUPERADMIN',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_rol' => date('Y-m-d h:i:s'),
      'fecha_modificacion_rol' => date('Y-m-d h:i:s')
    ]);

    Rol::create([
      'nombre_rol' => 'ADMINSUCURSAL',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_rol' => date('Y-m-d h:i:s'),
      'fecha_modificacion_rol' => date('Y-m-d h:i:s')
    ]);

    Rol::create([
      'nombre_rol' => 'ADMINORGANIZACION',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_rol' => date('Y-m-d h:i:s'),
      'fecha_modificacion_rol' => date('Y-m-d h:i:s')
    ]);

    Rol::create([
      'nombre_rol' => 'NORMAL',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_rol' => date('Y-m-d h:i:s'),
      'fecha_modificacion_rol' => date('Y-m-d h:i:s')
    ]);

    Rol::create([
      'nombre_rol' => 'COMERCIANTE',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_rol' => date('Y-m-d h:i:s'),
      'fecha_modificacion_rol' => date('Y-m-d h:i:s')
    ]);

    factory(Procredito\Modules\Seguridad\Models\Rol::class, 35)->create()->each(function ($r) {

    });
  }
}
