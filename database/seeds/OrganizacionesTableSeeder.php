<?php

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\Organizaciones;

class OrganizacionesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Organizaciones::create([
      'cod_municipio' => 1,
      'nombre_organizacion' => 'Flamingo',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_organizacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
    ]);

    Organizaciones::create([
      'cod_municipio' => 1,
      'nombre_organizacion' => 'Falabella',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_organizacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
    ]);

    Organizaciones::create([
      'cod_municipio' => 1,
      'nombre_organizacion' => 'Éxito',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_organizacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
    ]);

    Organizaciones::create([
      'cod_municipio' => 1,
      'nombre_organizacion' => 'Mercurio de Oro',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_organizacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
    ]);

    Organizaciones::create([
      'cod_municipio' => 1,
      'nombre_organizacion' => 'Comerciante Distinguido',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_organizacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
    ]);

    Organizaciones::create([
      'cod_municipio' => 1,
      'nombre_organizacion' => 'Lealtad Comercial',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_organizacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
    ]);

    Organizaciones::create([
      'cod_municipio' => 1,
      'nombre_organizacion' => 'Gratitud Perenne',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_organizacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
    ]);

    Organizaciones::create([
      'cod_municipio' => 1,
      'nombre_organizacion' => 'A Toda una Vida',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_organizacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
    ]);

    Organizaciones::create([
      'cod_municipio' => 1,
      'nombre_organizacion' => 'Tendera Líder',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_organizacion' => date('Y-m-d h:i:s'),
      'fecha_modificacion_organizacion' => date('Y-m-d h:i:s')
    ]);

    factory(Procredito\Modules\Seguridad\Models\Organizaciones::class, 35)->create()->each(function ($o) {
      $o->sucursales()->save(factory(Procredito\Modules\Seguridad\Models\Sucursales::class)->make());
    });
  }
}
