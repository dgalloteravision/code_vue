<?php

use Illuminate\Database\Seeder;
use Procredito\Modules\Seguridad\Models\Departamento;

class DepartamentosTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Departamento::create([
      'nombre_departamento' => 'Antioquia',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Caldas',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Cundinamarca',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Bogotá, D.C.',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Atlántico',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Bolívar',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Boyacá',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Caquetá',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Cauca',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Cesar',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Córdoba',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Chocó',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Huila',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'La Guajira',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Magdalena',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Meta',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Nariño',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Norte de Santander',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Quindio',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Risaralda',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Santander',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Sucre',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Tolima',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Valle del Cauca',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Arauca',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Casanare',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Putumayo',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Amazonas',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Guainía',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Guaviare',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Vaupés',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    Departamento::create([
      'nombre_departamento' => 'Vichada',
      'cod_estado'=> 1,
      'cod_usuario_modificacion' => 1,
      'fecha_creacion_departamento' => date('Y-m-d h:i:s'),
      'fecha_modificacion_departamento' => date('Y-m-d h:i:s')
    ]);

    factory(Procredito\Modules\Seguridad\Models\Departamento::class, 5)->create()->each(function ($d) {
      $d->municipios()->save(factory(Procredito\Modules\Seguridad\Models\Municipio::class)->make());
      //$d->municipios()->saveMany(factory(App\Modules\Seguridad\Models\Municipio::class, 2)->create());
    });
  }
}
