<?php

use Procredito\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Representa el usuario con perfil super administrador de todo el sistema
    User::create([
      'name' => 'superadmin',
      'email'=> 'dany338@gmail.com',
      'password' => bcrypt('123456'),
      'cod_tipo_documento' => 1
    ]);

    // Representa el usuario con perfil administrador de una sucursal
    User::create([
      'name' => 'adminsucursal',
      'email'=> 'dgallo@fenalcoantioquia.com',
      'password' => bcrypt('123456'),
      'cod_tipo_documento' => 1
    ]);

    // Representa el usuario con perfil normal de acceso al sistema y que pertenece a una sucursal
    User::create([
      'name' => 'normal',
      'email'=> 'dany338@misena.edu.co',
      'password' => bcrypt('123456'),
      'cod_tipo_documento' => 1
    ]);

    factory(Procredito\User::class, 5)->create()->each(function ($u) {
      $u->ubicacion()->save(factory(Procredito\Modules\Seguridad\Models\Ubicacion::class)->make());
      $u->configuracion()->save(factory(Procredito\Modules\Seguridad\Models\Configuracion::class)->make());
    });
  }
}
